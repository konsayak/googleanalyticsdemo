package com.googleanalyticsdemo.app;

import android.content.Context;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.googleanalyticsdemo.R;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by ERDALKAYMAK on 19.7.2016.
 */
public final class AnalyticsTrackers {

    public enum Target{
        APP,
    }

    private static AnalyticsTrackers sInstance;

    public static synchronized void initialize(Context context){
        if(sInstance!=null){
            throw new IllegalStateException("Extra call initialize Analytics Trackers");
        }

        sInstance=new AnalyticsTrackers(context);

    }

    public static synchronized AnalyticsTrackers getsInstance(){
        if(sInstance==null){
            throw new IllegalStateException("call initialize() before getInstsnce" );
        }

        return sInstance;
    }

    private final Map<Target,Tracker> mTrackers=new HashMap<Target,Tracker>();
    private final Context mcontext;

    private AnalyticsTrackers(Context context){

        mcontext=context.getApplicationContext();

    }

    public synchronized Tracker get(Target target){
        if(!mTrackers.containsKey(target)){
            Tracker tracker;
            switch (target){
                case APP:
                    tracker= GoogleAnalytics.getInstance(mcontext).newTracker(R.xml.app_tracker);
                    break;
                default:
                    throw new IllegalArgumentException("Unhandled analytics target " + target);
            }
            mTrackers.put(target,tracker);
        }

        return mTrackers.get(target);
    }




}
