package go.vipme.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import go.vipme.R;
import go.vipme.activity.CloseStoreActivity;
import go.vipme.activity.DealDetailActivity;
import go.vipme.activity.StoreDetailActivity;
import go.vipme.constant.GeneralValues;
import go.vipme.dao.StoreOpenedDao;
import go.vipme.model.DealModel;
import go.vipme.model.ServerResponse;
import go.vipme.model.StoreModel;
import go.vipme.util.CustomTextView;
import go.vipme.util.ServerUtilFunctions;
import com.squareup.picasso.Picasso;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by OguzhanBasar on 18.03.2015.
 */
public class StoreListAdapter extends ArrayAdapter<StoreModel> {

    String dealId;

    Intent intent;
    Context context;
    ArrayList<StoreModel> storeModels;

    public StoreListAdapter(Context context,int textViewResourceId,ArrayList<StoreModel> storeModels,String dealId){

        super(context,textViewResourceId,storeModels);
        this.storeModels = storeModels;
        this.context = context;
        this.dealId = dealId;
        intent = new Intent(context, StoreDetailActivity.class);

    }

    @Override
    public View getView(final int position,View convertView,final ViewGroup parent){

        View v = convertView;
        if(v==null){

            LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.store_list_item, null);

        }

        CustomTextView storeNameText,distanceText;

        if(storeModels.get(position)!=null) {

            storeNameText   = (CustomTextView) v.findViewById(R.id.store_name);
            distanceText    = (CustomTextView) v.findViewById(R.id.distance);

            storeNameText.setText(storeModels.get(position).name);
            distanceText.setText(storeModels.get(position).distance);

        }

        v.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view){

                StoreOpenedDao.sendStoreDataToServer(dealId, storeModels.get(position).id);
                intent.putExtra("store_id",storeModels.get(position).id);
                intent.putExtra("store_name",storeModels.get(position).name);
                intent.putExtra("store_distance",storeModels.get(position).distance);
                intent.putExtra("store_address",storeModels.get(position).address);
                intent.putExtra("store_phone",storeModels.get(position).phone);
                intent.putExtra("store_latitude",storeModels.get(position).latitude);
                intent.putExtra("store_longitude",storeModels.get(position).longitude);
                intent.putExtra("store_open_hours",storeModels.get(position).openHours);
                context.startActivity(intent);

            }

        });

        return v;

    }

}
