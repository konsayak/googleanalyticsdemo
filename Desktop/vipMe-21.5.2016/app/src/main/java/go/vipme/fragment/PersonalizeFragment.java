package go.vipme.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.adjust.sdk.Adjust;
import com.facebook.appevents.AppEventsLogger;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.analytics.tracking.android.Tracker;

import go.vipme.R;
import go.vipme.activity.MainActivity;
import go.vipme.activity.ProfileActivity;
import go.vipme.activity.UserFavoredActivity;
import go.vipme.util.CustomTextView;
import go.vipme.util.Dialogs;
import go.vipme.util.InternetStatus;
import go.vipme.util.MyApplication;

/**
 * Created by Kursat on 4/14/2016.
 */
public class PersonalizeFragment extends Fragment implements ObservableScrollViewCallbacks {

    static Context context;
    Activity activity;
    ActionBar ab;

    LinearLayout footerButton,mLinearLayout;
    CustomTextView footerText,brandsButton,tagsButton,title;
    static View rootView;
    private Tracker tracker;
    View headerView;
    boolean firstopen=true;
    public String type;
    boolean brand=true,tag=false;

    public static CustomTextView userCounter;



    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView            =   inflater.inflate(R.layout.activity_personalize, container, false);
        ab                  =   ((MainActivity)getActivity()).actionBar;
        title               =   (CustomTextView)rootView.findViewById(R.id.title_text);
        userCounter         =   (CustomTextView)rootView.findViewById(R.id.user_follow_count);
        footerButton        =   (LinearLayout)rootView.findViewById(R.id.footer_button);
        brandsButton        =   (CustomTextView)rootView.findViewById(R.id.brands);
        tagsButton          =   (CustomTextView)rootView.findViewById(R.id.tags);
        footerText          =   (CustomTextView)rootView.findViewById(R.id.footer_text);
        mLinearLayout       =   (LinearLayout)rootView.findViewById(R.id.linearLayoutMenu);
        headerView          =   (View)rootView.findViewById(R.id.header);

        context = getActivity();
        if(firstopen){
            setCurrentFragment(new CarryFragment().newInstance("Personalize","brands"));
            firstopen=false;
        }

        Log.d("P if brand", brand+"");
        if(brand){
            Log.d("P if brand","girdi");
            brandsButton.setTextColor(getResources().getColor(R.color.black));
            tagsButton.setTextColor(getResources().getColor(R.color.light_grey));
            brand=false;
            tag=true;
            userCounter.setText(String.valueOf(TagContainerFragment.userBrandFollowingCount));
            footerText.setText("Adet Markan Var");

            setCurrentFragment(new CarryFragment().newInstance("Personalize","brands"));
        }else if(tag){
            Log.d("P else brand", "girdi ");
            brandsButton.setTextColor(getResources().getColor(R.color.light_grey));
            tagsButton.setTextColor(getResources().getColor(R.color.black));
            brand=true;
            tag=false;
            userCounter.setText(String.valueOf(TagContainerFragment.userTagFollowingCount));
            footerText.setText("Adet İlgi Alanın Var");

            setCurrentFragment(new CarryFragment().newInstance("Personalize","tags"));
        }

        brandsButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v){

                if(brand) {
                    Log.d("brandsButton if brand","oncliclistener");
                    brandsButton.setTextColor(getResources().getColor(R.color.black));
                    tagsButton.setTextColor(getResources().getColor(R.color.light_grey));
                    brand=false;
                    tag=true;
                    userCounter.setText(String.valueOf(TagContainerFragment.userBrandFollowingCount));
                    footerText.setText("Adet Markan Var");

                    setCurrentFragment(new CarryFragment().newInstance("Personalize","brands"));
                }

            }

        });

        tagsButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v){

                if(tag) {
                    Log.d("tagsButton if tag","oncliclistener");
                    brandsButton.setTextColor(getResources().getColor(R.color.light_grey));
                    tagsButton.setTextColor(getResources().getColor(R.color.black));
                    brand=true;
                    tag=false;
                    userCounter.setText(String.valueOf(TagContainerFragment.userTagFollowingCount));
                    footerText.setText("Adet İlgi Alanın Var");

                    setCurrentFragment(new CarryFragment().newInstance("Personalize","tags"));
                }

            }

        });

        footerButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v){
                if (InternetStatus.getInstance(getActivity()).isOnline()) {
                    Intent intent;
                    if (brand) {
                        intent = new Intent(getActivity(), UserFavoredActivity.class);
                        intent.putExtra("type", "tags");
                    } else {
                        intent = new Intent(getActivity(), UserFavoredActivity.class);
                        intent.putExtra("type", "brands");
                    }
                    getActivity().startActivity(intent);
                } else {
                    Dialogs.InternetControlDialog(getActivity());

                }
            }

        });



        return rootView;
    }
    public void setCurrentFragment(Fragment _fragment){
        FragmentManager manager = getChildFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.pager, _fragment);
        transaction.commit();

    }
    @Override
    public void onActivityCreated(final Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.tracker = EasyTracker.getInstance(getActivity());

        activity = getActivity();
      //  brandsButton.performClick();
        //setCurrentFragment(new CarryFragment().newInstance("Personalize","brands"));
//        if(activity != null) {
//            activity.runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    // This code will always run on the UI thread, therefore is safe to modify UI elements.
//                    ((MainActivity) activity).setActionBarCount("");
//                }
//            });
//        }
    }

    @Override
    public void onStart() {
        super.onStart();
        userCounter.setVisibility(View.VISIBLE);
        footerText.setVisibility(View.VISIBLE);
        this.tracker.set(Fields.SCREEN_NAME, getClass().getSimpleName());
        this.tracker.send(MapBuilder.createAppView().build());
    }


    @Override
    public void onPause() {
        super.onPause();

        brandsButton.performClick();
        userCounter.setVisibility(View.VISIBLE);
        setCurrentFragment(new CarryFragment().newInstance("Personalize","brands"));


        Log.e(MyApplication.TAG, "Personalize Activity onPause() brandPage="+TagContainerFragment.brandPage);
        Adjust.onPause();
        try {
            ProfileActivity.setUpdate(String.valueOf(TagContainerFragment.userBrandFollowingCount), String.valueOf(TagContainerFragment.userTagFollowingCount));
        }catch(Exception e){e.printStackTrace();}
        try {
            TagContainerFragment.brandStart=0;
            TagContainerFragment.brandPage=1;
        }catch(Exception e){e.printStackTrace();}
        try{
            TagContainerFragment.tagStart=0;
            TagContainerFragment.tagPage=1;
        }catch (Exception e){e.printStackTrace();}
    }

    @Override
    public void onResume(){
        super.onResume();
        setCurrentFragment(new CarryFragment().newInstance("Personalize","brands"));
        Adjust.onResume();


        Log.e(MyApplication.TAG, "Personalize Activity onResume() brandPage=" + TagContainerFragment.brandPage);


        AppEventsLogger.activateApp(getActivity(), "520852994707454");
    }
    public static void setUpdate(String count){
        userCounter.setText(count);
    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {}
    @Override
    public void onDownMotionEvent() {}
    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {

    }
    public void onClick(View v){
        if (v == brandsButton)
            setCurrentFragment(new CarryFragment().newInstance("Personalize","brands"));
        else if (v ==tagsButton)
            setCurrentFragment(new CarryFragment().newInstance("Personalize","tags"));

        return;

    }


}
