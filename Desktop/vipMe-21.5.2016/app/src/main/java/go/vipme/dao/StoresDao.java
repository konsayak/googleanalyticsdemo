package go.vipme.dao;

import android.content.Context;
import android.util.Log;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;
import go.vipme.activity.CloseStoreActivity;
import go.vipme.constant.GeneralValues;
import go.vipme.model.ServerResponse;
import go.vipme.model.StoreModel;
import go.vipme.util.JsonFunctions;
import go.vipme.util.ServerUtilFunctions;

public class StoresDao {

    public static ArrayList<StoreModel> getStoresFromServer(String dealID,String brandId,Context context){
        final List<NameValuePair> mParams = new ArrayList<>();
        ArrayList<StoreModel> mStoreList = null;




            mParams.add(new BasicNameValuePair("brand_id", brandId));
            mParams.add(new BasicNameValuePair("deal_id",dealID));



        mParams.add(new BasicNameValuePair("type", "nearby"));

        try {
            ServerResponse s = ServerUtilFunctions.callServer(GeneralValues.BASE_URL + "stores", mParams, true);
            if(s.success){
                mStoreList = new ArrayList<>();
                JSONObject mJSONObject = JsonFunctions.getJsonObjectFromObject(s.json, "data");
                mStoreList= storeJsonParse(JsonFunctions.getArrayFromObject(mJSONObject, "stores"));
                try{
                    ((CloseStoreActivity)context).brandName = JsonFunctions.getStringValue(mJSONObject, "brand_name");
                }catch (ClassCastException e){e.printStackTrace();}
            }else{
                Log.e("", "StoresDao - getStoresFromServer() - ServerResponse : " + s.error_message);
            }
        }catch (Exception e){e.printStackTrace();}
        return mStoreList;
    }

    private static ArrayList<StoreModel> storeJsonParse(JSONArray jsonArray){

        StoreModel storeModel;
        ArrayList<StoreModel> mStoreList = new ArrayList<>();
        int counter=0;

        while(jsonArray!=null && counter!=jsonArray.length()){
            storeModel = new StoreModel();
            try {
                storeModel.id = jsonArray.getJSONObject(counter).getString("id");
                storeModel.name = jsonArray.getJSONObject(counter).getString("name");
                storeModel.address = jsonArray.getJSONObject(counter).getString("address");
                storeModel.phone = jsonArray.getJSONObject(counter).getString("phone");
                storeModel.openHours = jsonArray.getJSONObject(counter).getString("open_hours");
                storeModel.latitude = jsonArray.getJSONObject(counter).getString("latitude");
                storeModel.longitude = jsonArray.getJSONObject(counter).getString("longitude");
                storeModel.distance = jsonArray.getJSONObject(counter).getString("distance");
                mStoreList.add(storeModel);
            }catch (JSONException e){e.printStackTrace();}
            counter++;
        }

        return mStoreList;

    }

}
