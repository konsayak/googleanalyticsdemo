package go.vipme.dao;

import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

import go.vipme.constant.GeneralValues;
import go.vipme.model.ServerResponse;
import go.vipme.util.ServerUtilFunctions;

/**
 * Created by AhmetOguzhanBasar on 25.06.2015.
 */
public class NewsOpDao {

    public static void sendOpDataToServer(final String function, String newsId, String value){

        final List<NameValuePair> mParams = new ArrayList<>();
        mParams.add(new BasicNameValuePair("news_id", newsId));

        if(function.equals("news_favored")) {
            mParams.add(new BasicNameValuePair("favored", value));
        }else if(function.equals("news_liked")) {
            mParams.add(new BasicNameValuePair("liked", value));
        }else if(function.equals("news_disliked")) {
            mParams.add(new BasicNameValuePair("disliked", value));
        }

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                ServerResponse s = ServerUtilFunctions.callServer(GeneralValues.BASE_URL + function, mParams, true);
                if(!s.success){
                    Log.e("", "NewsOpDao - sendOpDataToServer() - ServerResponse : " + s.error_message);
                }
            }
        });
        thread.start();

    }

}
