package go.vipme.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.adjust.sdk.Adjust;
import com.facebook.appevents.AppEventsLogger;
import com.google.analytics.tracking.android.EasyTracker;

import go.vipme.R;
import go.vipme.dao.LogShareDao;
import go.vipme.dao.NewsOpDao;
import go.vipme.dao.SingleNewsDao;
import go.vipme.fragment.NewsFragment;
import go.vipme.model.NewsModel;
import go.vipme.util.Dialogs;
import go.vipme.util.InternetStatus;
import go.vipme.util.MyApplication;

/**
 * Created by OguzhanBasar on 18.03.2015.
 */
public class NewsWebViewActivity extends Activity {

    String newsId;
    Context context;
    RelativeLayout loadingPanel;
    ImageButton likeButton,dislikeButton;
    ProgressBar loadingProgress;
    Toast loadingToastMessage;

    private boolean pageLoadFinished=false;

    NewsModel mNewsModel;

    ImageButton shareButton,backButton;

    Handler handler = new Handler();

    private WebView webView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_webview);

        context = this;

        backButton      = (ImageButton)findViewById(R.id.webview_back_button);
       // favoriteButton  = (ImageButton)findViewById(R.id.webview_favorite_button);
        shareButton     = (ImageButton)findViewById(R.id.share_button);
        loadingPanel    = (RelativeLayout)findViewById(R.id.loadingPanel);
        webView         = (WebView) findViewById(R.id.webView1);
        likeButton      = (ImageButton)findViewById(R.id.webview_like_button);
        dislikeButton   = (ImageButton)findViewById(R.id.webview_dislike_button);
        loadingProgress =(ProgressBar)findViewById(R.id.loadingProgress);

        loadingToastMessage=Toast.makeText(NewsWebViewActivity.this,
                "Yükleniyor çok az kaldı", Toast.LENGTH_LONG);

         final Intent intent = getIntent();

        newsId = intent.getStringExtra("news_id");

        Log.e( MyApplication.TAG, "NewsWebviewActivity newsID = " + newsId );

        if (InternetStatus.getInstance(NewsWebViewActivity.this).isOnline()) {
           try {
               mNewsModel = new NewsModel();
               Thread thread = new Thread(new Runnable() {
                   @Override
                   public void run() {
                       mNewsModel = SingleNewsDao.getNewFromServer(newsId);
                       handler.post(new Runnable() {
                           @Override
                           public void run() {
                               setThingsUp();
                           }
                       });
                   }
               });
               thread.start();

           } catch (Exception e) {
               e.printStackTrace();
           }

       }
       else{
            Dialogs.InternetControlDialog(NewsWebViewActivity.this);
       }

    }



    @Override
    public void onResume(){
        super.onResume();
        Adjust.onResume();
        AppEventsLogger.activateApp(this, "520852994707454");
    }

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance(this).activityStart(this); // Add this method.
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance(this).activityStop(this); // Add this method.
    }

    private void setThingsUp(){
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, final int newProgress) {

                if (newProgress > 85) {
                    pageLoadFinished = true;
                    loadingProgress.setVisibility(View.GONE);

                }

            }
        });
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                loadingPanel.setVisibility(View.VISIBLE);
                loadingProgress.setVisibility(View.VISIBLE);

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {

                        if (!pageLoadFinished)
                            loadingToastMessage.show();
                    }
                }, 7000);
                super.onPageStarted(view, url, favicon);
            }


            @Override
            public void onPageFinished(WebView view, String url) {
                loadingPanel.setVisibility(View.GONE);
                loadingProgress.setVisibility(View.GONE);
                pageLoadFinished = true;
                super.onPageFinished(view, url);
            }
        });
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setSupportZoom(true);
        webView.setInitialScale(1);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.setScrollbarFadingEnabled(false);
        webView.loadUrl(mNewsModel.newsUrl);


        if(mNewsModel.liked.equals("1")){
            likeButton.setActivated(true);
        }else if(mNewsModel.liked.equals("0")){
            likeButton.setActivated(false);
        }
        if(mNewsModel.disliked.equals("1")){
            dislikeButton.setActivated(true);
        }else if(mNewsModel.disliked.equals("0")){
            dislikeButton.setActivated(false);
        }
//        if(mNewsModel.favored.equals("1")){
//        favoriteButton.setActivated(true);
//        }else if(mNewsModel.favored.equals("0")){
//            favoriteButton.setActivated(false);
//        }

        shareButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view){
                if(!view.isActivated()){view.setActivated(!view.isActivated());}
                Toast.makeText(context,"Paylaşım seçenekleri hazırlanıyor.",Toast.LENGTH_SHORT).show();
                LogShareDao.sendShareDataToServer("news", newsId);

                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_SUBJECT, "");
                sendIntent.putExtra(Intent.EXTRA_TEXT, mNewsModel.title + " " + mNewsModel.shareUrl + ". Bu haber vipme uygulaması ile bulundu.");
                sendIntent.setType("text/plain");

                startActivity(Intent.createChooser(sendIntent, "Haber Paylaş"));
                if(view.isActivated()){
                    view.setActivated(!view.isActivated());
                }//sorun olabilir denemedim.
            }

        });

        backButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v){
                try{
                    if (webView.canGoBack()){
                        webView.goBack();
                    }


                    else {
                        onBackPressed();
                        finish();
                    }
                }
                catch (Exception e){
                    Log.e(MyApplication.TAG,"NewsWebView backButton error"+e.getMessage());
                }

            }


//                v.setActivated(!v.isActivated());
//                webView.stopLoading();
//                webView.loadUrl("");
//                webView.reload();
//                webView = null;
//                onBackPressed();
//                finish();


        });

        likeButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v){
                v.setActivated(!v.isActivated());
                if(v.isActivated()){
                    NewsOpDao.sendOpDataToServer("news_liked", newsId, "1");
                    try {
                        ((NewsFragment)(MyApplication.getNewsFragment())).setLikedFromWebView(newsId, "1");
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                    try {
//                        ((FavoritesFragment)(MyApplication.getFavoritesFragment())).setLikedFromWebView(newsId, "1");
                    }catch(Exception e){e.printStackTrace();}
                    dislikeButton.setActivated(false);
                }else{
                    NewsOpDao.sendOpDataToServer("news_liked", newsId, "0");
                    try {
                        ((NewsFragment) (MyApplication.getNewsFragment())).setLikedFromWebView(newsId, "0");
                    }catch(Exception e){e.printStackTrace();}
                    try {
//                        ((FavoritesFragment) (MyApplication.getFavoritesFragment())).setLikedFromWebView(newsId, "0");
                    }catch(Exception e){e.printStackTrace();}
                }

            }

        });

        dislikeButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v){
                v.setActivated(!v.isActivated());
                if(v.isActivated()){
                    NewsOpDao.sendOpDataToServer("news_disliked", newsId, "2");
                    try {
                        ((NewsFragment)(MyApplication.getNewsFragment())).setDislikedFromWebView(newsId, "1");
                    }catch(Exception e){e.printStackTrace();}
                    try {
//                        ((FavoritesFragment)(MyApplication.getFavoritesFragment())).setDislikedFromWebView(newsId, "1");
                    }catch(Exception e){e.printStackTrace();}
                    likeButton.setActivated(false);
                }else{
                    NewsOpDao.sendOpDataToServer("news_disliked", newsId, "0");
                    try {
                        ((NewsFragment) (MyApplication.getNewsFragment())).setDislikedFromWebView(newsId, "0");
                    }catch(Exception e){e.printStackTrace();}
                    try {
//                        ((FavoritesFragment) (MyApplication.getFavoritesFragment())).setDislikedFromWebView(newsId, "0");
                    }catch(Exception e){e.printStackTrace();}
                }

            }

        });



//        favoriteButton.setOnClickListener(new View.OnClickListener(){
//
//            @Override
//            public void onClick(View v){
//                v.setActivated(!v.isActivated());
//                if(mNewsModel.favored.equals("1")){
//                    mNewsModel.favored = "0";
//                    NewsOpDao.sendOpDataToServer("news_favored", newsId, "0");
//                    try {
//                        ((NewsFragment) (MyApplication.getNewsFragment())).setFavoredFromWebView(newsId, "0");
//                    }catch(Exception e){e.printStackTrace();}
//                    try {
////                        ((FavoritesFragment) (MyApplication.getFavoritesFragment())).setFavoredFromWebView(newsId, "0");
//                    }catch(Exception e){e.printStackTrace();}
//                    try{
////                        ((FavoritesFragment) (MyApplication.getFavoritesFragment())).removeNewsFromList(newsId);
//                    }catch(Exception e){e.printStackTrace();}
//                }else if(mNewsModel.favored.equals("0")){
//                    mNewsModel.favored = "1";
//                    NewsOpDao.sendOpDataToServer("news_favored", newsId, "1");
//                    try {
//                        ((NewsFragment)(MyApplication.getNewsFragment())).setFavoredFromWebView(newsId,"1");
//                    }catch(Exception e){e.printStackTrace();}
//                    try {
////                        ((FavoritesFragment)(MyApplication.getFavoritesFragment())).setFavoredFromWebView(newsId,"1");
//                    }catch(Exception e){e.printStackTrace();}
//                }
//            }
//
//        });
    }


}