package go.vipme.util;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;


public class NetworkChangeReceiver extends BroadcastReceiver {

    private boolean isConnected = false;
    private boolean isAlertShow=false;
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e(MyApplication.TAG, "Receieved notification about network status");
        isNetworkAvailable(context);
    }

    private boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        if(!isConnected){
                            Log.e(MyApplication.TAG, "Now you are connected to Internet!");
                            isConnected = true;

                        }
                        return true;
                    }
                }
            }
        }
        Log.e(MyApplication.TAG, "You are not connected to Internet!");
        if (!isAlertShow){
            isAlertShow=true;
            new AlertDialog.Builder(context)
                    .setMessage("İnternet bağlantısı yok.")
                    .setCancelable(false)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            isAlertShow=false;

                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }

        isConnected = false;
        return isConnected;
    }
}
