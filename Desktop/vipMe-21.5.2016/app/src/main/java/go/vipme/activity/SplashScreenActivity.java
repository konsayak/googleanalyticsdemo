    package go.vipme.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.adjust.sdk.Adjust;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import go.vipme.R;
import go.vipme.constant.GeneralValues;
import go.vipme.dao.PreferenceDao;
import go.vipme.util.GPSTracker;
import go.vipme.util.MyApplication;
import go.vipme.util.SystemFunctions;

public class SplashScreenActivity extends Activity {

    String deviceId,mLocation;

    static Context context;
    private GPSTracker gps;

    private GoogleCloudMessaging gcm;
    private String push_id = "";

    private static int SPLASH_TIME_OUT = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash);

        context=this;

        gps = new GPSTracker(SplashScreenActivity.this);


        registerLocation();
        registerDeviceId();


    }

    @Override
    public void onResume(){
        super.onResume();
        Adjust.onResume();
        //Facebook id kaydedildi
        AppEventsLogger.activateApp(this, "520852994707454");
    }

    private void splash(){

        new Handler().postDelayed (new Runnable() {

            @Override
            public void run() {
                try {
                    if (isOnline()) {
                        Intent i;
                        if (!PreferenceDao.getValue("first_time").equals("")) {
                            i = new Intent(SplashScreenActivity.this, MainActivity.class);


                        } else {
                            i = new Intent(SplashScreenActivity.this, IntroductionActivity.class);
                            PreferenceDao.setValue("first_time", "not");
                        }
                        startActivity(i);
                        onBackPressed();
                        finish();
                    } else {
                        new AlertDialog.Builder(context)
                                .setTitle("Bağlantı Hatası")
                                .setMessage("İnternet bağlantınızı kontrol edin")
                                .setPositiveButton("Tamam", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        onBackPressed();
                                        finish();
                                    }
                                }).show();


                    }
                }
                catch (Exception e){
                    Log.e(MyApplication.TAG,"SplashScreenActivity splash(); handler error");
                }
            }

        }, SPLASH_TIME_OUT);
    }



    private void registerDeviceId(){
        Thread getPushIdThread = new Thread(new Runnable(){
            @Override
            public void run(){
                try {
                    String old_push_id = PreferenceDao.getValue("push_id");
                    gcm = GoogleCloudMessaging.getInstance(SplashScreenActivity.this);
                    push_id = gcm.register(GeneralValues.SENDER_ID);

                    Log.e(MyApplication.TAG,"SplashScreenActivity old_push_id : " + old_push_id + " new_push_id : " + push_id);
                    if (!push_id.equals("") && !push_id.equals(old_push_id)) {
                        PreferenceDao.setValue(GeneralValues.PUSH_ID, push_id);
                        Log.e(MyApplication.TAG,"SplashScreenActivity push id updated");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e(MyApplication.TAG,"SplashScreenActivity registerUser Push Id error : "+e.toString());
                }
            }
        });
        getPushIdThread.start();

        deviceId = PreferenceDao.getValue(GeneralValues.DEVICE_ID);
        if(deviceId.length() == 0){
            Log.e(MyApplication.TAG,"SplashScreenActivity - New User");
            deviceId = SystemFunctions.getDeviceId();
            if(deviceId != null && deviceId.length() > 0){
                PreferenceDao.setValue(GeneralValues.DEVICE_ID, deviceId);
                PreferenceDao.setDeviceId3DForToken(deviceId);
                Log.e(MyApplication.TAG,"SplashScreenActivity - Newly Setted Device Id : " + deviceId);
            }else{
                Log.e(MyApplication.TAG,"SplashScreenActivity - Device Id Did Not Get");
            }
        }else{
            Log.e(MyApplication.TAG,"SplashScreenActivity - Already Registered User");
        }
        splash();
    }

    private void registerLocation(){

        mLocation = gps.getLatitude() + "/" + gps.getLongitude();

        if(mLocation != null && mLocation.length() > 0){
            PreferenceDao.setValue("user_location", mLocation);
            Log.e(MyApplication.TAG,"SplashScreenActivity - Newly Setted Location : " + mLocation);
        }else{
            Log.e(MyApplication.TAG,"SplashScreenActivity - Location Did Not Get");
        }

    }

    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null &&
                cm.getActiveNetworkInfo().isConnectedOrConnecting();
    }

}