package go.vipme.util;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import go.vipme.dao.LocationUpdateDao;
import go.vipme.dao.PreferenceDao;

/**
 * Created by Halit on 29.7.2015.
 */
public class LocationService extends Service implements LocationListener {

    private boolean isFirstTimeToLocation=true;
    private boolean isGpsEnabled=false;
    private LocationUpdateDao locDao=new LocationUpdateDao();


    String changingLocation;



//    Location mLocation;
    double latitude=0.0;
    double longitude=0.0;

    private static final long DISTANCE=300; //300 m
    private static final long TIME=1000*60*15;//15 dakika

    private LocationManager mLocationManager;


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(MyApplication.TAG, "Location Service onStart();");
        try {
            mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            isGpsEnabled=mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            if (isFirstTimeToLocation&&isGpsEnabled){
                mLocationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER,
                        TIME,
                        DISTANCE,
                        this
                );
                isFirstTimeToLocation=false;
            }
            else {
                mLocationManager.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER,
                        TIME,
                        DISTANCE,
                        this
                );
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }



        return START_STICKY;
    }



    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        Log.e(MyApplication.TAG,"Location Service onDestroy();");
        super.onDestroy();
        mLocationManager.removeUpdates(this);
    }


//    private Location getLocation() {
//        try{
//
//            if (!isGpsEnabled&&!isNetworkEnabled){
//                Log.e(MyApplication.TAG,"LocationService getLocation PassiveProvider active");
//                //passive i kullan
//                mLocationManager.requestLocationUpdates(
//                        LocationManager.PASSIVE_PROVIDER,
//                        DISTANCE,
//                        TIME,
//                        this
//                );
//                if (mLocationManager!=null){
//                    mLocation=mLocationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
//                    if (mLocation!=null){
//                        latitude=mLocation.getLatitude();
//                        longitude=mLocation.getLongitude();
//                    }
//                }
//
//            }
//
//            else if (isNetworkEnabled){
//                Log.e(MyApplication.TAG,"LocationService getLocation NetworkProvider active");
//                mLocationManager.requestLocationUpdates(
//                        LocationManager.NETWORK_PROVIDER,
//                        DISTANCE,
//                        TIME,
//                        this
//                );
//                if (mLocationManager!=null){
//                    mLocation=mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
//                    if (mLocation!=null){
//                        latitude=mLocation.getLatitude();
//                        longitude=mLocation.getLongitude();
//                    }
//                }
//
//            }
//
//            else if (isGpsEnabled){
//                Log.e(MyApplication.TAG,"LocationService getLocation GPSProvider active");
//                mLocationManager.requestLocationUpdates(
//                        LocationManager.GPS_PROVIDER,
//                        DISTANCE,
//                        TIME,
//                        this
//                );
//                if (mLocationManager!=null){
//                    mLocation=mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
//                    if (mLocation!=null){
//                        latitude=mLocation.getLatitude();
//                        longitude=mLocation.getLongitude();
//                    }
//                }
//            }
//
//
//        }
//        catch (Exception e){
//            Log.e(MyApplication.TAG,"LocationService getLocation error"+e.toString());
//            e.printStackTrace();
//        }
//        return mLocation;
//    }

//    @Override
//    public int onStartCommand(Intent intent, int flags, int startId) {
//        super.onStartCommand(intent, flags, startId);
//        return START_STICKY;
//    }

    @Override
    public IBinder onBind(Intent ıntent) {
        return null;
    }

    @Override
    public void onLocationChanged(Location location) {
        latitude=location.getLatitude();
        longitude=location.getLongitude();

        changingLocation=latitude+"/"+longitude;

        if (location!=null){
            PreferenceDao.setValue("user_location", changingLocation);



            Log.e(MyApplication.TAG, "LocationService location=" + PreferenceDao.getValue("user_changeable_location"));
            updateLocationUser();
        }
        else {
            Log.e(MyApplication.TAG,"LocationService onLocationChanged Location Alınamadı mLocation="+location);
        }
    }

    private void updateLocationUser() {
        try{
            Thread thread=new Thread(new Runnable() {
                @Override
                public void run() {
                    locDao.sendLocationInstantToServer();
                }
            });
            thread.start();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {
        Log.e(MyApplication.TAG, "onProviderEnabled: " );
    }

    @Override
    public void onProviderDisabled(String s) {
        Log.e(MyApplication.TAG, "onProviderDisabled: " );
    }
}
