package go.vipme.dao;

import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

import go.vipme.constant.GeneralValues;
import go.vipme.model.ServerResponse;
import go.vipme.util.ServerUtilFunctions;

/**
 * Created by AhmetOguzhanBasar on 23.06.2015.
 */
public class LogShareDao {

    public static void sendShareDataToServer(String type, String remoteId){
        final List<NameValuePair> mParams = new ArrayList<>();
        mParams.add(new BasicNameValuePair("type", type));
        mParams.add(new BasicNameValuePair("remote_id", remoteId));

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    ServerResponse s = ServerUtilFunctions.callServer(GeneralValues.BASE_URL + "log_share", mParams, true);
                    if(!s.success){
                        Log.e("", "LogShareDao - sendShareDataToServer() - ServerResponse : " + s.error_message);
                    }
                }catch(Exception e){e.printStackTrace();}
            }
        });
        thread.start();

    }

}
