package go.vipme.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;

import go.vipme.R;
import go.vipme.activity.UserFavoredActivity;
import go.vipme.dao.TagDao;
import go.vipme.dao.TagFollowDao;
import go.vipme.model.TagModel;
import go.vipme.util.Dialogs;
import go.vipme.util.InternetStatus;
import go.vipme.util.SystemFunctions;

public class FollowContainerFragment extends Fragment {

    ArrayList<TagModel> tagList = new ArrayList<>(), mTagList;
    ScrollView mScrollView;
    SwipeRefreshLayout swipeRefresh;
    UserFavoredActivity mParentActivity = null;
    String fragmentType, callingPage;
    int width = 0, i = 0, margin, page = 1;
    Context context;
    Activity activity;

    final Handler mHandler = new Handler();
    final Runnable mUpdateResults = new Runnable() {
        public void run() {
            setTags(i);
        }
    };

    public static int userBrandFollowingCount = 0, userTagFollowingCount = 0;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_follow_container, container, false);
        mScrollView = (ScrollView) view.findViewById(R.id.scrollview);
        swipeRefresh = (SwipeRefreshLayout) view.findViewById(R.id.swipeContainer);

        swipeRefresh.setProgressViewOffset(false, 0, (int) (getResources().getDimension(R.dimen.top_padding_caused_by_action_bar)));
        swipeRefresh.setColorSchemeResources(
                R.color.theme_blue,
                R.color.tag_bg,
                R.color.male_blue);
        swipeRefresh.setEnabled(false);

        context = getActivity();
        activity = getActivity();

        fragmentType = getArguments().getString("type");
        callingPage = getArguments().getString("page");

        width = SystemFunctions.getScreenWidth(getActivity());
        margin = SystemFunctions.getScreenHeight(getActivity()) / 100;

        getData(this, callingPage, fragmentType, page, "personal", "", "");

        return view;

    }

    private void getData(final FollowContainerFragment fragment, final String callingPage, final String function, final int page, final String type, final String categoryId, final String keyword) {
        if (InternetStatus.getInstance(getActivity()).isOnline()) {
            swipeRefresh.setRefreshing(true);
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                mTagList = new ArrayList<>();
                mTagList = TagDao.getTagsFromServer(fragment, callingPage, function, page, type, categoryId, keyword);
                if (mTagList != null) {
                    mHandler.post(mUpdateResults);
                }
            }
        });
        thread.start();
    }
        else{
            Dialogs.InternetControlDialog(context);

        }

}

    private void setTags(final int start) {

        final Handler handler = new Handler();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {

                int recentId = 0, totalWidth = 0;

                final LinearLayout lyTemp = new LinearLayout(context);

                lyTemp.setLayoutParams(new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT));
                lyTemp.setOrientation(LinearLayout.VERTICAL);
                lyTemp.setGravity(Gravity.CENTER_HORIZONTAL);

                TextView mTv;
                RelativeLayout.LayoutParams mLayoutParams;
                RelativeLayout mRelativeLayout = null;
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
                lp.setMargins(0, margin, 0, 0);

                if (mTagList == null) {
                    return;
                } else {
                    lyTemp.removeAllViews();
                    tagList.addAll(tagList.size(), mTagList);
                }

                for (i = start; i < tagList.size(); i++) {
                    TagModel mTagModel = tagList.get(i);
                    if (getActivity() == null)
                        return;
                    Activity activity = getActivity();
                    final Resources mResources = activity.getResources();

                    mTv = new TextView(getActivity());
                    mTv.setText(mTagModel.brandName);
                    mTv.setId(Integer.parseInt(mTagModel.tagId));
                    mTv.setTextColor(mResources.getColor(R.color.tag_text));
                    if (fragmentType.equals("brands")) {
                        mTv.setBackgroundResource(R.drawable.brand_selector);
                    } else {
                        mTv.setBackgroundResource(R.drawable.tag_selector);
                    }
                    if (mTagModel.following.equals("1")) {
                        mTv.setActivated(true);
                        mTv.setTextColor(mResources.getColor(R.color.white));
                    } else {
                        mTv.setActivated(false);
                        mTv.setTextColor(mResources.getColor(R.color.tag_text));
                    }
                    mTv.setPadding(
                            mResources.getDimensionPixelOffset(R.dimen.tag_left_padding),
                            mResources.getDimensionPixelOffset(R.dimen.tag_top_padding),
                            mResources.getDimensionPixelOffset(R.dimen.tag_right_padding),
                            mResources.getDimensionPixelOffset(R.dimen.tag_bottom_padding)
                    );
                    mTv.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
                    final TextView finalMTv = mTv;
                    mTv.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            if (v.isActivated()) {
                                v.setActivated(false);
                                finalMTv.setTextColor(mResources.getColor(R.color.tag_text));
                                if (fragmentType.equals("brands")) {
                                    try {
                                        userBrandFollowingCount--;
                                        mParentActivity.userCounter.setText(String.valueOf(userBrandFollowingCount) + " Adet");
                                        TagFollowDao.setTagFollow("brand_following", Integer.toString(v.getId()), "0");
                                    } catch (NullPointerException e) {e.printStackTrace();}
                                } else {
                                    try {
                                        userTagFollowingCount--;
                                        mParentActivity.userCounter.setText(String.valueOf(userTagFollowingCount) + " Adet");
                                        TagFollowDao.setTagFollow("tag_following", Integer.toString(v.getId()), "0");
                                    } catch (NullPointerException e) {e.printStackTrace();}
                                }
                            } else {
                                v.setActivated(true);
                                finalMTv.setTextColor(mResources.getColor(R.color.white));
                                if (fragmentType.equals("brands")) {
                                    try {
                                        userBrandFollowingCount++;
                                        mParentActivity.userCounter.setText(String.valueOf(userBrandFollowingCount) + " Adet");
                                        TagFollowDao.setTagFollow("brand_following", Integer.toString(v.getId()), "1");
                                    } catch (NullPointerException e) {e.printStackTrace();}
                                } else {
                                    try {
                                        userTagFollowingCount++;
                                        mParentActivity.userCounter.setText(String.valueOf(userTagFollowingCount) + " Adet");
                                        TagFollowDao.setTagFollow("tag_following", Integer.toString(v.getId()), "1");
                                    } catch (NullPointerException e) {e.printStackTrace();}
                                }
                            }
                        }
                    });

                    mLayoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    mLayoutParams.setMargins(margin / 2, 0, margin / 2, 0);
                    if (totalWidth != 0) {
                        mLayoutParams.addRule(RelativeLayout.RIGHT_OF, recentId);
                    }

                    recentId = mTv.getId();
                    mTv.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);

                    if (totalWidth == 0) {
                        mRelativeLayout = new RelativeLayout(context);
                        mRelativeLayout.setLayoutParams(lp);
                        mRelativeLayout.setGravity(Gravity.CENTER_HORIZONTAL);
                        lyTemp.addView(mRelativeLayout);
                    }

                    if (totalWidth + mTv.getMeasuredWidth() + (mLayoutParams.rightMargin * 2) >= width) {
                        totalWidth = 0;
                        i--;
                    } else {
                        mRelativeLayout.addView(mTv, mLayoutParams);
                        totalWidth = totalWidth + mTv.getMeasuredWidth() + (mLayoutParams.rightMargin * 2);
                    }
                }

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefresh.setRefreshing(false);
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    mParentActivity = ((UserFavoredActivity) getActivity());
                                } catch (ClassCastException e) {e.printStackTrace();}

                                if (fragmentType.equals("brands") && mParentActivity != null) {
                                    mParentActivity.userFavoredTitle.setText("Markaların");
                                    mParentActivity.userCounter.setText(String.valueOf(userBrandFollowingCount + " Adet"));
                                } else if (fragmentType.equals("tags") && mParentActivity != null) {
                                    mParentActivity.userFavoredTitle.setText("Etiketlerin");
                                    mParentActivity.userCounter.setText(String.valueOf(userTagFollowingCount + " Adet"));
                                }
                                mScrollView.addView(lyTemp);
                            }
                        });
                    }
                });
            }
        });
        thread.start();

    }

    public static FollowContainerFragment newInstance(String page, String type) {
        FollowContainerFragment newFragment = new FollowContainerFragment();
        Bundle args = new Bundle();
        args.putString("type", type);
        args.putString("page", page);
        newFragment.setArguments(args);
        return newFragment;
    }




}