package go.vipme.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.ImageButton;

import com.adjust.sdk.Adjust;
import com.facebook.appevents.AppEventsLogger;
import com.google.analytics.tracking.android.EasyTracker;

import go.vipme.R;
import go.vipme.fragment.FollowContainerFragment;
import go.vipme.fragment.TagContainerFragment;
import go.vipme.util.CustomTextView;
import go.vipme.util.MyApplication;

/**
 * Created by OguzhanBasar on 27.03.2015.
 */
public class UserFavoredActivity extends FragmentActivity {

    String type;

    public CustomTextView userCounter,userFavoredTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_favored);

        ImageButton backButton       = (ImageButton)findViewById(R.id.back_button);
                    userCounter      = (CustomTextView)findViewById(R.id.user_favored_subtitle);
                    userFavoredTitle = (CustomTextView)findViewById(R.id.user_favored_title);

        type = getIntent().getStringExtra("type");

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String parts[] = String.valueOf(userCounter.getText()).split(" ");

                    if (type.equals("brands")) {
                        ProfileActivity.setUpdate(parts[0],String.valueOf(""));
                        ((PersonalizeActivity) (MyApplication.getPersonalizeActivity())).setUpdate(parts[0]);

                        ((TagContainerFragment)(((TagSelectActivity)(MyApplication.getTagSelectActivity())).mTagSelectPagerAdapter.getItem(0))).userBrandFollowingCount=Integer.parseInt(parts[0]);
                    } else if (type.equals("tags")) {
                        ProfileActivity.setUpdate(String.valueOf(""),parts[0]);
                        ((PersonalizeActivity) (MyApplication.getPersonalizeActivity())).setUpdate(parts[0]);

                        ((TagContainerFragment)(((TagSelectActivity)(MyApplication.getTagSelectActivity())).mTagSelectPagerAdapter.getItem(1))).userTagFollowingCount=Integer.parseInt(parts[0]);
                    }
                }catch(Exception e){e.printStackTrace();}
                onBackPressed();
                finish();
            }
        });

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        FollowContainerFragment containerFragment = new FollowContainerFragment().newInstance("UserFavored", type);

        if(savedInstanceState==null) {
            fragmentTransaction.add(R.id.fragment, containerFragment, "favored_"+type);
        }else{
            fragmentTransaction.replace(R.id.fragment, containerFragment, "favored_" + type);
        }
        fragmentTransaction.commit();
    }

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance(this).activityStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance(this).activityStop(this);
    }

    @Override
    public void onResume(){
        super.onResume();
        Adjust.onResume();
        AppEventsLogger.activateApp(this, "520852994707454");
    }
    public void onBackPressed(){
        super.onBackPressed();
        try {
            String parts[] = String.valueOf(userCounter.getText()).split(" ");

            if (type.equals("brands")) {
                ProfileActivity.setUpdate(parts[0],String.valueOf(""));
                ((PersonalizeActivity) (MyApplication.getPersonalizeActivity())).setUpdate(parts[0]);

                ((TagContainerFragment)(((TagSelectActivity)(MyApplication.getTagSelectActivity())).mTagSelectPagerAdapter.getItem(0))).userBrandFollowingCount=Integer.parseInt(parts[0]);
            } else if (type.equals("tags")) {
                ProfileActivity.setUpdate(String.valueOf(""),parts[0]);
                ((PersonalizeActivity) (MyApplication.getPersonalizeActivity())).setUpdate(parts[0]);

                ((TagContainerFragment)(((TagSelectActivity)(MyApplication.getTagSelectActivity())).mTagSelectPagerAdapter.getItem(1))).userTagFollowingCount=Integer.parseInt(parts[0]);
            }
        }catch(Exception e){e.printStackTrace();}

    }

}


