package go.vipme.util;

import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import go.vipme.constant.DeviceType;
import go.vipme.constant.GeneralValues;
import go.vipme.dao.PreferenceDao;
import go.vipme.model.ServerResponse;


public class ServerUtilFunctions {

    public static ServerResponse callServer(String url, List<NameValuePair> params, Boolean isget){
        List<NameValuePair> headers = new ArrayList<>();
        params.add(new BasicNameValuePair(GeneralValues.DEVICE_ID, PreferenceDao.getValue("device_id")));
        params.add(new BasicNameValuePair(GeneralValues.TOKEN, PreferenceDao.getValue("device_id_3d")));
        params.add(new BasicNameValuePair(GeneralValues.DEVICE_TYPE, DeviceType.AndroidPhone));
        params.add(new BasicNameValuePair(GeneralValues.DEVICE_MODEL, SystemFunctions.getDeviceName()));
        params.add(new BasicNameValuePair(GeneralValues.APP_VERSION, SystemFunctions.getVersionName()));
        params.add(new BasicNameValuePair(GeneralValues.CARRIER, SystemFunctions.getCarrierValue()));
        params.add(new BasicNameValuePair(GeneralValues.PUSH_ID, PreferenceDao.getValue("push_id")));
        String userLocation = PreferenceDao.getValue("user_location");
        try {
            String[] parts = userLocation.split("/");
            params.add(new BasicNameValuePair(GeneralValues.LAT, parts[0]));
            params.add(new BasicNameValuePair(GeneralValues.LNG, parts[1]));
        }catch(Exception e){e.printStackTrace();}

        return callServerCustomHeader(url, params, headers, null, isget);
    }


    public static ServerResponse callServerCustomHeader(String url, List<NameValuePair> params, List<NameValuePair> header, StringEntity jsonParam , Boolean isget){
        ServerResponse result = new ServerResponse();
        JSONObject json;
        String resultString = "";
        InputStream is = null;

        Log.e("","ServerUtilFunctions - callServerCustomHeader() - headers : "+header);
        Log.e("","ServerUtilFunctions - callServerCustomHeader() - params : "+params);

        try {
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpResponse httpResponse;
            if (isget) {
                String paramString = URLEncodedUtils.format(params, "UTF-8");
                url += "?" + paramString;
                Log.e(MyApplication.TAG,"URL="+url);

                HttpGet httpGet = new HttpGet(url);
                for (int i = 0; i < header.size(); i++) {
                    httpGet.addHeader(header.get(i).getName(),header.get(i).getValue());
                }
                httpResponse = httpClient.execute(httpGet);
            } else {
                HttpPost httpPost = new HttpPost(url);
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-Type", "application/json; charset=UTF-8");

                for (int i = 0; i < header.size(); i++) {
                    httpPost.addHeader(header.get(i).getName(),header.get(i).getValue());
                }

                if (jsonParam == null) {
                    String paramsJsonString = "{";
                    for (int i = 0; i < params.size(); i++) {
                        if (i == params.size() - 1) {
                            paramsJsonString+= "\"" + params.get(i).getName() + "\":" + "\"" + params.get(i).getValue() + "\"";
                        } else {
                            paramsJsonString+= "\"" + params.get(i).getName() + "\":" + "\"" + params.get(i).getValue() + "\",";
                        }
                    }
                    paramsJsonString+= "}";
                    StringEntity jsonEntity = new StringEntity(paramsJsonString, "UTF-8");
                    httpPost.setEntity(jsonEntity);
                } else {
                    httpPost.setEntity(jsonParam);
                }

                httpResponse = httpClient.execute(httpPost);
            }

            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();

            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            resultString = sb.toString();
            Log.d("callingUrl",url);
            Log.d("resultString",resultString);
            json = new JSONObject(resultString);
            Log.e("","ServerUtilFunctions - callServerCustomHeader() - result json : "+json);
            result.json = json;
            if(JsonFunctions.getStringValue(json, "result").equals("1")){
                result.success = true;
            }else{
                result.success = false;
                result.error_message = "Result not OK";
            }
        }catch (Exception e) {
            Log.e("","ServerUtilFunctions - callServer - error 57:"+e.toString());
            result.error_message = "Bir Hata Oluştu!";
            result.success = false;

        }

        return result;
    }
}
