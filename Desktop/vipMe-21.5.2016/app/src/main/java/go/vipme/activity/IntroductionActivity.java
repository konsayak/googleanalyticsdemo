package go.vipme.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.adjust.sdk.Adjust;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import go.vipme.R;
import go.vipme.constant.GeneralValues;
import go.vipme.dao.FacebookDao;
import go.vipme.dao.GenderDao;
import go.vipme.dao.PreferenceDao;
import go.vipme.fragment.IntroductionInnerFragment;
import go.vipme.model.IntroductionModel;
import go.vipme.util.CustomTextView;
import go.vipme.util.MyApplication;
import go.vipme.util.SystemFunctions;

public class IntroductionActivity extends FragmentActivity {

    ArrayList<Fragment> introFragmentList = new ArrayList<>();
    ArrayList<View> ballList;
    ImageView goOnButton;
    LoginButton facebookLoginBtn;
    CustomTextView askLater;
    private FacebookDao facebookDao=new FacebookDao();
    int numberOfViewPagerChildren;
    boolean clickable = false;
    private static String APP_ID="520852994707454";
    private CallbackManager callbackManager;



    public ViewPager viewPager=null;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode,resultCode,data);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager=CallbackManager.Factory.create();








        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_introduction);

        int screenHeight            = SystemFunctions.getScreenHeight(this);
        int buttonLayoutHeight      = (int)(screenHeight*23.13)/100;
        int tenDP                   = (int)(screenHeight*1.56)/100;

        LinearLayout ballContainer  = (LinearLayout)findViewById(R.id.ball_container);
        LinearLayout buttonLayout   = (LinearLayout)findViewById(R.id.button_layout);
        viewPager                   = (ViewPager) findViewById(R.id.pager);
        goOnButton                  = (ImageView)findViewById(R.id.go_on_button);
        askLater                    = (CustomTextView)findViewById(R.id.ask_later);
        facebookLoginBtn            =(LoginButton)findViewById(R.id.facebookLogin);

        facebookLoginBtn.setBackgroundResource(R.drawable.facebook_login);
        facebookLoginBtn
                .setCompoundDrawablesWithIntrinsicBounds(R.drawable.transparent, 0, 0, 0);

        facebookLoginBtn.setReadPermissions(Arrays.asList("public_profile"));
        facebookLoginBtn.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(final LoginResult loginResult) {
                Log.e("FacebookLogin onSuccess", "");


                PreferenceDao.setValue("fb_login_status", GeneralValues.FB_LOGIN);
                GraphRequest request=GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject user, GraphResponse response) {
                                try{

                                    Profile profile=Profile.getCurrentProfile();
                                    String name=user.getString("name");
                                    PreferenceDao.setValue("user_name", name);
                                    String gender=user.getString("gender");
                                    String userID=profile.getId();
                                    String accessToken=loginResult.getAccessToken().getToken();
                                    String fbTokenExpired="";
                                    String birthday="";


                                    facebookDao.facebookConnect(userID,accessToken,fbTokenExpired,name,
                                            gender,birthday);



                                    Log.e(MyApplication.TAG,"FacebookLogin MyName="+name);
                                    Log.e(MyApplication.TAG,"FacebookLogin MyGender="+gender);
                                    Log.e(MyApplication.TAG,"FacebookLogin MyID="+userID);
                                    Log.e(MyApplication.TAG,"FacebookLogin MyAccessToken"+accessToken);


                                    
                                    fbLoginToSkipGenderChoose(gender);
                                }
                                catch (Exception e){

                                    Log.e(MyApplication.TAG,"IntroductionActivity Login onSuccess Error");

                                }


                            }
                        }
                );
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Log.e("FacebookLogin onCancel", "");

            }

            @Override
            public void onError(FacebookException e) {
                Log.e("FacebookLogin onError()", "");
            }
        });



        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,1);

        ArrayList<IntroductionModel> introductionModelList = new ArrayList<>();
        introductionModelList.add(new IntroductionModel(R.string.intro_1_text,R.drawable.yeni_birinci));
        introductionModelList.add(new IntroductionModel(R.string.intro_2_text,R.drawable.iki));
        introductionModelList.add(new IntroductionModel(R.string.intro_3_text,R.drawable.uc));
        introductionModelList.add(new IntroductionModel(R.string.intro_4_text,R.drawable.dort));
        introductionModelList.add(new IntroductionModel(R.string.intro_6_text,R.drawable.vipme));


        numberOfViewPagerChildren = introductionModelList.size();

        for(int a=0;a<numberOfViewPagerChildren;a++){
            introFragmentList.add(a,newInstance(introductionModelList.get(a).introText, introductionModelList.get(a).introImage));

        }

        params.setMargins(0, 20, 0, 0);

        buttonLayout.getLayoutParams().height = buttonLayoutHeight;
        ballContainer.setPadding(0,tenDP,0,tenDP);

        params = new LinearLayout.LayoutParams(tenDP,tenDP);
        params.setMargins(10, 0, 10, 0);

        ballList = new ArrayList<>();

        for(int a=0;a<numberOfViewPagerChildren;a++) {
            View ball = new View(this);
            ball.setLayoutParams(params);
            if(a==0) {
                ball.setBackgroundResource(R.drawable.ball_focus);
            }
            else {
                ball.setBackgroundResource(R.drawable.ball_blur);
            }
            ballContainer.addView(ball);
            ballList.add(a,ball);
        }

        viewPager.getLayoutParams().height = screenHeight - buttonLayoutHeight-tenDP;
        viewPager.setAdapter(new MyAdapter(getSupportFragmentManager()));

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            int previouslySelected = 0;

            @Override
            public void onPageScrolled(int i, float v, int i2) {
            }

            @Override
            public void onPageSelected(int i) {

                View ball;

                ball = ballList.get(previouslySelected);
                ball.setBackgroundResource(R.drawable.ball_blur);

                ball = ballList.get(i);
                ball.setBackgroundResource(R.drawable.ball_focus);

                previouslySelected = i;

                if (i == numberOfViewPagerChildren - 1) {
                    clickable = true;
                    //fb girişi yapılmışsa
                    if (PreferenceDao.getValue(GeneralValues.FB_LOGIN_STATUS)
                            .equals(GeneralValues.FB_LOGIN)){
                        goOnButton.setVisibility(View.VISIBLE);
                        facebookLoginBtn.setVisibility(View.GONE);
                        askLater.setVisibility(View.GONE);
                    }
                    else {
                        goOnButton.setVisibility(View.GONE);
                        facebookLoginBtn.setVisibility(View.VISIBLE);
                        askLater.setVisibility(View.VISIBLE);
                    }


                }

            }

            @Override
            public void onPageScrollStateChanged(int i) {
            }
        });

        goOnButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (viewPager.getCurrentItem() == numberOfViewPagerChildren - 1) {
                    Intent intent = new Intent(IntroductionActivity.this, GenderChooseActivity.class);
                    startActivity(intent);
                    onBackPressed();
                    finish();
                }
                else {
                    viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true);
                }
            }

        });

        askLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(IntroductionActivity.this,GenderChooseActivity.class);
                startActivity(intent);
                onBackPressed();
                finish();
            }
        });




    }


    @Override
    public void onResume(){
        super.onResume();
        Adjust.onResume();

        AppEventsLogger.activateApp(this, "520852994707454");
    }

    class MyAdapter extends FragmentStatePagerAdapter {

        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            return introFragmentList.get(i);
        }

        @Override
        public int getCount() {
            return numberOfViewPagerChildren;
        }

    }

    public static final IntroductionInnerFragment newInstance(int text, int image){
        IntroductionInnerFragment fragment = new IntroductionInnerFragment();
        Bundle bundle = new Bundle(2);
        bundle.putInt("text", text);
        bundle.putInt("image", image);
        fragment.setArguments(bundle);
        return fragment ;
    }






    private void fbLoginToSkipGenderChoose(final String gender)
    {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                if (gender.equals("male")) {
                    GenderDao.sendGenderToServer(2);
                }
                else if (gender.equals("female")) {
                    GenderDao.sendGenderToServer(1);
                }
            }
        });
        thread.start();
        Intent intent = new Intent(IntroductionActivity.this, TagSelectActivity.class);
        intent.putExtra("type", "brands");
        startActivity(intent);
        onBackPressed();
        finish();
    }



}