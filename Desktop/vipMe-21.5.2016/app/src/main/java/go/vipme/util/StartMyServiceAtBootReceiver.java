package go.vipme.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by Halit on 31.7.2015.
 */
public class StartMyServiceAtBootReceiver  extends BroadcastReceiver{
    Context mContext;
    private final String BOOT_ACTION="android.intent.action.BOOT_COMPLETED";
    @Override
    public void onReceive(Context context, Intent intent) {
        mContext=context;
        String action=intent.getAction();
        if (action.equalsIgnoreCase(BOOT_ACTION)){
            startService();
        }
        Log.e(MyApplication.TAG, "BroadcastReceiver onReceive()");

    }

    private void startService() {
        Log.e(MyApplication.TAG, "StartMyServiceAtBootReceiver() startService()");
        Intent mServiceIntent=new Intent(mContext,LocationService.class);
        mContext.startService(mServiceIntent);

    }
}
