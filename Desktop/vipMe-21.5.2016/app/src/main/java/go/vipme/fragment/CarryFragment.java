package go.vipme.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.adjust.sdk.Adjust;
import com.facebook.appevents.AppEventsLogger;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.analytics.tracking.android.Tracker;

import java.util.ArrayList;

import go.vipme.R;
import go.vipme.activity.ProfileActivity;
import go.vipme.dao.CategoriesDao;
import go.vipme.model.NavigationDrawerListItemModel;
import go.vipme.util.MyApplication;

/**
 * Created by Kursat on 4/29/2016.
 */
public class CarryFragment extends Fragment {
    static View rootView;
    private Tracker tracker;
    public static ArrayList<NavigationDrawerListItemModel> tempCategoryArrayTags,tempCategoryArrayBrands;
    TextView[] myTextViews;
    Handler mHandler = new Handler();
    static int trigger;
    TextView mTextViewFirst;
    LinearLayout mLinearLayout;
    String fragmentType,callingPage;
    Activity activity;


    final Runnable mUpdateTagsResults = new Runnable() {
        public void run() {
            setTagsUI();
        }
    };

    final Runnable mUpdateBrandsResults = new Runnable() {
        public void run() {
            setBrandsUI();
        }
    };

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView            =   inflater.inflate(R.layout.fragment_carry, container, false);
        mLinearLayout       =   (LinearLayout)rootView.findViewById(R.id.linearLayoutMenu);
        fragmentType                    = getArguments().getString("type");
        callingPage                     = getArguments().getString("page");
        activity = getActivity();

        if(fragmentType.equals("brands")){
            getCategoriesBrandsData();
            setCurrentFragment(new TagContainerFragment().newInstance("Personalize","brands","",false));
        } else if (fragmentType.equals("tags")) {
            getCategoriesTagsData();
            setCurrentFragment(new TagContainerFragment().newInstance("Personalize","tags","",false));
        }

        return rootView;
    }
    public static CarryFragment newInstance(String page,String type) {

        CarryFragment newFragment = new CarryFragment();
        Bundle args = new Bundle();
        args.putString("type", type);
        args.putString("page", page);
        newFragment.setArguments(args);

        return newFragment;
    }


    public void getCategoriesTagsData() {


        tempCategoryArrayTags   = new ArrayList<>();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    tempCategoryArrayTags = CategoriesDao.getCategories("tag");
                    if(tempCategoryArrayTags!=null) {
                        mHandler.post(mUpdateTagsResults);
                    }
                }catch (Exception e){e.printStackTrace();}
            }
        });
        thread.start();

    }
    public void getCategoriesBrandsData() {
        tempCategoryArrayBrands   = new ArrayList<>();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    tempCategoryArrayBrands = CategoriesDao.getCategories("brand");
                    if(tempCategoryArrayBrands!=null) {
                        mHandler.post(mUpdateBrandsResults);
                    }
                }catch (Exception e){e.printStackTrace();}
            }
        });
        thread.start();

    }
    private void setBrandsUI() {
        int N = tempCategoryArrayBrands.size()+1;

        mLinearLayout.removeAllViews();
        myTextViews = new TextView[N];
        final float scale = activity.getResources().getDisplayMetrics().density;
        int wPixelToDps = (int) (100 * scale + 0.5f);
        int hPixelToDps = (int) (35 * scale + 0.5f);

        mTextViewFirst = new TextView(activity);
        mTextViewFirst.setText("Tümü");
        mTextViewFirst.setGravity(Gravity.CENTER);
        mTextViewFirst.setWidth(wPixelToDps);
        mTextViewFirst.setHeight(hPixelToDps);
        mTextViewFirst.setTextColor(activity.getResources().getColor(R.color.black));
        mTextViewFirst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TagContainerFragment.isComplete()){
                    setCurrentFragment(new TagContainerFragment().newInstance("Personalize","brands","",true));


                    trigger = 0;

                    for(int index = 0; index<tempCategoryArrayBrands.size(); index++){
                        myTextViews[index].setTextColor(getResources().getColor(R.color.light_grey));
                        if(index == trigger){
                            myTextViews[index].setTextColor(getResources().getColor(R.color.black));
                        }
                    }

                }

            }
        });

        mLinearLayout.addView(mTextViewFirst);

        myTextViews[0] = mTextViewFirst;

        for (int i = 1; i < tempCategoryArrayBrands.size()+1; i++) {
            Log.d("ArrayBrands.get(i).",tempCategoryArrayBrands.get(i-1).id+"");
            final TextView mTextView = new TextView(activity);
            mTextView.setText(tempCategoryArrayBrands.get(i-1).name);
            mTextView.setGravity(Gravity.CENTER);
            int len = (int) (103 * scale + 0.5f);
            mTextView.setWidth(len);
            mTextView.setHeight(hPixelToDps);
            mTextView.setTextColor(activity.getResources().getColor(R.color.light_grey));
            myTextViews[i] = mTextView;

            final int finalI = i;
            myTextViews[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(TagContainerFragment.isComplete()){
                        trigger = finalI;
                        Log.d("myTextViews[i]Brand",finalI+" tiklandi");
                        Log.d("categoryIdBrand",tempCategoryArrayBrands.get(finalI -1).id+"");
                        for(int index = 0; index<tempCategoryArrayBrands.size(); index++){
                            if(index == trigger){
                                myTextViews[index].setTextColor(getResources().getColor(R.color.black));
                            } else {
                                myTextViews[index].setTextColor(getResources().getColor(R.color.light_grey));
                            }
                        }


                        setCurrentFragment(new TagContainerFragment().newInstance("Personalize","brands",tempCategoryArrayBrands.get(finalI -1).id,true));

                        mTextView.setTextColor(getResources().getColor(R.color.black));


                    }

                }
            });

            mLinearLayout.addView(mTextView);
        }

    }
    private void setTagsUI(){
        mLinearLayout.removeAllViews();
        int N               = tempCategoryArrayTags.size()+1;
        myTextViews         = new TextView[N];
        final float scale = activity.getResources().getDisplayMetrics().density;
        int wPixelToDps = (int) (100 * scale + 0.5f);
        int hPixelToDps = (int) (35 * scale + 0.5f);
//        LinearLayout mLinearLayout = (LinearLayout) rootView.findViewById(R.id.linearLayoutMenu);

        mTextViewFirst = new TextView(activity);
        mTextViewFirst.setText("Tümü");
        mTextViewFirst.setGravity(Gravity.CENTER);
        mTextViewFirst.setWidth(wPixelToDps);
        mTextViewFirst.setHeight(hPixelToDps);
        mTextViewFirst.setTextColor(activity.getResources().getColor(R.color.black));
        mTextViewFirst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TagContainerFragment.isComplete()){

                    setCurrentFragment(new TagContainerFragment().newInstance("Personalize","tags","",true));

                    trigger = 0;

                    for(int index = 0; index<tempCategoryArrayTags.size()+1; index++){
                        myTextViews[index].setTextColor(getResources().getColor(R.color.light_grey));
                        if(index == trigger){
                            myTextViews[index].setTextColor(getResources().getColor(R.color.black));
                        }
                    }

                }

            }
        });

        mLinearLayout.addView(mTextViewFirst);

        myTextViews[0] = mTextViewFirst;

        for (int i = 1; i < tempCategoryArrayTags.size()+1; i++) {
            Log.d("ArrayTags.get(i).",tempCategoryArrayTags.get(i-1).id+"");
            final TextView mTextView = new TextView(activity);
            mTextView.setText(tempCategoryArrayTags.get(i-1).name);
            mTextView.setGravity(Gravity.CENTER);
            mTextView.setWidth(wPixelToDps);
            mTextView.setHeight(hPixelToDps);
            mTextView.setTextColor(activity.getResources().getColor(R.color.light_grey));
            myTextViews[i] = mTextView;

            final int finalI = i;
            myTextViews[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(TagContainerFragment.isComplete()){
                        Log.d("myTextViews[i]",finalI+" tiklandi");
                        Log.d("categoryId",tempCategoryArrayTags.get(finalI -1).id+"");
                        trigger = finalI;
                        for(int index = 0; index<tempCategoryArrayTags.size()+1; index++){

                            if(index == trigger){
                                myTextViews[index].setTextColor(getResources().getColor(R.color.black));
                            } else {
                                myTextViews[index].setTextColor(getResources().getColor(R.color.light_grey));
                            }
                        }

                        setCurrentFragment(new TagContainerFragment().newInstance("Personalize","tags",tempCategoryArrayTags.get(finalI -1).id,true));
                        mTextView.setTextColor(getResources().getColor(R.color.black));

                    }


                }
            });

            mLinearLayout.addView(mTextView);
        }


    }
    private void setCurrentFragment(Fragment _fragment){
        FragmentManager manager = getChildFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.carryPage, _fragment);
        transaction.commit();

    }
    @Override
    public void onActivityCreated(final Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.tracker = EasyTracker.getInstance(getActivity());
    }

    @Override
    public void onStart() {
        super.onStart();
        this.tracker.set(Fields.SCREEN_NAME, getClass().getSimpleName());
        this.tracker.send(MapBuilder.createAppView().build());
    }
    @Override
    public void onPause() {
        super.onPause();

        Log.e(MyApplication.TAG, "Personalize Activity onPause() brandPage="+TagContainerFragment.brandPage);
        Adjust.onPause();
        try {
            if((TagContainerFragment.userBrandFollowingCount != 0) && (TagContainerFragment.userTagFollowingCount != 0)) {
                ProfileActivity.setUpdate(String.valueOf(TagContainerFragment.userBrandFollowingCount), String.valueOf(TagContainerFragment.userTagFollowingCount));
            }
        }catch(Exception e){e.printStackTrace();}
        try {
            TagContainerFragment.brandStart=0;
            TagContainerFragment.brandPage=1;
        }catch(Exception e){e.printStackTrace();}
        try{
            TagContainerFragment.tagStart=0;
            TagContainerFragment.tagPage=1;
        }catch (Exception e){e.printStackTrace();}
    }

    @Override
    public void onResume(){
        super.onResume();
        Adjust.onResume();


        Log.e(MyApplication.TAG, "Personalize Activity onResume() brandPage=" + TagContainerFragment.brandPage);


        AppEventsLogger.activateApp(getActivity(), "520852994707454");
    }

}
