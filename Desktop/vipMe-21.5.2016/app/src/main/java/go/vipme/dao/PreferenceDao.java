package go.vipme.dao;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import go.vipme.util.MyApplication;
import go.vipme.util.TrippleDes;


public class PreferenceDao{

    public static void setValue(String key, String value) {
        try {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(MyApplication.getAppContext());
            Editor editor = sharedPreferences.edit();
            editor.putString(key, value);
            editor.commit();
            Log.d(MyApplication.TAG,"PreferencesDao - setValue(String key, String value) - Newly Setted Key/Value : " + key + "/" + value);
        }catch (Exception e) {
            Log.e(MyApplication.TAG,"PreferencesDao - setValue(String key, String value) : " + e.toString());
        }
    }

    public static void setValue(String key, Set<String> value) {
        try {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(MyApplication.getAppContext());
            Editor editor = sharedPreferences.edit();
            editor.putStringSet(key, value);
            editor.commit();
            Log.d(MyApplication.TAG,"PreferencesDao - setValue(String key, HashSet<String> value) - Newly Setted Key/Value : " + key + "/" + value);
        } catch (Exception e) {
            Log.e(MyApplication.TAG,"PreferencesDao - setValue(String key, HashSet<String> value) : " + e.toString());
        }
    }

    public static void setValue(String key, int value) {
        try{
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(MyApplication.getAppContext());
            Editor editor = sharedPreferences.edit();
            editor.putInt(key, value);
            editor.commit();
            Log.d(MyApplication.TAG,"PreferencesDao - setValue(String key, int value) - Newly Setted Key/Value : " + key + "/" + value);
        }catch(Exception e){
            Log.e(MyApplication.TAG,"PreferencesDao - setValue(String key, int value) : " + e.toString());
        }
    }

    public static String getValue(String key) {
        SharedPreferences mSharedPrefs = PreferenceManager.getDefaultSharedPreferences(MyApplication.getAppContext());
        SharedPreferences.Editor mPrefsEditor = mSharedPrefs.edit();
        String value = mSharedPrefs.getString(key, "");
        mPrefsEditor.commit();
        Log.d(MyApplication.TAG,"PreferencesDao - String getValue(String key) - Getted Key/Value : " + key + "/" + value);
        return value;
    }

    public static Set<String> getListValue(String key) {
        SharedPreferences mSharedPrefs = PreferenceManager.getDefaultSharedPreferences(MyApplication.getAppContext());
        SharedPreferences.Editor mPrefsEditor = mSharedPrefs.edit();
        Set<String> value = mSharedPrefs.getStringSet(key, new HashSet<String>());
        mPrefsEditor.commit();
        Log.d(MyApplication.TAG,"PreferencesDao - HashSet<String> getValue(String key) - Getted Key/Value : " + key + "/" + value);
        return value;
    }

    public static int getValueInt(String key) {
        int value = 0;
        try{
            SharedPreferences mSharedPrefs = PreferenceManager.getDefaultSharedPreferences(MyApplication.getAppContext());
            SharedPreferences.Editor mPrefsEditor = mSharedPrefs.edit();
            value = mSharedPrefs.getInt(key, 0);
            mPrefsEditor.commit();
            Log.d(MyApplication.TAG,"PreferencesDao - int getValue(String key) - Getted Key/Value : " + key + "/" + value);
        }catch(Exception e){
            Log.e(MyApplication.TAG,"PreferencesDao - int getValue(String key) : " + e.toString());
        }
        return value;
    }

    public static void removeValue(String key) {
        try{
            SharedPreferences mSharedPrefs = PreferenceManager.getDefaultSharedPreferences(MyApplication.getAppContext());
            mSharedPrefs.edit().remove(key).commit();
            Log.d(MyApplication.TAG,"PreferencesDao - removeValue(String key) - Removed Key : " + key);
        }catch(Exception e){
            Log.e(MyApplication.TAG,"PreferencesDao - removeValue() : " + e.toString());
        }
    }

    public static void setDeviceId3DForToken(String Id) {
        try {
            String text = Id;
            String encripted_text="";
            int text_length = text.length();
            String text_reverted = text.substring(0, text_length - 2);
            text_reverted = new StringBuilder(text_reverted).reverse().toString();
            Random random = new Random();
            Integer rand_number =  random.nextInt(100000) + 1;
            text = text + text_reverted + "&&" + rand_number + "&&";
            try {
                TrippleDes td= new TrippleDes();
                encripted_text = td.encrypt(text);
                setValue("device_id_3d", encripted_text);
                Log.e(MyApplication.TAG,"PreferencesDao - setDeviceId3DForToken() - Plain Text : " + text + ", Encripted Text : " + encripted_text);
            }catch(Exception e1){
                e1.printStackTrace();
                Log.e(MyApplication.TAG,"PreferencesDao - setDeviceId3DForToken() - Text Encription Error (Phase 2) : " + e1.toString());
            }

        }catch(Exception e){
            Log.e(MyApplication.TAG,"PreferencesDao - setDeviceId3DForToken() - Text Encription Error (Phase 1) : " + e.toString());
        }
    }

    public static void setScreenWidthSize(int screenWidth) {
        try{
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(MyApplication.getAppContext());
            Editor editor = sharedPreferences.edit();
            editor.putInt("screenWidth", screenWidth);
            editor.commit();
        }catch(Exception e){
            Log.e(MyApplication.TAG,"PreferencesDao - setScreenWidthSize : " + e.toString());
        }
    }

    public static int getScreenWidthSize() {
        int screenWidth = 0;
        try {
            SharedPreferences mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(MyApplication.getAppContext());
            SharedPreferences.Editor mEditor = mSharedPreferences.edit();
            screenWidth = mSharedPreferences.getInt("screenWidth",0);
            mEditor.commit();
        } catch (Exception e) {
            Log.e(MyApplication.TAG,"PreferencesDao - getScreenWidthSize : " + e.toString());
        }
        return screenWidth;
    }


    public static void hideSoftKeyboard(Context context,EditText searchText){
        InputMethodManager imm = (InputMethodManager) context.getSystemService(context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(searchText.getWindowToken(), 0);

    }
    public static void showSettingAlert(String title,String message,Context context){
        AlertDialog.Builder alertDialog=new AlertDialog.Builder(context);
        alertDialog.setTitle(title)
                .setMessage(message)
                .setPositiveButton("Tamam",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .show();
    }
}
