package go.vipme.dao;

import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

import go.vipme.constant.GeneralValues;
import go.vipme.model.ServerResponse;
import go.vipme.util.MyApplication;
import go.vipme.util.ServerUtilFunctions;


/**
 * Created by Halit on 29.7.2015.
 */
public class LocationUpdateDao {
    public static void sendLocationInstantToServer(){
        final List<NameValuePair> mParams=new ArrayList<>();

        Thread thread=new Thread(new Runnable() {


            @Override
            public void run() {

                try{
                    ServerResponse serverResponse= ServerUtilFunctions.callServer(GeneralValues.BASE_URL +
                            "update_location", mParams, true);
                    if (!serverResponse.success){
                        Log.e(MyApplication.TAG,"LocationUpdateDao   sendLocationInstantToServer()- Server Response:"
                                +serverResponse.error_message);
                    }
                }

                catch (Exception e){
                    e.printStackTrace();
            }
            }
        });
            thread.start();
    }




}

