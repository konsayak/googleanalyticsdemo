package go.vipme.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import go.vipme.fragment.DealsFragment;
import go.vipme.fragment.FavoritesFragment;
import go.vipme.fragment.NewsFragment;
import go.vipme.fragment.PersonalizeFragment;

public class MainActivityPagerAdapter extends FragmentPagerAdapter {


    public MainActivityPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int index){
        switch(index){
            case 0:

                return new DealsFragment().newInstance("0");
            case 1:
                return new NewsFragment().newInstance("0");
            case 2:
                return new FavoritesFragment();
            case 3:
                return new PersonalizeFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 4;
    }

}