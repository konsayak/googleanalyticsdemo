package go.vipme.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ksoichiro.android.observablescrollview.ObservableListView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.analytics.tracking.android.Tracker;
import com.melnykov.fab.FloatingActionButton;

import java.util.ArrayList;
import java.util.Iterator;

import go.vipme.R;
import go.vipme.activity.MainActivity;
import go.vipme.activity.SearchActivity;
import go.vipme.adapter.DealListAdapter;
import go.vipme.adapter.NavDrawerListAdapter;
import go.vipme.dao.CategoriesDao;
import go.vipme.dao.DealsDao;
import go.vipme.model.DealModel;
import go.vipme.model.NavigationDrawerListItemModel;
import go.vipme.util.CustomTextView;
import go.vipme.util.Dialogs;
import go.vipme.util.MyApplication;

public class DealsFragment extends Fragment implements ObservableScrollViewCallbacks {

    ActionBar ab;
    int currentFirstVisibleItem, currentTotalItemCount, currentScrollState;
    Activity activity;
    static int listViewFirstVisibleItem;
    static boolean loadedOnce = false, isLoading = false, swipeToRefresh = false;
    static Context context;
    static LayoutInflater headerInflater;
    static ObservableListView mListView;
    static View rootView;
    FloatingActionButton fab;
    private Tracker tracker;
    boolean isAlertShow=false,control=true;


    public static boolean isFirstLoad;
    CustomTextView myDealsButton, dealsButton;
    static TextView noContentDeals;
    static boolean myDealsButtonClikable = false, dealsButtonClikable = true, myDealsControl = true;

    public int pageCounter = 1;
    public static String deal_category;
    public static ArrayList<DealModel> dealArrayList;
    public static ArrayList<DealModel> myDealArrayList;
    public static ArrayList<NavigationDrawerListItemModel> categoryArray = new ArrayList<>();
    public static DealListAdapter mDealListAdapter, geciciCozum;
    public static NavDrawerListAdapter mNavDrawerArrayListAdapter;

    private static ArrayList<DealModel> tempDealArrayList;

    public static ArrayList<DealModel> tempMyDealArrayList;
    private static SwipeRefreshLayout swipeContainer;
    private static Handler handler = new Handler();
    private static ArrayList<String> navDrawerItems = new ArrayList<>();

    final static Handler mHandler = new Handler();
    Boolean isOpened = false;

    final static Runnable dealsUpdateResults = new Runnable() {
        public void run() {
            setUI();
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_deals, container, false);
        mListView = (ObservableListView) rootView.findViewById(R.id.deal_list);
        headerInflater = inflater;
        ab = ((MainActivity) getActivity()).actionBar;
        fab = (FloatingActionButton) rootView.findViewById(R.id.fab_deals_fragment);
        myDealsButton = (CustomTextView) rootView.findViewById(R.id.my_deals_button);
        dealsButton = (CustomTextView) rootView.findViewById(R.id.deals_button);
        noContentDeals = (TextView) rootView.findViewById(R.id.no_content);


        isOpened = true;
        MyApplication.setDealsFragment(this);
        context = getActivity();
        mListView.setScrollViewCallbacks(this);
        ViewGroup header = (ViewGroup) headerInflater.inflate(R.layout.top_padding_layout, mListView, false);
        mListView.addHeaderView(header, null, false);

        isFirstLoad = true;
        dealArrayList = new ArrayList<>();
        myDealArrayList = new ArrayList<>();
        tempDealArrayList = new ArrayList<>();
        tempMyDealArrayList = new ArrayList<>();

        deal_category = getArguments().getString("deal_category");
        swipeContainer = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeContainer);
        swipeContainer.setProgressViewOffset(false, 0, (int) (getResources().getDimension(R.dimen.top_padding_caused_by_action_bar)));
        swipeContainer.setColorSchemeResources(
                R.color.theme_blue,
                R.color.tag_bg,
                R.color.male_blue);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mListView.setVisibility(View.GONE);
                noContentDeals.setVisibility(View.INVISIBLE);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        getData("deals", pageCounter, deal_category);
                        ab.show();
                    }
                }, 800);
            }
        });
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isOnline()){
                    Intent i = new Intent(getActivity(), SearchActivity.class);
                i.putExtra("isDeal", "deal");
                startActivity(i);

            }
                else{
                    if (!isAlertShow){
                        isAlertShow=true;
                        new AlertDialog.Builder(context)
                                .setMessage("İnternet bağlantısı yok.")
                                .setCancelable(false)
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        isAlertShow=false;

                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }

                }
        }
        });
        mListView.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                currentFirstVisibleItem = firstVisibleItem;
                listViewFirstVisibleItem = currentFirstVisibleItem;
                currentTotalItemCount = totalItemCount;
            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                currentScrollState = scrollState;
                this.isScrollCompleted();
            }

            private void isScrollCompleted() {
                if (currentFirstVisibleItem > currentTotalItemCount - 5 && currentScrollState == SCROLL_STATE_IDLE) {
                    if (!isLoading) {
                        isLoading = true;
                        pageCounter++;
                        getData("deals", pageCounter, deal_category);





                    }
                }
            }

        });



        myDealsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.controlDrawlerClickDeals=false;
                MainActivity.mDrawerList.clearChoices();
                if (myDealsButtonClikable) {
                    //MainActivity.mDrawerList.clearChoices();
                    myDealsButton.setTextColor(getResources().getColor(R.color.close_stores_bg));
                    dealsButton.setTextColor(getResources().getColor(R.color.light_grey));
                    swipeContainer.setRefreshing(true);
                    mListView.setVisibility(View.GONE);
                    myDealsButtonClikable = false;
                    dealsButtonClikable = true;
                    myDealsControl = true;
                    pageCounter = 1;
                    getData("deals", pageCounter, "0");
                    MainActivity.changeTopTitle("İndirimlerim");
                    ab.show();


                }
            }
        });

        getData("deals", pageCounter, deal_category);

        dealsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.controlDrawlerClickDeals=false;
                MainActivity.mDrawerList.clearChoices();
                if (dealsButtonClikable){

                dealsButton.setTextColor(getResources().getColor(R.color.close_stores_bg));
                myDealsButton.setTextColor(getResources().getColor(R.color.light_grey));
                swipeContainer.setRefreshing(true);
                mListView.setVisibility(View.GONE);
                myDealsButtonClikable = true;
                dealsButtonClikable = false;
                myDealsControl = false;
                pageCounter = 1;
                getData("deals", pageCounter, "0");
                MainActivity.changeTopTitle("İndirimlerim");
                ab.show();
            }
            }
        });




        return rootView;
    }

    @Override
    public void onActivityCreated(final Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity = getActivity();
        this.tracker = EasyTracker.getInstance(getActivity());
    }

    @Override
    public void onResume() {
        super.onResume();
        this.tracker.set(Fields.SCREEN_NAME, getClass().getSimpleName());
        this.tracker.send(MapBuilder.createAppView().build());
    }


    @Override
    public void onStart() {

        super.onStart();
        if(control){
            myDealsButtonClikable = false;
            dealsButtonClikable = true;
            control=false;
        }
        else if (control){
            myDealsButtonClikable = true;
            dealsButtonClikable = false;
            control=true;

        }

        this.tracker.set(Fields.SCREEN_NAME, getClass().getSimpleName());
        this.tracker.send(MapBuilder.createAppView().build());
    }


    public void getData(final String function, final int page, final String dealsType) {
        Log.e("DealTAG", "getDataforDeals: " );
        noContentDeals.setVisibility(View.GONE);
        swipeContainer.setRefreshing(true);
        if(isOnline()){
        if (!isLoading) {
            mListView.setVisibility(View.GONE);
        }
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (!loadedOnce) {
                        loadedOnce = true;
                        Thread thread = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                categoryArray = CategoriesDao.getCategories("brand");
                                if (categoryArray != null) {
                                    handler.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            int counter = 0;
                                            navDrawerItems.add("Tümü");
                                            while (!categoryArray.isEmpty() && counter < categoryArray.size()) {
                                                try {
                                                    navDrawerItems.add(categoryArray.get(counter).name);
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                                counter++;
                                            }
                                            mNavDrawerArrayListAdapter = new NavDrawerListAdapter(context, R.layout.drawer_list_item, navDrawerItems);

                                            MainActivity.mDrawerList.setAdapter(mNavDrawerArrayListAdapter);
                                            MainActivity.mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

                                        }
                                    });
                                }
                            }
                        });
                        thread.start();

                    }

                    if (myDealsControl) {
                        tempMyDealArrayList = DealsDao.getDealsFromServer(function, page, "personal", "", dealsType, "", "");
                        if (page == 1) {
                            isFirstLoad = true;
                        }
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                swipeContainer.setRefreshing(false);
                                mListView.setVisibility(View.VISIBLE);


                                if (isFirstLoad) {
                                    myDealArrayList.clear();
                                    swipeContainer.setRefreshing(false);
                                    myDealArrayList.addAll(myDealArrayList.size(), tempMyDealArrayList);
                                    mDealListAdapter = new DealListAdapter(context, R.layout.list_item, myDealArrayList);
                                    mListView.setAdapter(mDealListAdapter);
                                    mListView.setVisibility(View.VISIBLE);
                                    isFirstLoad = false;
                                    if (myDealArrayList.isEmpty()) {

                                        noContentDeals.setText("Size özel indirimler için kişiselleştir menüsünden marka seçmelisiniz");
                                        noContentDeals.setVisibility(View.VISIBLE);


                                    }
                                }
                                else {
                                    try{
                                    if (!tempMyDealArrayList.isEmpty()) {


                                        if (isFirstLoad && myDealArrayList.isEmpty()) {

                                            Log.d("isFirstLoad", "111111");
                                            myDealArrayList.addAll(myDealArrayList.size(), tempMyDealArrayList);
                                        } else if (!isFirstLoad && !myDealArrayList.isEmpty()) {


                                              //myDealArrayList.clear();
                                            Log.d("isFirstLoad", "222222");
                                            myDealArrayList.addAll(myDealArrayList.size(), tempMyDealArrayList);
                                        }
                                        mDealListAdapter = new DealListAdapter(context, R.layout.list_item, myDealArrayList);
                                        mListView.setAdapter(mDealListAdapter);

                                        if (!myDealArrayList.isEmpty()) {
                                            noContentDeals.setVisibility(View.GONE);


                                            mListView.setSelection(listViewFirstVisibleItem);
                                        } else {

                                            mListView.setSelection(0);
                                        }
                                    } else if (tempMyDealArrayList.isEmpty() && myDealArrayList.isEmpty()) {
                                        noContentDeals.setText("Size özel indirimler için kişiselleştir menüsünden marka seçmelisiniz");
                                        noContentDeals.setVisibility(View.VISIBLE);

                                        Toast.makeText(context, "Uygun veri bulunamadı", Toast.LENGTH_SHORT).show();
                                        myDealArrayList.clear();
                                        mDealListAdapter = new DealListAdapter(context, R.layout.list_item, myDealArrayList);
                                        mListView.setAdapter(mDealListAdapter);
                                    }
                                }catch(Exception e){e.printStackTrace();}

                                    swipeContainer.setRefreshing(false);
                                    mListView.setVisibility(View.VISIBLE);
                                    tempMyDealArrayList.clear();
                                    isLoading = false;
                                }
                            }
                        });

                    } else {
                        Log.d("else e"," girdi");
                        tempDealArrayList = DealsDao.getDealsFromServer(function, page, "all", "", dealsType, "","");
                        Log.d("tempDealArrayList.size ",tempDealArrayList.size()+"");
                        if (page == 1) {
                            isFirstLoad = true;
                        }




                        handler.post(new Runnable() {

                            @Override
                            public void run() {
                                swipeContainer.setRefreshing(false);
                                mListView.setVisibility(View.VISIBLE);


                                if (isFirstLoad) {

                                    Log.d("aaa", " girdi");
                                    dealArrayList.clear();
                                    swipeContainer.setRefreshing(false);
                                    dealArrayList.addAll(dealArrayList.size(), tempDealArrayList);
                                    mDealListAdapter = new DealListAdapter(context, R.layout.list_item, dealArrayList);
                                    mListView.setAdapter(mDealListAdapter);
                                    mListView.setVisibility(View.VISIBLE);
                                    isFirstLoad=false;
                                }
                                 else {
                                    if(!tempDealArrayList.isEmpty()) {
                                        dealArrayList.addAll(dealArrayList.size(), tempDealArrayList);
                                        mDealListAdapter = new DealListAdapter(context, R.layout.list_item, dealArrayList);
                                        mListView.setAdapter(mDealListAdapter);
                                        if(!dealArrayList.isEmpty()){
                                            mListView.setSelection(listViewFirstVisibleItem);
                                        }else{
                                            mListView.setSelection(0);
                                        }
                                    }
                                    swipeContainer.setRefreshing(false);
                                    mListView.setVisibility(View.VISIBLE);
                                    tempDealArrayList.clear();
                                    isLoading = false;
                                }
                            }
                        });
                    }
                }catch (Exception e){e.printStackTrace();}
            }
        });
            thread.start();
        }
        else{
            swipeContainer.setRefreshing(false);
            Dialogs.InternetControlDialog(context);
        }

}




    private static void setUI(){

        if(!tempDealArrayList.isEmpty()) {


            if(swipeToRefresh){
                dealArrayList.clear();
            }
            dealArrayList.addAll(dealArrayList.size(), tempDealArrayList);
            mDealListAdapter.notifyDataSetChanged();

            if(!dealArrayList.isEmpty()){
                noContentDeals.setVisibility(View.GONE);
                mListView.setSelection(listViewFirstVisibleItem);
            }else{
                if(isFirstLoad) {
                    geciciCozum = new DealListAdapter(MyApplication.getAppContext(), R.layout.news_list_item, dealArrayList);
                    mListView.setAdapter(geciciCozum);
                    isFirstLoad = false;
                }
                mListView.setSelection(0);
            }
        }
        swipeContainer.setRefreshing(false);
        swipeToRefresh = false;
        mListView.setVisibility(View.VISIBLE);
        tempDealArrayList.clear();
        isLoading = false;
    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {}
    @Override
    public void onDownMotionEvent() {}
    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {
        if (scrollState == ScrollState.UP) {
            if (ab.isShowing()&&(mDealListAdapter.getCount()>5)) {
                ab.hide();
            }
            fab.hide();
        }else if (scrollState == ScrollState.DOWN) {
            if (!ab.isShowing()&&(mDealListAdapter.getCount()>5)) {
                ab.show();
            }
            fab.show();
        }
    }

    public static DealsFragment newInstance(String type) {
        DealsFragment newFragment = new DealsFragment();
        Bundle args = new Bundle();
        args.putString("deal_category", type);
        newFragment.setArguments(args);
        return newFragment;
    }

    public static void setFavoredFromDealDetail(String id,String favored){
        Iterator iterator = dealArrayList.iterator();
        DealModel tempDeal;
        while (iterator.hasNext()) {
            tempDeal = (DealModel)iterator.next();
            if(tempDeal.id.equals(id)){
                dealArrayList.get(dealArrayList.indexOf(tempDeal)).favored = favored;
            }
        }
        mDealListAdapter.notifyDataSetChanged();
    }
    public static boolean isOnline() {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }
    public void onStop(){
        super.onStop();
    }

}