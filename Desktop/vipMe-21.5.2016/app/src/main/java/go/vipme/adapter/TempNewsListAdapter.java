package go.vipme.adapter;

/**
 * Created by OguzhanBasar on 18.03.2015.
 */

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import go.vipme.R;
import go.vipme.activity.NewsWebViewActivity;
import go.vipme.constant.GeneralValues;
import go.vipme.model.NewsModel;
import go.vipme.model.ServerResponse;
import go.vipme.util.CustomTextView;
import go.vipme.util.ServerUtilFunctions;

// Çözüm bulununca bu class silinecek
public class TempNewsListAdapter extends ArrayAdapter<NewsModel> {

    Context context;
    ArrayList<CustomTextView> tagList = new ArrayList();
    static ArrayList<NewsModel> newsModels = new ArrayList();

    ImageView favoriteIcon;
    CustomTextView favoriteText,tag;

    public TempNewsListAdapter(Context context, int textViewResourceId, ArrayList<NewsModel> newsModels){

        super(context,textViewResourceId,newsModels);
        this.newsModels = newsModels;
        this.context = context;

    }

    @Override
    public View getView(final int position,View convertView,ViewGroup parent){

        View v = convertView;
        if(v==null) {

            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.news_list_item, null);

        }

        CustomTextView newsTitle;
        CustomTextView newsTitleV2;
        ImageView newsPhoto;
        CustomTextView summaryText;
        LinearLayout newsClickArea,newsFavorite,newsContainer=null,trashContainer2=null,tagContainer;
        RelativeLayout parentLayout;
        RelativeLayout trashContainer1=null;

        try {

            newsTitle = (CustomTextView) v.findViewById(R.id.news_title);
            newsTitleV2 = (CustomTextView) v.findViewById(R.id.news_title_v2);

            newsClickArea = (LinearLayout) v.findViewById(R.id.news_click_area);

            tag = (CustomTextView) v.findViewById(R.id.tag);

            switch (Integer.parseInt(newsModels.get(position).type)) {

                case 0:
                    newsTitle.setVisibility(View.VISIBLE);
                    newsTitle.setText(newsModels.get(position).title);
                    newsContainer = (LinearLayout) v.findViewById(R.id.news_type_0);
                    trashContainer1 = (RelativeLayout) v.findViewById(R.id.news_type_1);
                    trashContainer2 = (LinearLayout) v.findViewById(R.id.news_type_2);
                    summaryText = (CustomTextView) v.findViewById(R.id.news_text_type_0);
                    summaryText.setText(newsModels.get(position).summary);
                    try {
                        newsContainer.setVisibility(View.VISIBLE);
                        trashContainer1.setVisibility(View.GONE);
                        trashContainer2.setVisibility(View.GONE);
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                    break;
                case 1:
                    newsTitle.setVisibility(View.GONE);
                    newsTitleV2.setText(newsModels.get(position).title);
                    trashContainer1 = (RelativeLayout) v.findViewById(R.id.news_type_1);
                    newsContainer = (LinearLayout) v.findViewById(R.id.news_type_0);
                    trashContainer2 = (LinearLayout) v.findViewById(R.id.news_type_2);
                    newsPhoto = (ImageView) v.findViewById(R.id.news_image_type_1);
                    summaryText = (CustomTextView) v.findViewById(R.id.news_text_type_1);
                    summaryText.setText(newsModels.get(position).summary);
                    Picasso.with(context).load(newsModels.get(position).photoUrl).into(newsPhoto);
                    try {
                        trashContainer1.setVisibility(View.VISIBLE);
                        newsContainer.setVisibility(View.GONE);
                        trashContainer2.setVisibility(View.GONE);
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                    break;
                case 2:
                    newsTitle.setVisibility(View.VISIBLE);
                    newsTitle.setText(newsModels.get(position).title);
                    newsContainer = (LinearLayout) v.findViewById(R.id.news_type_2);
                    trashContainer1 = (RelativeLayout) v.findViewById(R.id.news_type_1);
                    trashContainer2 = (LinearLayout) v.findViewById(R.id.news_type_0);
                    newsPhoto = (ImageView) v.findViewById(R.id.news_image_type_2);
                    //Log.e("width:",width+"");
                    //Resources resources = context.getResources();
                    //DisplayMetrics metrics = resources.getDisplayMetrics();
                    //temp = width;
                    //width = width-(int)(2*(context.getResources().getDimension(R.dimen.activity_vertical_margin)));
                    //width= width*(int)(metrics.densityDpi / 160f);
                    //Log.e("margin:",context.getResources().getDimension(R.dimen.activity_vertical_margin)+"");
                    //Log.e("width:",width+"");
                    //width=temp;
                    //newsPhoto.getLayoutParams().height=(width*4)/7;
                    summaryText = (CustomTextView) v.findViewById(R.id.news_text_type_2);
                    summaryText.setText(newsModels.get(position).summary);
                    try{
                        Picasso.with(context).load(newsModels.get(position).photoUrl).into(newsPhoto);
                    }catch(Exception e){e.printStackTrace();}
                    try {
                        newsContainer.setVisibility(View.VISIBLE);
                        trashContainer1.setVisibility(View.GONE);
                        trashContainer2.setVisibility(View.GONE);
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }
                    break;
                default:
                    break;

            }

            if (!newsModels.get(position).tagName.equals("")) {
                tag.setText(newsModels.get(position).tagName);
                tag.setTextSize(context.getResources().getDimension(R.dimen.news_list_tag_text_size));
                tag.setBackgroundResource(R.drawable.news_fragment_tag_bg);
                tag.setPadding(
                        context.getResources().getDimensionPixelOffset(R.dimen.tag_left_padding),
                        context.getResources().getDimensionPixelOffset(R.dimen.tag_top_padding),
                        context.getResources().getDimensionPixelOffset(R.dimen.tag_right_padding),
                        context.getResources().getDimensionPixelOffset(R.dimen.tag_bottom_padding)
                );

                if (newsModels.get(position).tagFollowing.equals("1")) {
                    v.setActivated(true);
                    tag.setTextColor(context.getResources().getColor(R.color.list_option_blue));
                } else {
                    v.setActivated(false);
                    tag.setTextColor(context.getResources().getColor(R.color.tag_bg));
                }

                tag.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        if (newsModels.get(position).tagFollowing.equals("1")) {
                            setBackgroundForAllSameKindTag(newsModels.get(position).tagId, "0");
                            newsModels.get(position).tagFollowing = "0";
                            setTagFollow(newsModels.get(position).tagId, "0");
                            notifyDataSetChanged();
                        } else {
                            setBackgroundForAllSameKindTag(newsModels.get(position).tagId, "1");
                            newsModels.get(position).tagFollowing = "1";
                            setTagFollow(newsModels.get(position).tagId, "1");
                            notifyDataSetChanged();
                        }
                    }

                });

            } else {
                tag.setText("");
                tag.setBackgroundResource(R.drawable.tag_empty_bg);
                tag.setPadding(0, 0, 0, 0);
                tag.setOnClickListener(null);
            }

            tagList.add(tag);

            newsClickArea.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, NewsWebViewActivity.class);
                    //intent.putExtra("url", newsModels.get(position).newsUrl);
                    intent.putExtra("news_id", newsModels.get(position).id);
                   /*intent.putExtra("favored", newsModels.get(position).favored);
                    intent.putExtra("liked", newsModels.get(position).liked);
                    intent.putExtra("disliked", newsModels.get(position).disliked);
                    intent.putExtra("title", newsModels.get(position).title);*/
                    //setRead(newsModels.get(position).id);
                    context.startActivity(intent);
                }

            });

            if (newsModels.get(position).favored.equals("1")) {
                favoriteIcon.setImageResource(R.drawable.favorite_icon_remove);
                favoriteText.setText("Çıkart");
                favoriteText.setTextColor(context.getResources().getColor(R.color.list_option_blue));
                //shareText.setTextColor(context.getResources().getColor(R.color.list_option_blue));
            } else if (newsModels.get(position).favored.equals("0")) {
                //shareIcon.setImageResource(R.drawable.share_icon_non_favored);
                favoriteIcon.setImageResource(R.drawable.favorite_icon_add);
                favoriteText.setText("Ekle");
                favoriteText.setTextColor(context.getResources().getColor(R.color.tag_bg));
                //shareText.setTextColor(context.getResources().getColor(R.color.tag_bg));
            }
//
//            newsFavorite.setOnClickListener(new View.OnClickListener() {
//
//                @Override
//                public void onClick(View view) {
//                    String favoriteCount = PreferenceDao.getValue("favorite_counter");
//                    if (favoriteCount.equals("")) {
//                        PreferenceDao.setValue("favorite_counter", "1");
//                    } else {
//                        PreferenceDao.setValue("favorite_counter", String.valueOf(Integer.parseInt(favoriteCount) + 1));
//                    }
//                    favoriteCount = PreferenceDao.getValue("favorite_counter");
//                    Log.e("onClick()", "tıklantı:");
//                    if (newsModels.get(position).favored.equals("1")) {
//                        Log.e("onClick()", "favored:" + newsModels.get(position).favored);
//                        favoriteIcon.setImageResource(R.drawable.favorite_icon_add);
//                        favoriteText.setText("Ekle");
//                        favoriteText.setTextColor(context.getResources().getColor(R.color.tag_bg));
//                        newsModels.get(position).favored = "0";
//                        sendFavoredDataToServer(newsModels.get(position).id, "0");
//                        try{
////                            ((FavoritesFragment) (MyApplication.getFavoritesFragment())).removeNewsFromList(newsModels.get(position).id);
//                        }catch(Exception e){e.printStackTrace();}
//                        notifyDataSetChanged();
//                        if (Integer.parseInt(favoriteCount) <= 5) {
//                            Toast.makeText(MyApplication.getAppContext(), "Favorilerden çıkarıldı", Toast.LENGTH_SHORT).show();
//                        }
//                    } else if (newsModels.get(position).favored.equals("0")) {
//                        Log.e("onClick()", "favored:" + newsModels.get(position).favored);
//                        favoriteIcon.setImageResource(R.drawable.favorite_icon_remove);
//                        favoriteText.setText("Çıkart");
//                        favoriteText.setTextColor(context.getResources().getColor(R.color.list_option_blue));
//                        newsModels.get(position).favored = "1";
//                        sendFavoredDataToServer(newsModels.get(position).id, "1");
//                        notifyDataSetChanged();
//                        if (Integer.parseInt(favoriteCount) <= 5) {
//                            Toast.makeText(MyApplication.getAppContext(), "Favorilere eklendi", Toast.LENGTH_SHORT).show();
//                        }
//                    }
//
//                }
//
//            });
        }catch(Exception e){e.printStackTrace();}
        return v;

    }

    private void sendFavoredDataToServer(String newsId, String isFavored){
        final List<NameValuePair> mParams = new ArrayList<>();
        mParams.add(new BasicNameValuePair("news_id", newsId));
        mParams.add(new BasicNameValuePair("favored", isFavored));

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                ServerResponse s = ServerUtilFunctions.callServer(GeneralValues.BASE_URL + "news_favored", mParams, true);
            }
        });
        thread.start();

    }

    private void setTagFollow(final String tagId,final String following){

        Thread thread = new Thread(new Runnable() {
            String function="";
            @Override
            public void run() {

                final List<NameValuePair> mParams = new ArrayList<>();
                function="tag_following";
                mParams.add(new BasicNameValuePair("tag_id", tagId));
                mParams.add(new BasicNameValuePair("following", following));

                ServerResponse s = ServerUtilFunctions.callServer(GeneralValues.BASE_URL + function, mParams, true);

            }
        });
        thread.start();
    }

    private void setRead(final String newsId){

        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {

                final List<NameValuePair> mParams = new ArrayList<>();
                mParams.add(new BasicNameValuePair("news_id", newsId));

                ServerResponse s = ServerUtilFunctions.callServer(GeneralValues.BASE_URL + "news", mParams, true);

            }
        });
        thread.start();
    }

    private void setBackgroundForAllSameKindTag(String tagId,String favored){
        Iterator iterator = newsModels.iterator();
        NewsModel tempTag;
        while (iterator.hasNext()) {
            tempTag = (NewsModel)iterator.next();
            if(tempTag.tagId.equals(tagId)){
                newsModels.get(newsModels.indexOf(tempTag)).tagFollowing = favored;
            }
        }
    }

}
