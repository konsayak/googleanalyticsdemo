package go.vipme.dao;

import android.util.Log;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import go.vipme.constant.GeneralValues;
import go.vipme.model.NewsModel;
import go.vipme.model.ServerResponse;
import go.vipme.util.JsonFunctions;
import go.vipme.util.MyApplication;
import go.vipme.util.ServerUtilFunctions;

public class NewsesDao {

    public static ArrayList<NewsModel> getNewsesFromServer(String function,int page,String type,String categoryId,String sinceDate,String keyword){
        ArrayList<NewsModel> newsArrayList= null;
        String mPage = "";
        if (page != -1) {
            mPage = Integer.toString(page);
        }
        final List<NameValuePair> mParams = new ArrayList<>();
        mParams.add(new BasicNameValuePair("page", mPage));
        mParams.add(new BasicNameValuePair("type", type));
        mParams.add(new BasicNameValuePair("category_id", categoryId));
        mParams.add(new BasicNameValuePair("since_date", sinceDate));
        mParams.add(new BasicNameValuePair("keyword", keyword));

        try {
            ServerResponse s = ServerUtilFunctions.callServer(GeneralValues.BASE_URL + function, mParams, true);
            if(s.success) {
                newsArrayList = new ArrayList<>();
                JSONObject mJSONObject = JsonFunctions.getJsonObjectFromObject(s.json, "data");

                newsArrayList= newsJsonParse(JsonFunctions.getArrayFromObject(mJSONObject, "newses"));
                PreferenceDao.setValue("user_id", JsonFunctions.getStringValue(mJSONObject, "user_id"));
            }else{
                Log.e("", "NewsesDao - getNewsesFromServer() - ServerResponse : " + s.error_message);
            }
        }catch(Exception e){e.printStackTrace();}

        return newsArrayList;

    }

    private static ArrayList<NewsModel> newsJsonParse(JSONArray jsonArray){

        NewsModel newsModel;
        ArrayList<NewsModel> mNewsList = new ArrayList<>();
        int counter=0;

        while(jsonArray!=null && counter!=jsonArray.length()){
            newsModel = new NewsModel();
            try {
                newsModel.id = jsonArray.getJSONObject(counter).getString("id");
                newsModel.summary = jsonArray.getJSONObject(counter).getString("summary");
                newsModel.title = jsonArray.getJSONObject(counter).getString("title");
                newsModel.newsUrl = jsonArray.getJSONObject(counter).getString("news_url");
                newsModel.type = jsonArray.getJSONObject(counter).getString("type");
                newsModel.photoUrl = jsonArray.getJSONObject(counter).getString("photo_url");
                newsModel.favored = jsonArray.getJSONObject(counter).getString("favored");
                newsModel.tagId = jsonArray.getJSONObject(counter).getString("tag_id");
                newsModel.tagName = jsonArray.getJSONObject(counter).getString("tag_name");
                newsModel.tagFollowing = jsonArray.getJSONObject(counter).getString("tag_following");
                newsModel.liked = jsonArray.getJSONObject(counter).getString("liked");
                newsModel.disliked = jsonArray.getJSONObject(counter).getString("disliked");
                mNewsList.add(newsModel);
            }catch (JSONException e){e.printStackTrace();}
            counter++;
        }

        return mNewsList;

    }


}
