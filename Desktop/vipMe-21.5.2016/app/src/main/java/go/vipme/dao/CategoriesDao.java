package go.vipme.dao;

import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import go.vipme.constant.GeneralValues;
import go.vipme.model.NavigationDrawerListItemModel;
import go.vipme.model.ServerResponse;
import go.vipme.util.JsonFunctions;
import go.vipme.util.ServerUtilFunctions;

public class CategoriesDao {

    private static ArrayList<NavigationDrawerListItemModel> categoryArray = null;

    public static ArrayList<NavigationDrawerListItemModel> getCategories(String type) {

        final List<NameValuePair> mParams = new ArrayList<>();
        mParams.add(new BasicNameValuePair("type", type));
        try {
            ServerResponse s = ServerUtilFunctions.callServer(GeneralValues.BASE_URL + "categories", mParams, true);
            if (s.success) {
                categoryArray = new ArrayList<>();
                categoryArray = navItemJsonParse(JsonFunctions.getArrayFromObject(JsonFunctions.getJsonObjectFromObject(s.json, "data"), "categories"));
            }else{
                Log.e("", "CategoriesDao - getCategories() - ServerResponse : " + s.error_message);
            }
        }catch(Exception e){e.printStackTrace();}
        return categoryArray;
    }

    public static ArrayList<NavigationDrawerListItemModel> navItemJsonParse(JSONArray jsonArray){

        NavigationDrawerListItemModel listItemModel;
        ArrayList<NavigationDrawerListItemModel> mItemList = new ArrayList<>();
        int counter=0;

        while(jsonArray!=null && counter!=jsonArray.length()){
            listItemModel = new NavigationDrawerListItemModel();
            try {
                listItemModel.id    = jsonArray.getJSONObject(counter).getString("id");
                listItemModel.name  = jsonArray.getJSONObject(counter).getString("name");
                listItemModel.photo = jsonArray.getJSONObject(counter).getString("photo");
                mItemList.add(listItemModel);
            }catch (JSONException e){e.printStackTrace();}
            counter++;
        }

        return mItemList;

    }

}
