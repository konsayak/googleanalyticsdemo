package go.vipme.dao;

import android.app.Activity;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import go.vipme.constant.GeneralValues;
import go.vipme.model.NewsModel;
import go.vipme.model.ServerResponse;
import go.vipme.util.JsonFunctions;
import go.vipme.util.ServerUtilFunctions;

/**
 * Created by Halit on 1.9.2015.
 */
public class SingleNewsDao  {


    public static NewsModel getNewFromServer(String newsId){

        NewsModel mNewsModel=null;
        final List<NameValuePair> mParams=new ArrayList<>();
        mParams.add(new BasicNameValuePair("news_id",newsId));

        try{
            ServerResponse s= ServerUtilFunctions.callServer(GeneralValues.BASE_URL + "news", mParams, true);
            mNewsModel=new NewsModel();
            JSONObject jObj= JsonFunctions.getJsonObjectFromObject(s.json,"data");
            mNewsModel.newsUrl=JsonFunctions.getStringValue(jObj,"url");
            mNewsModel.summary=JsonFunctions.getStringValue(jObj,"summary");
            mNewsModel.favored=JsonFunctions.getStringValue(jObj,"favored");
            mNewsModel.photoUrl=JsonFunctions.getStringValue(jObj,"photo_url");
            mNewsModel.liked=JsonFunctions.getStringValue(jObj,"liked");
            mNewsModel.title=JsonFunctions.getStringValue(jObj,"title");
            mNewsModel.disliked=JsonFunctions.getStringValue(jObj,"disliked");
            mNewsModel.id=JsonFunctions.getStringValue(jObj,"id");
            mNewsModel.shareUrl = JsonFunctions.getStringValue(jObj,"share_url");


        }
        catch (Exception e){
            e.printStackTrace();
        }
        return mNewsModel;
    }
}
