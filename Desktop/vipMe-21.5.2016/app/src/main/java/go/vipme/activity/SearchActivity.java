package go.vipme.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;

import java.util.ArrayList;

import go.vipme.R;
import go.vipme.adapter.CategoryListAdapter;
import go.vipme.adapter.DealListAdapter;
import go.vipme.adapter.NewsListAdapter;
import go.vipme.dao.CategoriesDao;
import go.vipme.dao.DealsDao;
import go.vipme.dao.NewsesDao;
import go.vipme.dao.PreferenceDao;
import go.vipme.interfaces.CategoryItemListener;
import go.vipme.model.DealModel;
import go.vipme.model.NavigationDrawerListItemModel;
import go.vipme.model.NewsModel;
import go.vipme.util.CustomTextView;

/**
 * Created by Egemen on 30.01.2016.
 */
public class SearchActivity extends Activity implements ObservableScrollViewCallbacks {



    public static ArrayList<DealModel> tempDealArrayList = new ArrayList<>();
    public static ArrayList<NavigationDrawerListItemModel> tempCategoryArray = new ArrayList<>();
    public static ArrayList<NewsModel> tempNewsArrayList = new ArrayList<>();
    String isDeal="news";

    public static NewsListAdapter newsListAdapter;
    public static DealListAdapter mDealListAdapter;
    public static CategoryListAdapter mCategoryListAdapter;
    CategoryItemListener categoryItemListener;
    EditText search;
    TextView kategoriText,notFoundText;
    View searchTitleLine;


    private static boolean mKeyboardStatus;

    static ListView mListView;
    static boolean isLoading = false;

    public static int pageCounter = 1;
    LinearLayout ab;
    Boolean firstOpen=true;
    RelativeLayout loadingPanel;
    Boolean searchDeal, isKeyboardOpen;
    Boolean searchCategory=true;
    final static Handler mHandler = new Handler();
    final  Runnable mDealsUpdateResults = new Runnable() {
        public void run() {
            setDealsUI();
        }
    };
    final Runnable mCategoryUpdateResults = new Runnable() {
        public void run() {
            setCategoryUI();
        }
    };

    final Runnable mUpdateResults = new Runnable() {
        public void run() {
            setUI();
        }
    };
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        mListView                           = (ListView)findViewById(R.id.activity_search_listView);
        ab                                  = (LinearLayout)findViewById(R.id.activity_search_top_bar);
        ImageButton backButton              = (ImageButton)findViewById(R.id.back_button_activity_search);
        final CustomTextView indirimler     = (CustomTextView)findViewById(R.id.deals_button_search);
        final CustomTextView haberler       = (CustomTextView)findViewById(R.id.news_button_search);
        search                              = (EditText)findViewById(R.id.search_editText);
        kategoriText                        = (TextView)findViewById(R.id.kategoriler_text);
        searchTitleLine                     = (View)findViewById(R.id.search_title_line);
        loadingPanel                        = (RelativeLayout)findViewById(R.id.loadingPanel);
        notFoundText                        = (TextView)findViewById(R.id.sonuc_bulunamadi_text);
        final InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);

        categoryItemListener = new CategoryItemListener() {
            @Override
            public void OnItemClick(String categoryId) {

                if (searchCategory){
                    getDealsData("deals", pageCounter, categoryId, "");

                    kategoriText.setVisibility(View.GONE);
                    searchTitleLine.setVisibility(View.GONE);
                }else{
                    getData("newses", pageCounter,categoryId , "");

                    kategoriText.setVisibility(View.GONE);
                    searchTitleLine.setVisibility(View.GONE);
                }
            }
        };

        mCategoryListAdapter    =   new CategoryListAdapter(SearchActivity.this,R.layout.list_item,tempCategoryArray,categoryItemListener);
        newsListAdapter         =   new NewsListAdapter(SearchActivity.this,R.layout.news_list_item,tempNewsArrayList);
        mDealListAdapter        =   new DealListAdapter(SearchActivity.this,R.layout.list_item,tempDealArrayList);

        Intent i        = getIntent();
        isDeal         =i.getStringExtra("isDeal");
        //fragment=i.getStringExtra("fragment");
        final String searchResutText=getResources().getString(R.string.arama_sonuc);

        search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    // performSearch();
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);
                    isKeyboardOpen=true;
                    if (searchDeal) {
                        kategoriText.setText("\""+search.getText().toString()+"\""+" "+searchResutText);
                        getDealsData("deals", pageCounter, "", search.getText().toString());
                    } else {
                        kategoriText.setText("\""+search.getText().toString()+"\""+" "+searchResutText);
                        getData("newses", pageCounter, "news_category", search.getText().toString());
                    }

                }
                return false;
            }
        });


        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isKeyboardActive()){
                    showKeyboard();
                }else{
                    dismissKeyboard();
                }
                onBackPressed();
                PreferenceDao.hideSoftKeyboard(SearchActivity.this,search);




                }



        });





        if(isDeal.equals("deal")){
            searchDeal=true;
            indirimler.setTextColor(getResources().getColor(R.color.close_stores_bg));
            haberler.setTextColor(getResources().getColor(R.color.light_grey));
            mListView.setAdapter(mCategoryListAdapter);
            getCategoriesData("brand");
            search.setHint(getResources().getString(R.string.indirim_ara));
            firstOpen=false;
        }else{
            searchDeal=false;
            searchCategory=false;
            indirimler.setTextColor(getResources().getColor(R.color.light_grey));
            haberler.setTextColor(getResources().getColor(R.color.close_stores_bg));
            mListView.setAdapter(mCategoryListAdapter);
            getCategoriesData("tag");
            search.setHint(getResources().getString(R.string.haber_ara));
            firstOpen=false;
        }



        isKeyboardOpen = true;

        haberler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                indirimler.setTextColor(getResources().getColor(R.color.light_grey));
                haberler.setTextColor(getResources().getColor(R.color.close_stores_bg));
                searchDeal = false;
                searchCategory = false;
                search.setHint(getResources().getString(R.string.haber_ara));
                search.setText("");
                kategoriText.setVisibility(View.VISIBLE);
                searchTitleLine.setVisibility(View.VISIBLE);
                kategoriText.setText(getResources().getString(R.string.kateriler));
                getCategoriesData("tag");
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                isKeyboardOpen = true;
                notFoundText.setVisibility(View.GONE);




            }
        });

        indirimler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                indirimler.setTextColor(getResources().getColor(R.color.close_stores_bg));
                haberler.setTextColor(getResources().getColor(R.color.light_grey));
                searchDeal = true;
                searchCategory = true;
                kategoriText.setVisibility(View.VISIBLE);
                searchTitleLine.setVisibility(View.VISIBLE);
                kategoriText.setText(getResources().getString(R.string.kateriler));
                getCategoriesData("brand");
                search.setText("");
                search.setHint(getResources().getString(R.string.indirim_ara));
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                isKeyboardOpen = true;
                notFoundText.setVisibility(View.GONE);
            }
        });


        //mListView.notify();

    }

    public void getCategoriesData(final String category) {

        if(!isLoading) {
            mListView.setVisibility(View.GONE);
        }
        loadingPanel.setVisibility(View.VISIBLE);
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    tempCategoryArray = CategoriesDao.getCategories(category);
                    if(tempCategoryArray!=null) {
                        mHandler.post(mCategoryUpdateResults);
                    }
                }catch (Exception e){e.printStackTrace();}
            }
        });
        thread.start();

    }


    public void getData(final String function,final int page,final String category, final String _keyword) {

        if(!isLoading) {
            mListView.setVisibility(View.GONE);
        }
        loadingPanel.setVisibility(View.VISIBLE);
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    tempNewsArrayList = NewsesDao.getNewsesFromServer(function, page, "all", category, "", _keyword);
                    if(tempNewsArrayList!=null) {
                        mHandler.post(mUpdateResults);
                    }
                }catch(Exception e){e.printStackTrace();}
            }

        });
        thread.start();
    }

    public void getDealsData(final String function, final int page, final String category,final String _keyword) {
        if(!isLoading) {
            mListView.setVisibility(View.GONE);
        }
        loadingPanel.setVisibility(View.VISIBLE);
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    tempDealArrayList = DealsDao.getDealsFromServer(function, page, "all", "", category, "", _keyword);
                    if(tempDealArrayList!=null) {
                        mHandler.post(mDealsUpdateResults);

                    }

                }catch (Exception e){e.printStackTrace();}
            }
        });
        thread.start();
    }

    private  void setCategoryUI(){

        mCategoryListAdapter = new CategoryListAdapter(SearchActivity.this, R.layout.list_item, tempCategoryArray, categoryItemListener);
        mListView.setAdapter(mCategoryListAdapter);

        mListView.setSelection(0);
        loadingPanel.setVisibility(View.GONE);
        mListView.setVisibility(View.VISIBLE);
        isLoading = false;
    }

    private void setDealsUI() {
        mDealListAdapter = new DealListAdapter(SearchActivity.this, R.layout.list_item, tempDealArrayList);
        mListView.setAdapter(mDealListAdapter);
        if(tempDealArrayList.size()==0){
            notFoundText.setVisibility(View.VISIBLE);
        }else{
            notFoundText.setVisibility(View.GONE);
        }
        mListView.setSelection(0);
        loadingPanel.setVisibility(View.GONE);
        mListView.setVisibility(View.VISIBLE);
        isLoading = false;
    }

    private void setUI(){

        newsListAdapter = new NewsListAdapter(SearchActivity.this, R.layout.news_list_item, tempNewsArrayList);
        mListView.setAdapter(newsListAdapter);
        if(tempNewsArrayList.size()==0){
            notFoundText.setVisibility(View.VISIBLE);
        }else{
            notFoundText.setVisibility(View.GONE);
        }
        mListView.setSelection(0);
        loadingPanel.setVisibility(View.GONE);
        mListView.setVisibility(View.VISIBLE);
        isLoading = false;
    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {}
    @Override
    public void onDownMotionEvent() {}
    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {
        if (scrollState == ScrollState.UP) {
            if (ab.getVisibility()==View.VISIBLE&&(mDealListAdapter.getCount()>5)) {
                ab.setVisibility(View.GONE);

            }
        }else if (scrollState == ScrollState.DOWN) {
            if (ab.getVisibility()==View.GONE&&(mDealListAdapter.getCount()>5)) {
                ab.setVisibility(View.VISIBLE);

            }
        }
    }

    private void dismissKeyboard(){
        InputMethodManager imm =(InputMethodManager)this.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        mKeyboardStatus = false;
    }

    private void showKeyboard(){
        InputMethodManager imm =(InputMethodManager)this.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
        mKeyboardStatus = true;
    }

    private boolean isKeyboardActive(){
        return mKeyboardStatus;
    }


    @Override
    public void onBackPressed() {
        finish();

    }

}