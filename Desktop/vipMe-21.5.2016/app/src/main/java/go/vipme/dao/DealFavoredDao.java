package go.vipme.dao;

import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

import go.vipme.constant.GeneralValues;
import go.vipme.model.ServerResponse;
import go.vipme.util.ServerUtilFunctions;

/**
 * Created by AhmetOguzhanBasar on 25.06.2015.
 */
public class DealFavoredDao {

    public static void sendFavoredDataToServer(String dealId, String isFavored){
        final List<NameValuePair> mParams = new ArrayList<>();
        mParams.add(new BasicNameValuePair("deal_id", dealId));
        mParams.add(new BasicNameValuePair("favored", isFavored));

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    ServerResponse s = ServerUtilFunctions.callServer(GeneralValues.BASE_URL + "deal_favored", mParams, true);
                    if(!s.success){
                        Log.e("", "DealFavoredDao - sendFavoredDataToServer() - ServerResponse : " + s.error_message);
                    }
                }catch(Exception e){e.printStackTrace();}
            }
        });
        thread.start();

    }

}
