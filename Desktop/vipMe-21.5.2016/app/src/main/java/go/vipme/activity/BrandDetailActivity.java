package go.vipme.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import go.vipme.R;
import go.vipme.adapter.DealListAdapter;
import go.vipme.constant.GeneralValues;
import go.vipme.dao.BrandDao;
import go.vipme.dao.DealsDao;
import go.vipme.dao.TagFollowDao;
import go.vipme.model.BrandModel;
import go.vipme.model.DealModel;
import go.vipme.util.CustomTextView;
import go.vipme.util.Dialogs;
import go.vipme.util.InternetStatus;
import go.vipme.util.MyApplication;

/**
 * Created by AhmetOguzhanBasar on 30.06.2015.
 */
public class BrandDetailActivity extends Activity {

    BrandModel mBrandModel;
    ImageView brandPhoto,favoriteIcon;
    CustomTextView brandName,infoText,favoriteText;
    LinearLayout followButton;
    RelativeLayout closeStores;
    ListView list;
    SwipeRefreshLayout swipeContainer;

    final Handler mHandler = new Handler();
    final Runnable mUpdateResults = new Runnable() {
        public void run() {
            setUI();
        }
    };

    public static DealListAdapter mDealListAdapter;

    private static ArrayList<DealModel> dealArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brand_detail);

        LayoutInflater headerInflater       = getLayoutInflater();
        ViewGroup header                    = (ViewGroup)headerInflater.inflate(R.layout.brand_detail_header, list, false);

        ImageView backButton                = (ImageView)header.findViewById(R.id.back_button);
        ImageView shareButton               = (ImageView)header.findViewById(R.id.share_button);
        favoriteIcon                        = (ImageView)header.findViewById(R.id.favorite_icon);
        brandPhoto                          = (ImageView)header.findViewById(R.id.brand_photo);
        brandName                           = (CustomTextView)header.findViewById(R.id.brand_name);
        infoText                            = (CustomTextView)header.findViewById(R.id.info_text);
        favoriteText                        = (CustomTextView)header.findViewById(R.id.favorite_text);
        followButton                        = (LinearLayout)header.findViewById(R.id.favorite_button);
        closeStores                         = (RelativeLayout)findViewById(R.id.close_stores);
        list                                = (ListView)findViewById(R.id.list);
        swipeContainer                      = (SwipeRefreshLayout)findViewById(R.id.swipeContainer);

        Intent intent                       = getIntent();
        final Context context               = this;
        final String brandId                = intent.getStringExtra("brand_id");

        list.addHeaderView(header, null, false);

        swipeContainer.setProgressViewOffset(false, 0, (int) (getResources().getDimension(R.dimen.top_padding_caused_by_action_bar)));
        swipeContainer.setColorSchemeResources(
                R.color.theme_blue,
                R.color.tag_bg,
                R.color.male_blue);

        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                list.setVisibility(View.GONE);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getData(brandId);
                    }
                }, 800);
            }
        });
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (InternetStatus.getInstance(BrandDetailActivity.this).isOnline()) {
                    if (followButton.isActivated()) {
                        DealDetailActivity.followButton.setActivated(true);
                        DealDetailActivity.favoriteIcon.setImageResource(R.drawable.check_v2);
                        DealDetailActivity.favoriteText.setText("Takibi Bırak");
                        DealDetailActivity.favoriteText.setTextColor(getResources().getColor(R.color.brand_background));

                    }
                    else {
                        DealDetailActivity.followButton.setActivated(false);
                        DealDetailActivity.favoriteIcon.setImageResource(R.drawable.plus_v2);
                        DealDetailActivity.favoriteText.setText("Takip Et");
                        DealDetailActivity.favoriteText.setTextColor(getResources().getColor(R.color.tag_bg));


                    }
                    onBackPressed();
                    finish();
                }
                else{
                    onBackPressed();

                }
            }
        });
        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Paylaşım seçenekleri hazırlanıyor.", Toast.LENGTH_SHORT).show();
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "text girilecek");
                sendIntent.setType("text/plain");
                context.startActivity(Intent.createChooser(sendIntent, "İndirimi Paylaş"));
            }
        });
        followButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (InternetStatus.getInstance(BrandDetailActivity.this).isOnline()) {
                    if (followButton.isActivated()) {
                        followButton.setActivated(false);
                        favoriteIcon.setImageResource(R.drawable.plus_v2);
                        favoriteText.setText("Takip Et");
                        favoriteText.setTextColor(getResources().getColor(R.color.tag_bg));
                        TagFollowDao.setTagFollow("brand_following", mBrandModel.id, "0");

                    } else {
                        followButton.setActivated(true);
                        favoriteIcon.setImageResource(R.drawable.check_v2);
                        favoriteText.setText("Takibi Bırak");
                        favoriteText.setTextColor(getResources().getColor(R.color.brand_background));
                        TagFollowDao.setTagFollow("brand_following", mBrandModel.id, "1");

                    }
                }
                else{
                    Dialogs.InternetControlDialog(BrandDetailActivity.this);
                }
            }
        });
        closeStores.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View button) {
                if (InternetStatus.getInstance(BrandDetailActivity.this).isOnline()) {
                    Intent intent = new Intent(BrandDetailActivity.this, CloseStoreActivity.class);
                    intent.putExtra("brand_id", mBrandModel.id);
                    intent.putExtra("brand_name", mBrandModel.name);
                    intent.putExtra("calling_activity", GeneralValues.BRAND_DETAIL_ACTIVITY);

                    startActivity(intent);

                }
                else{
                    Dialogs.InternetControlDialog(BrandDetailActivity.this);
                }
            }
        });

        getData(brandId);

    }

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance(this).activityStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance(this).activityStop(this);
    }
    public void onBackPressed() {
        super.onBackPressed();
        if (InternetStatus.getInstance(BrandDetailActivity.this).isOnline()) {
            if (followButton.isActivated()) {
                DealDetailActivity.followButton.setActivated(true);
                DealDetailActivity.favoriteIcon.setImageResource(R.drawable.check_v2);
                DealDetailActivity.favoriteText.setText("Takibi Bırak");
                DealDetailActivity.favoriteText.setTextColor(getResources().getColor(R.color.brand_background));

            } else {
                DealDetailActivity.followButton.setActivated(false);
                DealDetailActivity.favoriteIcon.setImageResource(R.drawable.plus_v2);
                DealDetailActivity.favoriteText.setText("Takip Et");
                DealDetailActivity.favoriteText.setTextColor(getResources().getColor(R.color.tag_bg));


            }


        }

    }

    private void getData(final String brandId){
        swipeContainer.setRefreshing(true);
        list.setVisibility(View.GONE);

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    mBrandModel     = new BrandModel();
                    mBrandModel     = BrandDao.getBrandFromServer(brandId);
                    dealArrayList   = DealsDao.getDealsFromServer("deals", -1, "all", mBrandModel.id, "", "","");

                    if(mBrandModel!=null) {
                        mHandler.post(mUpdateResults);
                    }
                }catch(Exception e){e.printStackTrace();}
            }
        });
        thread.start();
    }

    private void setUI(){
        try {
            swipeContainer.setRefreshing(false);
            list.setVisibility(View.VISIBLE);
            Picasso.with(this).load(mBrandModel.brandPhoto).into(brandPhoto);
            brandName.setText(mBrandModel.name);
            infoText.setText(getResources().getString(R.string.brand_info_text, mBrandModel.storeCount, mBrandModel.dealCount, mBrandModel.category));
            if (InternetStatus.getInstance(BrandDetailActivity.this).isOnline()) {
                if (mBrandModel.following.equals("1")) {
                    followButton.setActivated(true);
                    favoriteIcon.setImageResource(R.drawable.check_v2);
                    favoriteText.setText("Takibi Bırak");
                    favoriteText.setTextColor(getResources().getColor(R.color.brand_background));
                } else {
                    followButton.setActivated(false);
                    favoriteIcon.setImageResource(R.drawable.plus_v2);
                    favoriteText.setText("Takip Et");
                    favoriteText.setTextColor(getResources().getColor(R.color.tag_bg));
                }
                if (dealArrayList != null) {
                    mDealListAdapter = new DealListAdapter(this, R.layout.list_item, dealArrayList);
                    list.setAdapter(mDealListAdapter);
                }
            }

        }
        catch (Exception e){
            Log.e(MyApplication.TAG,"BrandDetailActivity setUI error");
        }


    }

}
