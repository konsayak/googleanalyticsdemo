package go.vipme.dao;

import android.content.Context;
import android.nfc.Tag;
import android.support.v4.app.Fragment;
import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import go.vipme.constant.GeneralValues;
import go.vipme.fragment.FollowContainerFragment;
import go.vipme.fragment.TagContainerFragment;
import go.vipme.model.NewsModel;
import go.vipme.model.ServerResponse;
import go.vipme.model.TagModel;
import go.vipme.util.JsonFunctions;
import go.vipme.util.MyApplication;
import go.vipme.util.ServerUtilFunctions;

/**
 * Created by AhmetOguzhanBasar on 24.06.2015.
 */
public class TagDao {

    public static ArrayList<TagModel> getTagsFromServer(Fragment fragment, String callingPage, String function, int page, String type,String categoryId, String keyword){

        String mPage="";
        JSONArray mJsonArray;
        JSONObject mJSONObject;
        ArrayList<TagModel> mTagList = null;
        if(page!=-1){mPage=Integer.toString(page);}
        final List<NameValuePair> mParams = new ArrayList<>();
        mParams.add(new BasicNameValuePair("page", mPage));
        mParams.add(new BasicNameValuePair("type", type));
        mParams.add(new BasicNameValuePair("keyword", keyword));
        mParams.add(new BasicNameValuePair("since", ""));
        mParams.add(new BasicNameValuePair("category_id", categoryId));

        try {
            ServerResponse s = ServerUtilFunctions.callServer(GeneralValues.BASE_URL + function, mParams, true);
            Log.e(MyApplication.TAG,"ServerUtilFunctions function="+function);
            if(s.success){
                mJSONObject = JsonFunctions.getJsonObjectFromObject(s.json, "data");
                mJsonArray = JsonFunctions.getArrayFromObject(mJSONObject, function);
                mTagList = new ArrayList<TagModel>();
                mTagList = tagJsonParse(mJsonArray,mTagList);


                if (function.equals("brands")) {
                    int brandCount = Integer.parseInt(JsonFunctions.getStringValue(mJSONObject, "user_brand_count"));
                    if(brandCount<0){
                        brandCount=0;
                    }
                    try {
                        if(callingPage.equals("UserFavored")){
                            ((FollowContainerFragment)fragment).userBrandFollowingCount = brandCount;
                        }else{
                            ((TagContainerFragment)fragment).userBrandFollowingCount = brandCount;
                        }
                    }catch(Exception e){e.printStackTrace();}
                } else if (function.equals("tags")) {
                    int tagCount = Integer.parseInt(JsonFunctions.getStringValue(mJSONObject, "user_tag_count"));
                    if(tagCount<0){
                        tagCount=0;
                    }
                    try {
                        if(callingPage.equals("UserFavored")){
                            ((FollowContainerFragment)fragment).userTagFollowingCount = tagCount;
                        }else{
                            ((TagContainerFragment)fragment).userTagFollowingCount = tagCount;
                        }
                    }catch(Exception e){e.printStackTrace();}
                }
            } else {
                Log.e("", "TagDao - getTagsFromServer() - ServerResponse : " + s.error_message);
            }
        }catch (Exception e){e.printStackTrace();}
        return mTagList;
    }

    private static ArrayList<TagModel> tagJsonParse(JSONArray jsonArray,ArrayList<TagModel> tagList){

        TagModel tagModel;
//        ArrayList<TagModel> mTagList = new ArrayList<>();
        int counter=0;

        while(jsonArray!=null && counter!=jsonArray.length()){
            tagModel = new TagModel();
            try {
                tagModel.brandName = jsonArray.getJSONObject(counter).getString("name");
                tagModel.tagId = jsonArray.getJSONObject(counter).getString("id");
                tagModel.following = jsonArray.getJSONObject(counter).getString("following");
                tagList.add(tagModel);
            }catch (JSONException e){e.printStackTrace();}
            counter++;
        }

        return tagList;

    }

}
