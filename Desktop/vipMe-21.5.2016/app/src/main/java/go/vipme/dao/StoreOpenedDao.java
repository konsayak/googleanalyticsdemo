package go.vipme.dao;

import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

import go.vipme.constant.GeneralValues;
import go.vipme.model.ServerResponse;
import go.vipme.util.ServerUtilFunctions;

/**
 * Created by AhmetOguzhanBasar on 25.06.2015.
 */
public class StoreOpenedDao {

    public static void sendStoreDataToServer(String dealId, String storeId){
        final List<NameValuePair> mParams = new ArrayList<>();
        mParams.add(new BasicNameValuePair("deal_id", dealId));
        mParams.add(new BasicNameValuePair("store_id", storeId));

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    ServerResponse s = ServerUtilFunctions.callServer(GeneralValues.BASE_URL + "store", mParams, true);
                    if(!s.success){
                        Log.e("", "StoreOpenedDao - sendStoreDataToServer() - ServerResponse : " + s.error_message);
                    }
                }catch(Exception e){e.printStackTrace();}
            }
        });
        thread.start();

    }

}
