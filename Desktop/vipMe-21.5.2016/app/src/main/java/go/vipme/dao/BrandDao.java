package go.vipme.dao;

import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;
import go.vipme.constant.GeneralValues;
import go.vipme.model.BrandModel;
import go.vipme.model.ServerResponse;
import go.vipme.util.JsonFunctions;
import go.vipme.util.ServerUtilFunctions;

/**
 * Created by AhmetOguzhanBasar on 01.07.2015.
 */
public class BrandDao {
    public static BrandModel getBrandFromServer(String brandId){
        BrandModel mBrandModel= null;
        String function = "brand";

        final List<NameValuePair> mParams = new ArrayList<>();
        mParams.add(new BasicNameValuePair("brand_id", brandId));

        try {
            ServerResponse s = ServerUtilFunctions.callServer(GeneralValues.BASE_URL + function, mParams, true);
            if(s.success) {
                mBrandModel = new BrandModel();
                JSONObject mJSONObject = JsonFunctions.getJsonObjectFromObject(s.json, "data");
                mBrandModel = brandJsonParse(mJSONObject);
            }else{
                Log.e("", "BrandDao - getBrandFromServer() - ServerResponse : " + s.error_message);
            }
        }catch(Exception e){e.printStackTrace();}

        return mBrandModel;

    }

    private static BrandModel brandJsonParse(JSONObject jsonObject){

        BrandModel brandModel = null;
        try {
            brandModel = new BrandModel();
            brandModel.id = jsonObject.getString("id");
            brandModel.name = jsonObject.getString("name");
            brandModel.following = jsonObject.getString("following");
            brandModel.category = jsonObject.getString("category");
            brandModel.dealCount = jsonObject.getString("deal_count");
            brandModel.storeCount = jsonObject.getString("store_count");
            brandModel.brandPhoto = jsonObject.getString("brand_photo");
        }catch (JSONException e){e.printStackTrace();}

        return brandModel;

    }
}
