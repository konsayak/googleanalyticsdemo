package go.vipme.dao;

import android.util.Log;
import org.apache.http.NameValuePair;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;
import go.vipme.constant.GeneralValues;
import go.vipme.model.ProfileModel;
import go.vipme.model.ServerResponse;
import go.vipme.util.JsonFunctions;
import go.vipme.util.ServerUtilFunctions;

public class ProfileDao {

    public static ProfileModel getProfileDataFromServer(){
        final List<NameValuePair> mParams = new ArrayList<>();
        ProfileModel mProfileModel = null;
        try {
            ServerResponse s = ServerUtilFunctions.callServer(GeneralValues.BASE_URL + "profile", mParams, true);
            if(s.success) {
                mProfileModel = new ProfileModel();
                mProfileModel = profileJsonParse(JsonFunctions.getJsonObjectFromObject(s.json, "data"));
            }else{
                Log.e("","ProfileDao - getProfileDataFromServer() - ServerResponse : "+s.error_message);
            }
        }catch(Exception e){e.printStackTrace();}
        return mProfileModel;
    }

    private static ProfileModel profileJsonParse(JSONObject jsonObject){

        String brandCount,tagCount,pushSoundEnable,pushClock,gender;
        ProfileModel mProfileModel = null;
        try {
            mProfileModel = new ProfileModel();
            brandCount = JsonFunctions.getStringValue(jsonObject, "brand_count");
            int tempCount = Integer.parseInt(brandCount);
            if (tempCount < 0) {
                tempCount = 0;
            }
            brandCount = Integer.toString(tempCount);
            tagCount = JsonFunctions.getStringValue(jsonObject, "tag_count");
            tempCount = Integer.parseInt(tagCount);
            if (tempCount < 0) {
                tempCount = 0;
            }
            tagCount = Integer.toString(tempCount);
            pushSoundEnable = JsonFunctions.getStringValue(jsonObject, "push_sound_enable");
            pushClock = JsonFunctions.getStringValue(jsonObject, "push_times");
            gender = JsonFunctions.getStringValue(jsonObject, "gender");

            mProfileModel.brandCount = brandCount;
            mProfileModel.tagCount = tagCount;
            mProfileModel.pushSoundEnable = pushSoundEnable;
            mProfileModel.pushClock = pushClock;
            mProfileModel.gender = gender;
        }catch(Exception e){e.printStackTrace();}

        return mProfileModel;
    }

}
