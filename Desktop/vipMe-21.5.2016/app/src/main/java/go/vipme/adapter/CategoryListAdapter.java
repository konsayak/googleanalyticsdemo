package go.vipme.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import go.vipme.R;
import go.vipme.interfaces.CategoryItemListener;
import go.vipme.model.NavigationDrawerListItemModel;

/**
 * Created by Egemen on 02.02.2016.
 */
public class CategoryListAdapter extends ArrayAdapter<NavigationDrawerListItemModel> {

    ArrayList<NavigationDrawerListItemModel> categoryList;
    Context context;
    CategoryItemListener categoryItemListener;

    public CategoryListAdapter(Context context,int textViewResourceId,ArrayList<NavigationDrawerListItemModel> categoryList,CategoryItemListener _categoryItemListener){
        super(context, textViewResourceId, categoryList);
        this.categoryList = categoryList;
        this.context = context;
        this.categoryItemListener = _categoryItemListener;
    }
    @Override
    public View getView(final int position,View convertView,final ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.category_list_item, null);
        }
        String photo;
        TextView categoryNmae   = (TextView)v.findViewById(R.id.category_item_name);
        ImageView image         = (ImageView)v.findViewById(R.id.category_item_image);


        photo=categoryList.get(position).photo;
        Log.d("photoUrl", photo);

            categoryNmae.setText(categoryList.get(position).name);
            Picasso.with(context)
                    .load(photo)
                    .into(image);

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                categoryItemListener.OnItemClick(categoryList.get(position).id);
                Log.d("categoryId", categoryList.get(position).id);
            }
        });

        return v;
    }
}
