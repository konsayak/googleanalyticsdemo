package go.vipme.model;

public class IntroductionModel {
    public int introImage;
    public int introText;

    public IntroductionModel(int text, int image){
        this.introImage=image;
        this.introText=text;
    }
}

