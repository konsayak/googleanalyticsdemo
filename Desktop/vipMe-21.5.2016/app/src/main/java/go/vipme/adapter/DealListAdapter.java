package go.vipme.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import go.vipme.R;
import go.vipme.activity.BrandDetailActivity;
import go.vipme.activity.DealDetailActivity;
import go.vipme.dao.LogShareDao;
import go.vipme.model.DealModel;
import go.vipme.util.CustomTextView;
import go.vipme.util.Dialogs;
import go.vipme.util.InternetStatus;

/**
 * Created by OguzhanBasar on 18.03.2015.
 */
public class DealListAdapter extends ArrayAdapter<DealModel> {

    ArrayList<DealModel> dealModels;
    Context context;

    ImageView shareIcon,favoriteIcon,onlineDeal;
    CustomTextView shareText,favoriteText;

    public DealListAdapter(Context context,int textViewResourceId,ArrayList<DealModel> dealModels){
                super(context, textViewResourceId, dealModels);
                this.dealModels = dealModels;
                this.context = context;

    }

    @Override
    public View getView(final int position, final View convertView, final ViewGroup parent){

        View v = convertView;

        if(v==null){
            LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.list_item, null);
        }

        if(dealModels.get(position)!=null){

            CustomTextView  dealTitle               = (CustomTextView) v.findViewById(R.id.deal_title);
            ImageView       dealPhoto               = (ImageView) v.findViewById(R.id.deal_image);
            ImageView       brandPhoto              = (ImageView) v.findViewById(R.id.brand_logo);
//            LinearLayout    dealAddRemoveFavorite   = (LinearLayout) v.findViewById(R.id.deal_add_remove_favorite);
            LinearLayout    openDealDetail          = (LinearLayout) v.findViewById(R.id.open_deal_detail);
      final LinearLayout    dealShare               = (LinearLayout) v.findViewById(R.id.deal_share);
                            shareIcon               = (ImageView) v.findViewById(R.id.share_image);//
                            favoriteIcon            = (ImageView) v.findViewById(R.id.favorite_icon);
                            onlineDeal              = (ImageView) v.findViewById(R.id.online_deal);
                            shareText               = (CustomTextView) v.findViewById(R.id.share_text);
                            favoriteText            = (CustomTextView) v.findViewById(R.id.favorite_text);
                            context=getContext();
            dealTitle.setText(dealModels.get(position).title);
            Picasso.with(context)
                .load(dealModels.get(position).dealPhoto)
                .into(dealPhoto);
            Picasso.with(context)
                .load(dealModels.get(position).brandPhoto)
                .into(brandPhoto);

//            if (dealModels.get(position).favored.equals("1")) {
//                favoriteIcon.setImageResource(R.drawable.favorite_icon_remove);
//                favoriteText.setText("Çıkart");
//                favoriteText.setTextColor(context.getResources().getColor(R.color.list_option_blue));
//            } else if (dealModels.get(position).favored.equals("0")) {
//                favoriteIcon.setImageResource(R.drawable.favorite_icon_add);
//                favoriteText.setText("Ekle");
//                favoriteText.setTextColor(context.getResources().getColor(R.color.tag_bg));
//            }

            if (dealModels.get(position).online.equals("1")) {
                onlineDeal.setVisibility(View.VISIBLE);
                onlineDeal.setImageDrawable(getContext().getResources().getDrawable(R.drawable.online_indirim));
            } else {
                onlineDeal.setVisibility(View.VISIBLE);
                onlineDeal.setImageDrawable(getContext().getResources().getDrawable(R.drawable.magaza_indirim));
            }

            openDealDetail.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    if (InternetStatus.getInstance(getContext()).isOnline()) {
                        Intent intent = new Intent(context, DealDetailActivity.class);
                        intent.putExtra("deal_id", dealModels.get(position).id);
                        intent.putExtra("calling", "deal_list_adapter");
                        context.startActivity(intent);
                    }
                    else{
                        Dialogs.InternetControlDialog(context);

                    }
                    }


            });

            brandPhoto.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {

                    if (InternetStatus.getInstance(getContext()).isOnline()) {
                    Intent intent = new Intent(context, BrandDetailActivity.class);
                    intent.putExtra("brand_id", dealModels.get(position).brandId);
                    context.startActivity(intent);
                }
                    else{
                        Dialogs.InternetControlDialog(context);
                    }
            }





            });

//            dealAddRemoveFavorite.setOnClickListener(new View.OnClickListener() {
//
//                @Override
//                public void onClick(View view) {
//
//                    String favoriteCount = PreferenceDao.getValue("favorite_counter");
//
//                    if(favoriteCount.equals("")){
//                        PreferenceDao.setValue("favorite_counter", "1");
//                    }else{
//                        PreferenceDao.setValue("favorite_counter",String.valueOf(Integer.parseInt(favoriteCount)+1));
//                    }
//
//                    favoriteCount = PreferenceDao.getValue("favorite_counter");
//
//                    if (dealModels.get(position).favored.equals("1")) {
//                        favoriteIcon.setImageResource(R.drawable.favorite_icon_add);
//                        favoriteText.setText("Ekle");
//                        favoriteText.setTextColor(context.getResources().getColor(R.color.tag_bg));
//                        dealModels.get(position).favored = "0";
//                        DealFavoredDao.sendFavoredDataToServer(dealModels.get(position).id, "0");
//
//                        try {
//                            ((DealsFragment) (MyApplication.getDealsFragment())).setFavoredFromDealDetail(dealModels.get(position).id, "0");
//                        }catch(Exception e){e.printStackTrace();}
//                        try{
////                            ((FavoritesFragment) (MyApplication.getFavoritesFragment())).removeDealFromList(dealModels.get(position).id);
//                        }catch(Exception e){e.printStackTrace();}
//
//                        notifyDataSetChanged();
//
//                        if(Integer.parseInt(favoriteCount)<=5) {
//                            Toast.makeText(MyApplication.getAppContext(), "Favorilerden çıkarıldı", Toast.LENGTH_SHORT).show();
//                        }
//                    } else if (dealModels.get(position).favored.equals("0")) {
//                        favoriteIcon.setImageResource(R.drawable.favorite_icon_remove);
//                        favoriteText.setText("Çıkart");
//                        favoriteText.setTextColor(context.getResources().getColor(R.color.list_option_blue));
//                        dealModels.get(position).favored = "1";
//                        DealFavoredDao.sendFavoredDataToServer(dealModels.get(position).id, "1");
//                        notifyDataSetChanged();
//
//                        if(Integer.parseInt(favoriteCount)<=5) {
//                            Toast.makeText(MyApplication.getAppContext(), "Favorilere eklendi", Toast.LENGTH_SHORT).show();
//                        }
//                    }
//
//                }
//
//            });

            dealShare.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {

                    if (InternetStatus.getInstance(getContext()).isOnline()) {
                    Toast.makeText(context, "Paylaşım seçenekleri hazırlanıyor.", Toast.LENGTH_SHORT).show();
                    LogShareDao.sendShareDataToServer("deal", dealModels.get(position).id);
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, dealModels.get(position).brandName + " " + dealModels.get(position).title + " vipme’de. http://www.vipme.com.tr/f/" + dealModels.get(position).id + " #vipme");
                    sendIntent.setType("text/plain");
                    context.startActivity(Intent.createChooser(sendIntent, "İndirimi Paylaş"));

                }
                    else{
                        Dialogs.InternetControlDialog(context);
                    }
            }

            });

        }

        return v;

    }

}