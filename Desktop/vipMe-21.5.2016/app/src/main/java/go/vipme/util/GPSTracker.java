package go.vipme.util;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
 
public class GPSTracker extends Service implements LocationListener {
 
    private final Context mContext;
 
    boolean isGPSEnabled = false;
    boolean isNetworkEnabled = false;
    boolean canGetLocation = false;
    Location location;
    double latitude = 0.0;
    double longitude = 0.0;
 
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 10; // 10 minute
    protected LocationManager locationManager;
 
    public GPSTracker(Context context) {
        this.mContext = context;
        location = getLocation();
    }
 
    public Location getLocation() {
        try {
            locationManager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);
 
            isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
 
            isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            //if içi boş gibi
            if (!isGPSEnabled && !isNetworkEnabled) {
            	Log.e(MyApplication.TAG, "GPSTracker getLocation !isGPSEnabled && !isNetworkEnabled");
            } else {
                this.canGetLocation = true;
                if (isNetworkEnabled) {
                	Log.e(MyApplication.TAG, "GPSTracker getLocation isNetworkEnabled");
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    if (locationManager != null) {
                        location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    }
                } else if (isGPSEnabled) {
                	Log.e(MyApplication.TAG, "GPSTracker getLocation isGPSEnabled");
                	locationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    if (locationManager != null) {
                        location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    }
                }
            }
 
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(MyApplication.TAG, "GPSTracker getLocation error : " + e.toString());
        }
 
        return location;
    }
    
    public void stopUsingGPS(){
    	try {
    		if(locationManager != null){
    			locationManager.removeUpdates(GPSTracker.this);
    		}       
		} catch (Exception e) {
			Log.e(MyApplication.TAG,"GPSTracker - stopUsingGPS() - e : "+e.toString());
		}
    }
    
    public double getLatitude(){
        if(location != null){
            latitude = location.getLatitude();
        }
         
        return latitude;
    }
     
    public double getLongitude(){
        if(location != null){
            longitude = location.getLongitude();
        }
         
        return longitude;
    }

    public boolean canGetLocation() {
        return this.canGetLocation;
    }
     
    @Override
    public void onLocationChanged(Location location) {}
    @Override
    public void onProviderDisabled(String provider) {}
    @Override
    public void onProviderEnabled(String provider) {}
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {}
    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }
 
}