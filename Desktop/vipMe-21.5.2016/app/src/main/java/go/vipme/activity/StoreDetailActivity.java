package go.vipme.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.adjust.sdk.Adjust;
import com.facebook.appevents.AppEventsLogger;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import go.vipme.R;
import go.vipme.dao.PreferenceDao;
import go.vipme.util.CustomTextView;
import go.vipme.util.MyApplication;

/**
 * Created by OguzhanBasar on 02.04.2015.
 */
public class StoreDetailActivity extends Activity {

    String id,name,distance,address,phone,latitude,longitude,storeOpenHours;
    RelativeLayout loadingPanel,closeStores;
    LinearLayout middleLayout;
    CustomTextView phoneView;

    private GoogleMap googleHarita;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_detail);

        Intent intent   = getIntent();
        id              = intent.getStringExtra("store_id");
        name            = intent.getStringExtra("store_name");
        distance        = intent.getStringExtra("store_distance");
        address         = intent.getStringExtra("store_address");
        phone           = intent.getStringExtra("store_phone");
        latitude        = intent.getStringExtra("store_latitude");
        longitude       = intent.getStringExtra("store_longitude");
        storeOpenHours  = intent.getStringExtra("store_open_hours");

        ImageButton     backButton   = (ImageButton)findViewById(R.id.back_button);
        CustomTextView  titleView    = (CustomTextView)findViewById(R.id.close_store_title);
        CustomTextView  subTitleView = (CustomTextView)findViewById(R.id.close_store_sub_title);
        CustomTextView  addressView  = (CustomTextView)findViewById(R.id.store_address);
                        phoneView    = (CustomTextView)findViewById(R.id.store_phone);
                        loadingPanel = (RelativeLayout)findViewById(R.id.loadingPanel);
                        middleLayout = (LinearLayout)findViewById(R.id.middle_layout);
                        closeStores  = (RelativeLayout)findViewById(R.id.close_stores);

        loadingPanel.setVisibility(View.VISIBLE);
        middleLayout.setVisibility(View.GONE);

        titleView.setText(name);
        subTitleView.setText("Sana "+distance+" yakınlıkta");
        addressView.setText(address);
        phoneView.setText(phone);

        backButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view){
                onBackPressed();
                finish();
            }

        });

        try {
            googleHarita = ((MapFragment) getFragmentManager().findFragmentById(R.id.map_fragment)).getMap();
            googleHarita.setMyLocationEnabled(true);
            googleHarita.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude))).title(name));
            googleHarita.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude)), 9f));

            closeStores.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    getRoute(latitude, longitude);

                }

            });

            phoneView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    String number = "tel:" + phoneView.getText().toString().trim();
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse(number));
                    startActivity(intent);

                }

            });
        }catch(Exception e){e.printStackTrace();}
        loadingPanel.setVisibility(View.GONE);
        middleLayout.setVisibility(View.VISIBLE);

    }

    @Override
    public void onResume(){
        super.onResume();
        Adjust.onResume();
        AppEventsLogger.activateApp(this, "520852994707454");
    }

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance(this).activityStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance(this).activityStop(this);
    }

    public void getRoute(String lat, String lng){
        Toast.makeText(this, "Yol tarifi almak için harita servisi açılıyor..", Toast.LENGTH_LONG).show();
        String uri = String.format("http://maps.google.com/maps?daddr=%s,%s", String.valueOf(lat), String.valueOf(lng));
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        startActivity(intent);
    }

}