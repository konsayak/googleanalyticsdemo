package go.vipme.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.adjust.sdk.Adjust;
import com.facebook.appevents.AppEventsLogger;
import com.google.analytics.tracking.android.EasyTracker;

import go.vipme.R;
import go.vipme.adapter.TagSelectPagerAdapter;
import go.vipme.dao.PreferenceDao;
import go.vipme.fragment.TagContainerFragment;
import go.vipme.util.CustomTextView;
import go.vipme.util.DontSwipeViewPager;
import go.vipme.util.MyApplication;


public class TagSelectActivity extends FragmentActivity {

    Context context;
    DontSwipeViewPager viewPager=null;
    TagSelectPagerAdapter mTagSelectPagerAdapter;
    CustomTextView title_text,titleText2,skipButton;
    LinearLayout hidden;
    private int mUserFollowingBrandCount;
    private int mUserFollowingTagCount;
    public String type;

    @Override
    public void onCreate(Bundle savedInstanceState){

        super.onCreate(savedInstanceState);

        MyApplication.setTagSelectActivity(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_tag_select);

        viewPager            = (DontSwipeViewPager) findViewById(R.id.pager);
        skipButton           = (CustomTextView)findViewById(R.id.skip_button);
        title_text           = (CustomTextView)findViewById(R.id.title_text);
        titleText2           = (CustomTextView)findViewById(R.id.title_text_2);
        hidden               = (LinearLayout)findViewById(R.id.hidden);

        type = getIntent().getStringExtra("type");
        context = this;

        mTagSelectPagerAdapter = new TagSelectPagerAdapter(getSupportFragmentManager());
        try{
            mUserFollowingBrandCount=Integer.parseInt(TagContainerFragment.mProfileModel.brandCount);
            mUserFollowingTagCount=Integer.parseInt(TagContainerFragment.mProfileModel.tagCount);
            Log.e(MyApplication.TAG,"TagSelectActivity mUserFollowingBrandCount="+mUserFollowingBrandCount);
            Log.e(MyApplication.TAG,"TagSelectActivity mUserFollowingTagCount="+mUserFollowingTagCount);
        }
        catch (Exception e){
            Log.e(MyApplication.TAG,"TagSelectActivity mUserFollowing***Count error");
        }

        viewPager.setPagingEnabled(false);
        viewPager.setAdapter(mTagSelectPagerAdapter);

        skipButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.e(MyApplication.TAG,"TagSelect UserFollowingCount="+TagContainerFragment.userBrandFollowingCount);
                Log.e(MyApplication.TAG, "TagSelect UserTagFollowingCount=" + TagContainerFragment.userTagFollowingCount);

                if (TagContainerFragment.userBrandFollowingCount<5){
                    PreferenceDao.showSettingAlert("",
                            "Sana özel indirimler için en az 5 marka seçmelisin.",
                            TagSelectActivity.this);
                }
                else {
                    viewPager.setCurrentItem(1);
                }


            }

        });

        viewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                switch(position){
                    case 0: title_text.setText(getResources().getString(R.string.trademark_choose_title));
                            hidden.setVisibility(LinearLayout.VISIBLE);
                            titleText2.setVisibility(LinearLayout.INVISIBLE);
                        break;
                    case 1: titleText2.setText(getResources().getString(R.string.interest_choose_title));
                            hidden.setVisibility(LinearLayout.INVISIBLE);
                            titleText2.setVisibility(LinearLayout.VISIBLE);
                            skipButton.setText("Seçimi Tamamla");
                            skipButton.setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    if (checkUserTagFollowingCount()){
                                        Intent intent;
                                        if(!PreferenceDao.getValue("first_time").equals("")){
                                            intent = new Intent(TagSelectActivity.this, MainActivity.class);


                                        }else if(PreferenceDao.getValue("first_time").equals("")){
                                            intent = new Intent(TagSelectActivity.this, MainActivity.class);


//                                        Log.e(MyApplication.TAG,"TagSelect UserFollowingCount="+TagContainerFragment.userBrandFollowingCount);
//                                        Log.e(MyApplication.TAG,"TagSelect UserTagFollowingCount="+TagContainerFragment.userTagFollowingCount);
                                        }else{
                                            intent = new Intent(TagSelectActivity.this, MainActivity.class);

                                            Log.e(MyApplication.TAG,"TagSelect UserFollowingCount="+TagContainerFragment.userBrandFollowingCount);
                                            Log.e(MyApplication.TAG,"TagSelect UserTagFollowingCount="+TagContainerFragment.userTagFollowingCount);

                                        }

                                        intent.putExtra("previous_page","tag_select");
                                        startActivity(intent);
                                        onBackPressed();
                                        finish();
                                    }

                                }

                            });
                        break;
                    default: title_text.setText(getResources().getString(R.string.trademark_choose_title));
                        skipButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                viewPager.setCurrentItem(1);
                            }
                        });
                        break;
                }
            }

        });

    }

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance(this).activityStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance(this).activityStop(this);
        try {
            ((TagContainerFragment) ((FragmentPagerAdapter) viewPager.getAdapter()).getItem(0)).brandStart=0;
            ((TagContainerFragment) ((FragmentPagerAdapter) viewPager.getAdapter()).getItem(0)).brandPage=1;
        }catch(Exception e){e.printStackTrace();}
        try{
            ((TagContainerFragment) ((FragmentPagerAdapter) viewPager.getAdapter()).getItem(1)).tagStart=0;
            ((TagContainerFragment) ((FragmentPagerAdapter) viewPager.getAdapter()).getItem(1)).tagPage=1;
        }catch (Exception e){e.printStackTrace();}
    }

    @Override
    public void onPause() {
        super.onPause();
        Adjust.onPause();
        try {
            ((TagContainerFragment) ((FragmentPagerAdapter) viewPager.getAdapter()).getItem(0)).brandStart=0;
            ((TagContainerFragment) ((FragmentPagerAdapter) viewPager.getAdapter()).getItem(0)).brandPage=1;
        }catch(Exception e){e.printStackTrace();}
        try{
            ((TagContainerFragment) ((FragmentPagerAdapter) viewPager.getAdapter()).getItem(1)).tagStart=0;
            ((TagContainerFragment) ((FragmentPagerAdapter) viewPager.getAdapter()).getItem(1)).tagPage=1;
        }catch (Exception e){e.printStackTrace();}
    }

    @Override
    public void onResume(){
        super.onResume();
        Adjust.onResume();
        AppEventsLogger.activateApp(this, "520852994707454");
    }


    private  boolean checkUserTagFollowingCount(){
        if (TagContainerFragment.userTagFollowingCount<3){
            PreferenceDao.showSettingAlert("",
                    "Sana özel dergin için en az 3 ilgi alanı seçmelisin.",
                    TagSelectActivity.this);
            return false;
        }
        else {
            return true;
        }
    }

}