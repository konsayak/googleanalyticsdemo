package go.vipme.util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.util.Log;

import go.vipme.model.DealModel;
import go.vipme.model.NavigationDrawerListItemModel;
import go.vipme.model.NewsModel;
import go.vipme.model.StoreModel;

import java.util.ArrayList;

public class JsonFunctions {

    public static boolean getBooleanValue(JSONObject obj, String key){
        boolean result = false;
        try{
            result = obj.getBoolean(key);
        }catch(Exception e){
            Log.e("","JsonFunctions - getBooleanValue : error"+e.toString());
        }
        return result;
    }

    public static String getStringValue(JSONObject obj, String key){
        String resultText = "";
        try{
            resultText = obj.getString(key);
        }catch(Exception e){
            Log.e("","JsonFunctions - getStringValue : error"+e.toString());
        }
        return resultText;
    }

    public static int getIntegerValue(JSONObject obj, String key){
        int result = 0;
        try{
            result = obj.getInt(key);
        }catch(Exception e){
            Log.e("","JsonFunctions - getIntegerValue : error : "+e.toString());
        }
        return result;
    }

    public static JSONArray getArrayFromObject(JSONObject obj, String key){
        JSONArray resultArray = new JSONArray();
        try{
            resultArray = obj.getJSONArray(key);
        }catch(Exception e){
            Log.e("","JsonFunctions - getArrayFromObject : error"+e.toString());
        }
        return resultArray;
    }

    public static JSONObject getObjectFromArray(JSONArray array, int index){
        JSONObject resultObject = new JSONObject();
        try{
            resultObject = array.getJSONObject(index);
        }catch(Exception e){
            Log.e("","JsonFunctions - getObjectFromArray : error"+e.toString());
        }
        return resultObject;
    }

    public static JSONObject getJsonObjectFromObject(JSONObject object, String key){
        JSONObject resultObject = new JSONObject();
        try{
            resultObject = object.getJSONObject(key);
        }catch(Exception e){
            Log.e("","JsonFunctions - getJsonObjectFromObject : error"+e.toString());
        }
        return resultObject;
    }

}
