package go.vipme.interfaces;

/**
 * Created by Egemen on 04.02.2016.
 */
public interface CategoryItemListener {
    void OnItemClick(String categoryId);
}
