package go.vipme.util;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import go.vipme.R;
import go.vipme.activity.MainActivity;


public class AlertDialogManager {

    static final String DISPLAY_MESSAGE_ACTION = "go.vipme";
    public final String EXTRA_MESSAGE = "message";

    public void displayMessage(Context context, String message) {
        Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
        intent.putExtra(EXTRA_MESSAGE, message);
        context.sendBroadcast(intent);
    }

    @SuppressWarnings("deprecation")
    public void generateNotification(Context context, String message, String rid, String uid) {
        int icon = R.drawable.logo;
        long when = System.currentTimeMillis();
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = new Notification(icon, message, when);

        String title = context.getString(R.string.app_name);

        Intent notificationIntent = new Intent(context, MainActivity.class);
        notificationIntent.putExtra("rid", rid);
        notificationIntent.putExtra("uid", uid);
        notificationIntent.putExtra("message", message);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        //notificationIntent.addFlags(PendingIntent.FLAG_UPDATE_CURRENT);

        PendingIntent pendingNotificationIntent = PendingIntent.getActivity(context, (int)when, notificationIntent,PendingIntent.FLAG_UPDATE_CURRENT);
        notification.setLatestEventInfo(context, title, message, pendingNotificationIntent);
        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        // Play default notification sound
        notification.defaults |= Notification.DEFAULT_SOUND;

        // Vibrate if vibrate is enabled
        notification.defaults |= Notification.DEFAULT_VIBRATE;

        notificationManager.notify((int)when, notification);

    }
}