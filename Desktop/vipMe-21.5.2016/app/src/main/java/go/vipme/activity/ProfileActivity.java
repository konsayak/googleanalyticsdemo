package go.vipme.activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.analytics.tracking.android.Tracker;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import go.vipme.R;
import go.vipme.constant.GeneralValues;
import go.vipme.dao.FacebookDao;
import go.vipme.dao.LogShareDao;
import go.vipme.dao.PreferenceDao;
import go.vipme.dao.ProfileChoicesDao;
import go.vipme.dao.ProfileDao;
import go.vipme.model.ProfileModel;
import go.vipme.util.CustomTextView;
import go.vipme.util.Dialogs;
import go.vipme.util.MyApplication;

/**
 * Created by Kursat on 4/16/2016.
 */
public class ProfileActivity extends FragmentActivity {
    private CallbackManager callbackManager;

    static CustomTextView brandCountView,tagCountView,pushSoundEnableView,pushClockView,genderView;
    static ProfileModel mProfileModel;
    static ImageView fbLoginButton;
    private FacebookDao facebookDao=new FacebookDao();


    private Tracker tracker;

    final Handler mHandler = new Handler();
    final Runnable mUpdateResults = new Runnable() {
        public void run() {
            setUI();
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_profile);
        FacebookSdk.sdkInitialize(getApplicationContext());

        brandCountView                  = (CustomTextView)findViewById(R.id.brand_count_view);
        tagCountView                    = (CustomTextView)findViewById(R.id.tag_count_view);
        pushSoundEnableView             = (CustomTextView)findViewById(R.id.push_sound_enable_view);
        pushClockView                   = (CustomTextView)findViewById(R.id.push_clock_view);
        genderView                      = (CustomTextView)findViewById(R.id.gender_view);

        LinearLayout facebookLayout     =   (LinearLayout)findViewById(R.id.facebook);
        LinearLayout twitterLayout      =   (LinearLayout)findViewById(R.id.twitter);
        LinearLayout instagramLayout    =   (LinearLayout)findViewById(R.id.instagram);
        LinearLayout vipmeLayout        =   (LinearLayout)findViewById(R.id.vipme);
        LinearLayout genderLayout       =   (LinearLayout)findViewById(R.id.gender_layout);
        LinearLayout messageClockLayout =   (LinearLayout)findViewById(R.id.message_clock_layout);
        LinearLayout messageSoundLayout =   (LinearLayout)findViewById(R.id.message_sound_layout);
        LinearLayout brandsFollowing    =   (LinearLayout)findViewById(R.id.brands_following);
        LinearLayout tagsFollowing      =   (LinearLayout)findViewById(R.id.tags_following);

        ImageView inviteButton          =      (ImageView)findViewById(R.id.invite_button);
        ImageView rateButton            =      (ImageView)findViewById(R.id.rate_button);
        ImageView termsButton           =      (ImageView)findViewById(R.id.terms_button);
        ImageView emailButton           =      (ImageView)findViewById(R.id.email_button);
        fbLoginButton=(ImageView)findViewById(R.id.fb_loginButton);
        MyApplication.setProfileActivity(this);
        callbackManager=CallbackManager.Factory.create();


        inviteButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                LogShareDao.sendShareDataToServer("app", "0");
                invite();
            }
        });
        rateButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                rateMe();
            }
        });
        termsButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                openTerms();
            }
        });
        emailButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                sendMail();
            }
        });

        facebookLayout.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                openWebURL(GeneralValues.VIPME_FACEBOOK);
            }
        });
        twitterLayout.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                openWebURL(GeneralValues.VIPME_TWITTER);
            }
        });
        instagramLayout.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                openWebURL(GeneralValues.VIPME_INSTAGRAM);
            }
        });
        vipmeLayout.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                openWebURL(GeneralValues.VIPME_WEBSITE);
            }
        });
        genderLayout.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                DialogFragment newFragment = new Dialogs.GenderDialog();
                newFragment.show(getSupportFragmentManager(), "gender_dialog");
            }
        });
        messageClockLayout.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                DialogFragment newFragment = new Dialogs.MessageClockDialog();
                newFragment.show(getSupportFragmentManager(), "message_clock_dialog");
            }
        });
        messageSoundLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new Dialogs.MessageSoundDialog();
                newFragment.show(getSupportFragmentManager(), "message_sound_dialog");
            }
        });
        brandsFollowing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileActivity.this, UserFavoredActivity.class);
                intent.putExtra("type", "brands");
                startActivity(intent);
            }
        });
        tagsFollowing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileActivity.this, UserFavoredActivity.class);
                intent.putExtra("type", "tags");
                startActivity(intent);
            }
        });


        fbLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e(MyApplication.TAG,"ProfileFragment fbLoginButton onClick");
                facebookLoginOrLogout(PreferenceDao.getValue(GeneralValues.FB_LOGIN_STATUS));

            }
        });




        getProfileDataFromServer();
        this.tracker = EasyTracker.getInstance(this);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode,resultCode,data);
    }

    private void facebookLoginOrLogout(String fbLoginStatus) {



        if (fbLoginStatus.equals(GeneralValues.FB_LOGIN)){
            LoginManager.getInstance().logOut();
            PreferenceDao.setValue(GeneralValues.FB_LOGIN_STATUS, GeneralValues.FB_LOGOUT);
            PreferenceDao.setValue("user_name", "");
            Toast.makeText(getApplicationContext(),
                    "Facebook oturumu kapatıldı.", Toast.LENGTH_SHORT).show();
            fbLoginButton.setImageDrawable(getResources().getDrawable(
                    R.drawable.profile_login_with_facebook
            ));
            MainActivity.changeTopTitle();
        }
        else {
            LoginManager.getInstance().logInWithReadPermissions(
                    ProfileActivity.this, Arrays.asList("public_profile"));

            LoginManager.getInstance().registerCallback(callbackManager,
                    new FacebookCallback<LoginResult>() {
                        @Override
                        public void onSuccess(final LoginResult loginResult) {

                            GraphRequest request=GraphRequest.newMeRequest(loginResult.getAccessToken(),
                                    new GraphRequest.GraphJSONObjectCallback(){

                                        @Override
                                        public void onCompleted(JSONObject user, GraphResponse graphResponse) {
                                            try{
                                                Log.e(MyApplication.TAG, "ProfileFragment FB Login Success");
                                                Toast.makeText(getApplicationContext(),
                                                        "Facebook girişi yapıldı.", Toast.LENGTH_SHORT).show();
                                                fbLoginButton.setImageDrawable(getResources().
                                                        getDrawable(R.drawable.profile_logout_with_facebook));
                                                PreferenceDao.setValue(GeneralValues.FB_LOGIN_STATUS,
                                                        GeneralValues.FB_LOGIN);
                                                Profile profile=Profile.getCurrentProfile();
                                                String name=user.getString("name");
                                                PreferenceDao.setValue("user_name", name);
                                                String gender=user.getString("gender");
                                                String userID=profile.getId();
                                                String accessToken=loginResult.getAccessToken().getToken();
                                                String fbTokenExpired="";
                                                String birthday="";


                                                Log.e(MyApplication.TAG, "ProfileFragment userName=" + name);

                                                MainActivity.changeTopTitle(name);

                                                facebookDao.facebookConnect(userID,accessToken,fbTokenExpired,name,
                                                        gender,birthday);

                                                Log.e(MyApplication.TAG, "FacebookLogin P.Fragment MyName=" + name);
                                                Log.e(MyApplication.TAG, "FacebookLogin P.Fragment MyGender=" + gender);
                                                Log.e(MyApplication.TAG, "FacebookLogin P.Fragment MyID=" + userID);
                                                Log.e(MyApplication.TAG, "FacebookLogin P.Fragment MyAccessToken" + accessToken);


                                            }
                                            catch (Exception e){
                                                Log.e(MyApplication.TAG,"ProfileFragment Login onSuccess Error");
                                                e.printStackTrace();
                                            }


                                        }
                                    });
                            request.executeAsync();

                        }

                        @Override
                        public void onCancel() {
                            Log.e(MyApplication.TAG, "ProfileFragment FB Login Cancaled");
                        }

                        @Override
                        public void onError(FacebookException e) {
                            Log.e(MyApplication.TAG, "ProfileFragment FB Login Error");
                        }
                    });

        }



    }

    @Override
    public void onResume() {
        super.onResume();
        this.tracker.set(Fields.SCREEN_NAME, getClass().getSimpleName());
        this.tracker.send( MapBuilder.createAppView().build() );
    }

    @Override
    public void onStart() {
        super.onStart();
        this.tracker.set(Fields.SCREEN_NAME, getClass().getSimpleName());
        this.tracker.send(MapBuilder.createAppView().build());
    }

    private  void  getProfileDataFromServer(){

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    mProfileModel = new ProfileModel();
                    mProfileModel = ProfileDao.getProfileDataFromServer();
                    if(mProfileModel!=null) {
                        mHandler.post(mUpdateResults);
                    }
                }
                catch(Exception e){e.printStackTrace();}
            }
        });
        thread.start();
    }

    private void setUI(){
        findViewById(R.id.loadingPanel).setVisibility(View.GONE);
        String[] messageClockArray = MyApplication.getAppContext().getResources().getStringArray(R.array.message_clock);
        Log.e(MyApplication.TAG,"ProfileFragment mProfilebrandCount"+mProfileModel.brandCount);
        brandCountView.setText(mProfileModel.brandCount);
        Log.e(MyApplication.TAG, "ProfileFragment mProfileTagCount" + mProfileModel.tagCount);
        tagCountView.setText(mProfileModel.tagCount);
        if(mProfileModel.pushSoundEnable.equals("1")){
            pushSoundEnableView.setText("Evet");
        }
        else if(mProfileModel.pushSoundEnable.equals("0")){
            pushSoundEnableView.setText("Hayır");
        }
        if(mProfileModel.gender.equals("1")){
            genderView.setText("Kadın");
        }
        else if(mProfileModel.gender.equals("2")){
            genderView.setText("Erkek");
        }
        else{genderView.setText("Erkek");}
        pushClockView.setText(messageClockArray[Integer.parseInt(mProfileModel.pushClock)]);
        if (PreferenceDao.getValue(GeneralValues.FB_LOGIN_STATUS).
                equals(GeneralValues.FB_LOGIN)){
            fbLoginButton.setImageDrawable(getResources().getDrawable(R.drawable.profile_logout_with_facebook));
        }
        else if (PreferenceDao.getValue(GeneralValues.FB_LOGIN_STATUS).
                equals(GeneralValues.FB_LOGOUT)){
            fbLoginButton.setImageDrawable(getResources().getDrawable(R.drawable.profile_login_with_facebook));
        }
        else {
            fbLoginButton.setImageDrawable(getResources().getDrawable(R.drawable.profile_login_with_facebook));
        }
    }

    private void openWebURL( String url ) {
        Uri uri = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }

    public static void DialogOnClick(String parentDialog,int choice,String choiseString){

        final List<NameValuePair> mParams = new ArrayList<>();
        String dialogFunction;
        if(parentDialog.equals("message_sound_dialog")){
            if(choice==0){
                choice=1;
            }else if(choice==1){
                choice=0;
            }
            mParams.add(new BasicNameValuePair("push_sound_enable", Integer.toString(choice)));
            dialogFunction = "set_push_sound_enable";
            pushSoundEnableView.setText(choiseString);
        }
        else if(parentDialog.equals("message_clock_dialog")){
            mParams.add(new BasicNameValuePair("push_time_interval", Integer.toString(choice)));
            dialogFunction= "set_push_time_interval";
            pushClockView.setText(choiseString);
        }
        else if(parentDialog.equals("gender_dialog")){
            if(choice==0){
                choice=1;
            }else if(choice==1){
                choice=2;
            }
            mParams.add(new BasicNameValuePair("gender", Integer.toString(choice)));
            dialogFunction= "set_gender";
            genderView.setText(choiseString);
        }else{
            dialogFunction= "";
        }
        ProfileChoicesDao.setUserChoiceToServer(dialogFunction, mParams);

    }

    private void openTerms(){
        Intent intent = new Intent(ProfileActivity.this, DealWebViewActivity.class);
        intent.putExtra("url", "http://www.vipme.com.tr/gizlilik");
        startActivity(intent);
    }

    private void sendMail(){
        Intent intent = new Intent(Intent.ACTION_VIEW);
        Uri data = Uri.parse("mailto:"
                + "iletisim@vipme.com.tr"
                + "?subject=" + "VIPME için bir önerim var!" + "&body=" + "\n\n\n\n\n\nLütfen öneri ya da şikayetlerinizi bize iletmekten çekinmeyin. Teşekkürler.\nVIPME.\nid:"+PreferenceDao.getValue(GeneralValues.DEVICE_ID));
        intent.setData(data);
        startActivity(intent);
    }

    private void rateMe(){
        Uri uri = Uri.parse("market://details?id=" + getBaseContext().getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + getBaseContext().getPackageName())));
        }
    }

    private void invite(){
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.invite_text));
        sendIntent.setType("text/plain");
        ((ProfileActivity) ProfileActivity.this).startActivity(Intent.createChooser(sendIntent, "Seçim"));
    }

    public static void setUpdate(String brandCount,String tagCount){
        if(!brandCount.equals("")&&tagCount.equals("")){
            brandCountView.setText(brandCount);
        }else if(!tagCount.equals("")&&brandCount.equals("")){
            tagCountView.setText(tagCount);
        }else{
            brandCountView.setText(brandCount);
            tagCountView.setText(tagCount);
        }

    }
}
