package go.vipme.dao;

import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import go.vipme.constant.GeneralValues;
import go.vipme.model.DealModel;
import go.vipme.model.ServerResponse;
import go.vipme.util.JsonFunctions;
import go.vipme.util.ServerUtilFunctions;

/**
 * Created by AhmetOguzhanBasar on 25.06.2015.
 */
public class SingleDealDao {

    public static DealModel getDealFromServer(String dealId){

        DealModel mDealModel = null;
        final List<NameValuePair> mParams = new ArrayList<>();
        mParams.add(new BasicNameValuePair("deal_id", dealId));

        try {
            ServerResponse s = ServerUtilFunctions.callServer(GeneralValues.BASE_URL + "deal", mParams, true);
            if(s.success){
                mDealModel = new DealModel();
                mDealModel = dealJsonParse(JsonFunctions.getJsonObjectFromObject(s.json, "data"));
            }else{
                Log.e("", "SingleDealDao - getDealFromServer() - ServerResponse : " + s.error_message);
            }
        }catch (Exception e){e.printStackTrace();}

        return mDealModel;

    }

    private static DealModel dealJsonParse(JSONObject jsonObject){

        DealModel mDealModel = new DealModel();
        Date willEnd,today;
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        mDealModel.id = JsonFunctions.getStringValue(jsonObject, "id");
        mDealModel.brandName = JsonFunctions.getStringValue(jsonObject, "brand_name");
        mDealModel.title = JsonFunctions.getStringValue(jsonObject, "title");
        mDealModel.dealPhoto = JsonFunctions.getStringValue(jsonObject, "deal_photo");
        mDealModel.brandPhoto = JsonFunctions.getStringValue(jsonObject, "brand_photo");
        mDealModel.body = JsonFunctions.getStringValue(jsonObject, "body");
        mDealModel.showEndDate = JsonFunctions.getStringValue(jsonObject, "show_end_date");
        mDealModel.startDate = JsonFunctions.getStringValue(jsonObject, "start_date");
        mDealModel.endDate = JsonFunctions.getStringValue(jsonObject, "end_date");
        mDealModel.favored = JsonFunctions.getStringValue(jsonObject, "favored");
        mDealModel.url = JsonFunctions.getStringValue(jsonObject, "url");
        mDealModel.brandId = JsonFunctions.getStringValue(jsonObject, "brand_id");
        mDealModel.online = JsonFunctions.getStringValue(jsonObject, "online");
        mDealModel.couponEnable = JsonFunctions.getStringValue(jsonObject,"coupon_enable");
        try {
            mDealModel.started = df.parse(mDealModel.startDate);
            willEnd = df.parse(mDealModel.endDate);
            today = new java.util.Date();
            long diff = willEnd.getTime() - today.getTime();
            int dateDiff = (int) (diff / 1000 / 60 / 60 / 24);
            mDealModel.dateDiffString = Integer.toString(dateDiff) + " Gün Kaldı";
            if (dateDiff < 0) {
                mDealModel.dateDiffString = "İndirim Sona Erdi";
            }else if (dateDiff == 0) {
                mDealModel.dateDiffString = "Son Gün";
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return mDealModel;
    }

}
