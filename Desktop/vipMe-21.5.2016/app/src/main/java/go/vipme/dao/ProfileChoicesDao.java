package go.vipme.dao;

import android.util.Log;

import org.apache.http.NameValuePair;

import java.util.List;

import go.vipme.constant.GeneralValues;
import go.vipme.model.ServerResponse;
import go.vipme.util.ServerUtilFunctions;

/**
 * Created by AhmetOguzhanBasar on 23.06.2015.
 */
public class ProfileChoicesDao {

    public static void setUserChoiceToServer(final String dialogFunction,final List<NameValuePair> params){
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    ServerResponse s = ServerUtilFunctions.callServer(GeneralValues.BASE_URL + dialogFunction, params, true);
                    if(!s.success){
                        Log.e("","ProfileChoicesDao - setUserChoiceToServer() - ServerResponse : " + s.error_message);
                    }
                }catch (Exception e){e.printStackTrace();}
            }
        });
        thread.start();
    }

}
