package go.vipme.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import go.vipme.R;

/**
 * Created by OguzhanBasar on 03.04.2015.
 */
public class DealWebViewActivity extends Activity {

    String url;
    Context context;
    RelativeLayout loadingPanel;
    ProgressBar loadingProgress;
    Toast loadingToastMessage;
    private boolean pageLoadFinished=false;



    private WebView webView;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deal_webview);


        Intent intent   = getIntent();
        context         = this;
        url             = intent.getStringExtra("url");

        loadingToastMessage=Toast.makeText(DealWebViewActivity.this,
                "Yükleniyor çok az kaldı", Toast.LENGTH_LONG);


        ProgressBar spinner=new android.widget.ProgressBar(
                context,
                null,
                android.R.attr.progressBarStyle);
        spinner.getIndeterminateDrawable().setColorFilter(0xFFFF0000, android.graphics.PorterDuff.Mode.MULTIPLY);

        ImageButton backButton      = (ImageButton) findViewById(R.id.webview_back_button);
                    loadingPanel    = (RelativeLayout) findViewById(R.id.loadingPanel);
                    webView         = (WebView) findViewById(R.id.webView1);
        loadingProgress=(ProgressBar)findViewById(R.id.loadingProgress);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebChromeClient(new WebChromeClient(){

            @Override
            public void onProgressChanged(WebView view, final int newProgress) {


                if (newProgress>85){
                    pageLoadFinished=true;
                    loadingProgress.setVisibility(View.GONE);


                }

            }
        });
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                loadingPanel.setVisibility(View.VISIBLE);
                loadingProgress.setVisibility(View.GONE);
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {

                        if (!pageLoadFinished) {
                            loadingPanel.setVisibility(View.GONE);

                        }
                    }
                }, 7000);

                super.onPageStarted(view, url, favicon);
            }


            @Override
            public void onPageFinished(WebView view, String url) {
                loadingPanel.setVisibility(View.GONE);
                loadingProgress.setVisibility(View.GONE);
                pageLoadFinished=true;

                super.onPageFinished(view, url);
            }


        });

        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setSupportZoom(true);
        webView.setInitialScale(1);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.setScrollbarFadingEnabled(false);
        webView.loadUrl(url);

        backButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (webView.canGoBack()){
                    webView.goBack();
                }
                else {
                    onBackPressed();
                    finish();
                }



            }

        });
    }

}
