package go.vipme.model;

import java.io.Serializable;
import org.json.JSONObject;

public class ServerResponse implements Serializable{

    private static final long serialVersionUID = 1L;

    public boolean success = false;
    public String error_message;
    public JSONObject json;

}
