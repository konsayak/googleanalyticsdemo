package go.vipme.dao;

import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

import go.vipme.constant.GeneralValues;
import go.vipme.model.ServerResponse;
import go.vipme.util.MyApplication;
import go.vipme.util.ServerUtilFunctions;

/**
 * Created by Halit on 20.8.2015.
 */
public class FacebookDao {

    public static void facebookConnect(String facebook_id,String facebook_access_token,
                                       String facebook_token_expire, String name, String gender, String birthday){
        final List<NameValuePair> mParams=new ArrayList<>();
        mParams.add(new BasicNameValuePair("facebook_id",facebook_id));
        mParams.add(new BasicNameValuePair("facebook_access_token",facebook_access_token));
        mParams.add(new BasicNameValuePair("facebook_token_expire",facebook_token_expire));
        mParams.add(new BasicNameValuePair("name",name));
        mParams.add(new BasicNameValuePair("gender",gender));
        mParams.add(new BasicNameValuePair("birthday",birthday));

        Thread thread=new Thread(new Runnable() {
            @Override
            public void run() {
            try{
                ServerResponse s= ServerUtilFunctions.callServer(GeneralValues.BASE_URL+"facebook_connect",
                        mParams,true);
                if (!s.success){
                    Log.e(MyApplication.TAG,"FacebookDao facebookConnect serverResponse error"+s.error_message);

                }
            }
            catch (Exception e){
                e.printStackTrace();
            }
            }
        });
        thread.start();
    }
}
