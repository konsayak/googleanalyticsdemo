package go.vipme.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;

import com.github.ksoichiro.android.observablescrollview.ObservableListView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.analytics.tracking.android.Tracker;
import com.melnykov.fab.FloatingActionButton;

import java.util.ArrayList;
import java.util.Iterator;

import go.vipme.R;
import go.vipme.activity.MainActivity;
import go.vipme.activity.SearchActivity;
import go.vipme.adapter.DealListAdapter;
import go.vipme.dao.DealsDao;
import go.vipme.model.DealModel;
import go.vipme.util.CustomTextView;
import go.vipme.util.Dialogs;
import go.vipme.util.InternetStatus;
import go.vipme.util.MyApplication;

public class FavoritesFragment extends Fragment implements ObservableScrollViewCallbacks {
    ActionBar ab;
    int currentFirstVisibleItem, currentTotalItemCount, currentScrollState;

    Activity activity;
    static int listViewFirstVisibleItem;
    static boolean  isLoading = false;
    static Context context;
    static LayoutInflater headerInflater;
    static ObservableListView mListView;
    static View rootView;
    FloatingActionButton fab;
    private Tracker tracker;


    public static boolean isFirstLoad;

    CustomTextView myDealsButton, dealsButton;
    static boolean  myDealsControl = true;

    public static int pageCounter = 1;
    public static String active_type;
    public static ArrayList<DealModel> dealArrayList;
    public static ArrayList<DealModel> myDealArrayList;

    public static DealListAdapter mDealListAdapter;


    private static ArrayList<DealModel> tempDealArrayList;

    public static ArrayList<DealModel> tempMyDealArrayList;
    private static SwipeRefreshLayout swipeContainer;
    private static Handler handler = new Handler();



    final static Runnable dealsUpdateResults = new Runnable() {
        public void run() {
            setUI();
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_special_for_user, container, false);
        mListView = (ObservableListView) rootView.findViewById(R.id.deal_list);
        headerInflater = inflater;
        ab = ((MainActivity) getActivity()).actionBar;
        fab = (FloatingActionButton) rootView.findViewById(R.id.fab_deals_fragment);
        myDealsButton = (CustomTextView) rootView.findViewById(R.id.my_deals_button);
        dealsButton = (CustomTextView) rootView.findViewById(R.id.deals_button);
        active_type = "designer";

        MyApplication.setFavoritesFragment(this);
        context = getActivity();
        mListView.setScrollViewCallbacks(this);
        ViewGroup header = (ViewGroup) headerInflater.inflate(R.layout.top_padding_layout, mListView, false);
        mListView.addHeaderView(header, null, false);

        isFirstLoad = true;
        dealArrayList = new ArrayList<>();
        myDealArrayList = new ArrayList<>();
        tempDealArrayList = new ArrayList<>();
        tempMyDealArrayList = new ArrayList<>();

        swipeContainer = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeContainer);
        swipeContainer.setProgressViewOffset(false, 0, (int) (getResources().getDimension(R.dimen.top_padding_caused_by_action_bar)));
        swipeContainer.setColorSchemeResources(
                R.color.theme_blue,
                R.color.tag_bg,
                R.color.male_blue);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mListView.setVisibility(View.GONE);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getData("deals", pageCounter, active_type);
                        ab.show();
                    }
                }, 800);
            }
        });
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (InternetStatus.getInstance(getActivity()).isOnline()) {
                    Intent i = new Intent(getActivity(), SearchActivity.class);
                i.putExtra("isDeal", "deal");
                startActivity(i);

            }
                else{
                    Dialogs.InternetControlDialog(context);
                }
        }
        });
        mListView.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                currentFirstVisibleItem = firstVisibleItem;
                listViewFirstVisibleItem = currentFirstVisibleItem;
                currentTotalItemCount = totalItemCount;
            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                currentScrollState = scrollState;
                this.isScrollCompleted();
            }

            private void isScrollCompleted() {
                if (currentFirstVisibleItem > currentTotalItemCount - 5 && currentScrollState == SCROLL_STATE_IDLE) {
                    if (!isLoading) {
                        isLoading = true;
                            pageCounter++;
                            getData("deals", pageCounter, "designer");


                    }
                }
            }

        });

        dealArrayList.clear();
        getData("deals", pageCounter, "designer");

//        myDealsButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(myDealsButtonClikable) {
//                    active_type = "designer";
//                    myDealsButton.setTextColor(getResources().getColor(R.color.close_stores_bg));
//                    dealsButton.setTextColor(getResources().getColor(R.color.light_grey));
//                    swipeContainer.setRefreshing(true);
//                    mListView.setVisibility(View.GONE);
//                    myDealsButtonClikable = false;
//                    dealsButtonClikable = true;
//                    myDealsControl=true;
//                    pageCounter=1;
//                    getData("deals", pageCounter,"designer");
//                    ab.show();
//
//
//                }
//            }
//        });
//
//        dealsButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                active_type = "boutique";
//                dealsButton.setTextColor(getResources().getColor(R.color.close_stores_bg));
//                myDealsButton.setTextColor(getResources().getColor(R.color.light_grey));
//                swipeContainer.setRefreshing(true);
//                mListView.setVisibility(View.GONE);
//                myDealsButtonClikable = true;
//                dealsButtonClikable = false;
//                myDealsControl=false;
//                pageCounter=1;
//                getData("deals",pageCounter,"boutique");
//                ab.show();
//
//            }
//        });


        return rootView;
    }

    @Override
    public void onActivityCreated(final Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity = getActivity();
        this.tracker = EasyTracker.getInstance(getActivity());
    }

    @Override
    public void onResume() {
        super.onResume();

        this.tracker.set(Fields.SCREEN_NAME, getClass().getSimpleName());
        this.tracker.send(MapBuilder.createAppView().build());
    }

    @Override
    public void onPause() {
        super.onPause();
//        if(activity != null) {
//            activity.runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    // This code will always run on the UI thread, therefore is safe to modify UI elements.
//                    ((MainActivity) activity).setActionBarCount("");
//                }
//            });
//        }
    }

    @Override
    public void onStart() {
        super.onStart();
        this.tracker.set(Fields.SCREEN_NAME, getClass().getSimpleName());
        this.tracker.send(MapBuilder.createAppView().build());
    }

    public void getData(final String function, final int page, final String dealsType) {
        Log.e("FavouriteTAG", "getDataforfavourite: " );
        swipeContainer.setRefreshing(true);
        if (InternetStatus.getInstance(getActivity()).isOnline()) {
            if (!isLoading) {
                mListView.setVisibility(View.GONE);
            }
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (myDealsControl) {
                        tempMyDealArrayList = DealsDao.getDealsFromServer(function, page, dealsType, "", "", "", "");

                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                swipeContainer.setRefreshing(false);
                                mListView.setVisibility(View.VISIBLE);

//                                if(activity != null) {
//                                    activity.runOnUiThread(new Runnable() {
//                                        @Override
//                                        public void run() {
//                                            // This code will always run on the UI thread, therefore is safe to modify UI elements.
//                                            ((MainActivity) getActivity()).setActionBarCount(dealsCount + " indirim listelendi");
//                                        }
//                                    });
//                                }

                                if (myDealArrayList.size() == 0) {
                                    swipeContainer.setRefreshing(false);
                                    myDealArrayList.addAll(myDealArrayList.size(), tempMyDealArrayList);
                                    mDealListAdapter = new DealListAdapter(context, R.layout.list_item, myDealArrayList);
                                    mListView.setAdapter(mDealListAdapter);
                                    mListView.setVisibility(View.VISIBLE);
                                    isFirstLoad = false;
                                } else {
                                    if (!tempMyDealArrayList.isEmpty()) {
                                        if (isFirstLoad && myDealArrayList.isEmpty()) {
                                            Log.d("isFirstLoad", "111111");
                                            myDealArrayList.addAll(myDealArrayList.size(), tempMyDealArrayList);
                                        } else if (!isFirstLoad && !myDealArrayList.isEmpty()) {
                                            myDealArrayList.clear();
                                            Log.d("isFirstLoad", "222222");
                                            myDealArrayList.addAll(myDealArrayList.size(), tempMyDealArrayList);
                                        }
                                        mDealListAdapter = new DealListAdapter(context, R.layout.list_item, myDealArrayList);
                                        mListView.setAdapter(mDealListAdapter);

                                        if (!myDealArrayList.isEmpty()) {
                                            mListView.setSelection(listViewFirstVisibleItem);
                                        } else {
                                            mListView.setSelection(0);
                                        }
                                    }
                                    swipeContainer.setRefreshing(false);
                                    mListView.setVisibility(View.VISIBLE);
                                    tempMyDealArrayList.clear();
                                    isLoading = false;


                                }
                            }
                        });

                    } else {
                        Log.d("else e", " girdi");
                        tempDealArrayList = DealsDao.getDealsFromServer(function, page, dealsType, "", "", "", "");
                        Log.d("tempDealArrayList.size ", tempDealArrayList.size() + "");


                        handler.post(new Runnable() {
                            @Override
                            public void run() {
//                                if(activity != null) {
//                                    activity.runOnUiThread(new Runnable() {
//                                        @Override
//                                        public void run() {
//                                            // This code will always run on the UI thread, therefore is safe to modify UI elements.
//                                            ((MainActivity) getActivity()).setActionBarCount(dealsCount + " indirim listelendi");
//                                        }
//                                    });
//                                }

                                if (dealArrayList.size() == 0) {
                                    Log.d("aaa", " girdi");
                                    swipeContainer.setRefreshing(false);
                                    dealArrayList.addAll(dealArrayList.size(), tempDealArrayList);
                                    mDealListAdapter = new DealListAdapter(context, R.layout.list_item, dealArrayList);
                                    mListView.setAdapter(mDealListAdapter);

                                    mListView.setVisibility(View.VISIBLE);
                                } else {
                                    Log.d("bbb", " girdi");
                                    if (!tempDealArrayList.isEmpty()) {
                                        Log.d("ccc", " girdi");

                                        dealArrayList.addAll(dealArrayList.size(), tempDealArrayList);

                                        mDealListAdapter = new DealListAdapter(context, R.layout.list_item, dealArrayList);
                                        mListView.setAdapter(mDealListAdapter);


                                        if (!dealArrayList.isEmpty()) {
                                            Log.d("ddd", " girdi");
                                            mListView.setSelection(listViewFirstVisibleItem);
                                        } else {
                                            mListView.setSelection(0);
                                        }
                                    }
                                    swipeContainer.setRefreshing(false);
                                    mListView.setVisibility(View.VISIBLE);
                                    tempDealArrayList.clear();
                                    isLoading = false;

                                }



                            }
                        });



                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }
        else{
            swipeContainer.setRefreshing(false);

        }


}


    private static void setUI(){

        if(!tempDealArrayList.isEmpty()) {


            dealArrayList.addAll(dealArrayList.size(), tempDealArrayList);
            mDealListAdapter.notifyDataSetChanged();

            if(!dealArrayList.isEmpty()){
                mListView.setSelection(listViewFirstVisibleItem);
            }else{
                mListView.setSelection(0);
            }
        }
        swipeContainer.setRefreshing(false);
        mListView.setVisibility(View.VISIBLE);
        tempDealArrayList.clear();
        isLoading = false;
    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {}
    @Override
    public void onDownMotionEvent() {}
    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {
        if (scrollState == ScrollState.UP) {
            if (ab.isShowing()&&(mDealListAdapter.getCount()>5)) {
                ab.hide();
            }
            fab.hide();
        }else if (scrollState == ScrollState.DOWN) {
            if (!ab.isShowing()&&(mDealListAdapter.getCount()>5)) {
                ab.show();
            }
            fab.show();
        }
    }

    public static DealsFragment newInstance(String type) {
        DealsFragment newFragment = new DealsFragment();
        Bundle args = new Bundle();
        newFragment.setArguments(args);
        return newFragment;
    }

    public static void setFavoredFromDealDetail(String id,String favored){
        Iterator iterator = dealArrayList.iterator();
        DealModel tempDeal;
        while (iterator.hasNext()) {
            tempDeal = (DealModel)iterator.next();
            if(tempDeal.id.equals(id)){
                dealArrayList.get(dealArrayList.indexOf(tempDeal)).favored = favored;
            }
        }
        mDealListAdapter.notifyDataSetChanged();
    }


    public void onStop(){
        super.onStop();
        pageCounter=1;
    }


}



