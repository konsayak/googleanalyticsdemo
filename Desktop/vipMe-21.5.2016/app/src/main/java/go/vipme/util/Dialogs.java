package go.vipme.util;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import go.vipme.R;
import go.vipme.activity.ProfileActivity;

/**
 * Created by OguzhanBasar on 27.03.2015.
 */
public class Dialogs {
    private static Dialogs instance = new Dialogs();
    static Context context;
    boolean isAlertShow=false;


    public Dialogs(){

    }

    public static class GenderDialog extends DialogFragment {
        String[] genderArray = MyApplication.getAppContext().getResources().getStringArray(R.array.gender);
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(R.string.gender_title)
                    .setItems(R.array.gender, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            ProfileActivity.DialogOnClick(getTag(),which, genderArray[which]);
                        }
                    });
            return builder.create();
        }

    }

    public static Dialogs  getInstance(Context ctx) {
        context = ctx.getApplicationContext();
        return instance;
    }

    public static class MessageSoundDialog extends DialogFragment {
        String[] messageSoundArray = MyApplication.getAppContext().getResources().getStringArray(R.array.message_sound);
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(R.string.message_sound_title)
                    .setItems(R.array.message_sound, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            ProfileActivity.DialogOnClick(getTag(),which, messageSoundArray[which]);
                        }
                    });
            return builder.create();
        }

    }

    public static class MessageClockDialog extends DialogFragment {
        String[] messageClockArray = MyApplication.getAppContext().getResources().getStringArray(R.array.message_clock);
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(R.string.message_clock_title)
                    .setItems(R.array.message_clock, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            ProfileActivity.DialogOnClick(getTag(),which,messageClockArray[which]);


                        }
                    });

            return builder.create();
        }
    }

    public static void InternetControlDialog(Context context){

            new AlertDialog.Builder(context)
                    .setMessage("İnternet bağlantısı yok.")
                    .setCancelable(false)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();


                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }




}
