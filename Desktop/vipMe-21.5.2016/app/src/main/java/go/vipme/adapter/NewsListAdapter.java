package go.vipme.adapter;

/**
 * Created by OguzhanBasar on 18.03.2015.
 */

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Iterator;

import go.vipme.R;
import go.vipme.activity.NewsWebViewActivity;
import go.vipme.dao.LogShareDao;
import go.vipme.dao.SingleNewsDao;
import go.vipme.dao.TagFollowDao;
import go.vipme.model.NewsModel;
import go.vipme.util.CustomTextView;
import go.vipme.util.Dialogs;
import go.vipme.util.InternetStatus;


public class NewsListAdapter extends ArrayAdapter<NewsModel> {

    Context context;
    ArrayList<CustomTextView> tagList = new ArrayList();
    static ArrayList<NewsModel> newsModels = new ArrayList();
  //  NewsModel mNewsModel;
    boolean isAlertShow=false;

    NewsModel mNewsModel;

    Handler handler = new Handler();

    ImageView favoriteIcon;
    CustomTextView favoriteText,tag;

    public NewsListAdapter(Context context, int textViewResourceId, ArrayList<NewsModel> newsModels){

        super(context,textViewResourceId,newsModels);
        this.newsModels = newsModels;
        this.context = context;

    }

    @Override
    public View getView(final int position,View convertView,ViewGroup parent){

        View v = convertView;

        if(v==null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.news_list_item, null);
        }

        ImageView newsPhoto;
        CustomTextView summaryText;
        RelativeLayout trashContainer1;
        LinearLayout newsContainer,trashContainer2;

        CustomTextView  newsTitle       = (CustomTextView) v.findViewById(R.id.news_title);
        CustomTextView  newsTitleV2     = (CustomTextView) v.findViewById(R.id.news_title_v2);
        LinearLayout    newsClickArea   = (LinearLayout) v.findViewById(R.id.news_click_area);
                        tag             = (CustomTextView) v.findViewById(R.id.tag);
        final LinearLayout    newsesShare               = (LinearLayout) v.findViewById(R.id.newses_share);
        context=getContext();

        newsesShare.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(final View view) {

                if (InternetStatus.getInstance(getContext()).isOnline()) {
                try {
                    mNewsModel = new NewsModel();
                    Thread thread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            mNewsModel = SingleNewsDao.getNewFromServer(newsModels.get(position).id);
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    if (!view.isActivated()) {
                                        view.setActivated(!view.isActivated());
                                    }
                                    Toast.makeText(context, "Paylaşım seçenekleri hazırlanıyor.", Toast.LENGTH_SHORT).show();
                                    LogShareDao.sendShareDataToServer("news", newsModels.get(position).id);

                                    Intent sendIntent = new Intent();
                                    sendIntent.setAction(Intent.ACTION_SEND);
                                    sendIntent.putExtra(Intent.EXTRA_TEXT, mNewsModel.title + " " + mNewsModel.shareUrl + ". Bu haber vipme uygulaması ile bulundu.");
                                    sendIntent.setType("text/plain");
                                    Log.d("", newsModels.get(position).shareUrl + "");
                                    context.startActivity(Intent.createChooser(sendIntent, "Haberi Paylaş"));
                                }
                            });
                        }
                    });
                    thread.start();

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
                else{
                    Dialogs.InternetControlDialog(context);
                }
            }

        });

       // Log.d("newsModelType",newsModels.get(position).type);
        if(newsModels.size()!=0){
        switch (Integer.parseInt(newsModels.get(position).type)) {

            case 0:
                newsTitle.setVisibility(View.VISIBLE);
                newsTitle.setText(newsModels.get(position).title);
                newsContainer = (LinearLayout) v.findViewById(R.id.news_type_0);
                trashContainer1 = (RelativeLayout) v.findViewById(R.id.news_type_1);
                trashContainer2 = (LinearLayout) v.findViewById(R.id.news_type_2);
                summaryText = (CustomTextView) v.findViewById(R.id.news_text_type_0);
                summaryText.setText(newsModels.get(position).summary);
                newsContainer.setVisibility(View.VISIBLE);
                trashContainer1.setVisibility(View.GONE);
                trashContainer2.setVisibility(View.GONE);
                break;
            case 1:
                newsTitle.setVisibility(View.GONE);
                newsTitleV2.setText(newsModels.get(position).title);
                trashContainer1 = (RelativeLayout) v.findViewById(R.id.news_type_1);
                newsContainer = (LinearLayout) v.findViewById(R.id.news_type_0);
                trashContainer2 = (LinearLayout) v.findViewById(R.id.news_type_2);
                newsPhoto = (ImageView) v.findViewById(R.id.news_image_type_1);
                summaryText = (CustomTextView) v.findViewById(R.id.news_text_type_1);
                summaryText.setText(newsModels.get(position).summary);
                try {
                    Picasso.with(context)
                            .load(newsModels.get(position).photoUrl)
                            .into(newsPhoto);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }
                trashContainer1.setVisibility(View.VISIBLE);
                newsContainer.setVisibility(View.GONE);
                trashContainer2.setVisibility(View.GONE);
                break;
            case 2:
                newsTitle.setVisibility(View.VISIBLE);
                newsTitle.setText(newsModels.get(position).title);
                newsContainer = (LinearLayout) v.findViewById(R.id.news_type_2);
                trashContainer1 = (RelativeLayout) v.findViewById(R.id.news_type_1);
                trashContainer2 = (LinearLayout) v.findViewById(R.id.news_type_0);
                newsPhoto = (ImageView) v.findViewById(R.id.news_image_type_2);
                summaryText = (CustomTextView) v.findViewById(R.id.news_text_type_2);
                summaryText.setText(newsModels.get(position).summary);
                try {
                    Picasso.with(context)
                            .load(newsModels.get(position).photoUrl)
                            .into(newsPhoto);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }
                newsContainer.setVisibility(View.VISIBLE);
                trashContainer1.setVisibility(View.GONE);
                trashContainer2.setVisibility(View.GONE);
                break;
            default:
                break;
        }


        if (!newsModels.get(position).tagName.equals("")) {
            tag.setText(newsModels.get(position).tagName);
            tag.setTextSize(context.getResources().getDimension(R.dimen.news_list_tag_text_size));
            tag.setBackgroundResource(R.drawable.news_fragment_tag_bg);
            tag.setPadding(
                context.getResources().getDimensionPixelOffset(R.dimen.tag_left_padding),
                context.getResources().getDimensionPixelOffset(R.dimen.tag_top_padding),
                context.getResources().getDimensionPixelOffset(R.dimen.tag_right_padding),
                context.getResources().getDimensionPixelOffset(R.dimen.tag_bottom_padding)
            );

            if (newsModels.get(position).tagFollowing.equals("1")) {
                v.setActivated(true);
                tag.setTextColor(context.getResources().getColor(R.color.list_option_blue));
            } else {
                v.setActivated(false);
                tag.setTextColor(context.getResources().getColor(R.color.tag_bg));
            }

            tag.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    if (InternetStatus.getInstance(getContext()).isOnline()) {
                        if (newsModels.get(position).tagFollowing.equals("1")) {
                            setBackgroundForAllSameKindTags(newsModels.get(position).tagId, "0");
                            newsModels.get(position).tagFollowing = "0";
                            TagFollowDao.setTagFollow("tag_following", newsModels.get(position).tagId, "0");
                            notifyDataSetChanged();
                        } else {
                            setBackgroundForAllSameKindTags(newsModels.get(position).tagId, "1");
                            newsModels.get(position).tagFollowing = "1";
                            TagFollowDao.setTagFollow("tag_following", newsModels.get(position).tagId, "1");
                            notifyDataSetChanged();
                        }
                    }
                    else{
                        Dialogs.InternetControlDialog(context);
                    }
                }


            });

        } else {
            tag.setText("");
            tag.setBackgroundResource(R.drawable.tag_empty_bg);
            tag.setPadding(0, 0, 0, 0);
            tag.setOnClickListener(null);
        }

        tagList.add(tag);
        }
        newsClickArea.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (InternetStatus.getInstance(getContext()).isOnline()) {
                    Intent intent = new Intent(context, NewsWebViewActivity.class);
                //intent.putExtra("url", newsModels.get(position).newsUrl);
                intent.putExtra("news_id", newsModels.get(position).id);
                /*intent.putExtra("favored", newsModels.get(position).favored);
                intent.putExtra("liked", newsModels.get(position).liked);
                intent.putExtra("disliked", newsModels.get(position).disliked);
                intent.putExtra("title", newsModels.get(position).title);*/
                //ReadDao.sendReadDataToServer(newsModels.get(position).id);
                context.startActivity(intent);
            }
                else{

                    Dialogs.InternetControlDialog(context);
                }
        }

        });

//        if (newsModels.get(position).favored.equals("1")) {
//            favoriteIcon.setImageResource(R.drawable.favorite_icon_remove);
//            favoriteText.setText("Çıkart");
//            favoriteText.setTextColor(context.getResources().getColor(R.color.list_option_blue));
//        } else if (newsModels.get(position).favored.equals("0")) {
//            favoriteIcon.setImageResource(R.drawable.favorite_icon_add);
//            favoriteText.setText("Ekle");
//            favoriteText.setTextColor(context.getResources().getColor(R.color.tag_bg));
//        }

        return v;

    }

    private void setBackgroundForAllSameKindTags(String tagId,String favored){
        Iterator iterator = newsModels.iterator();
        NewsModel tempTag;
        while (iterator.hasNext()) {
            tempTag = (NewsModel)iterator.next();
            if(tempTag.tagId.equals(tagId)){
                newsModels.get(newsModels.indexOf(tempTag)).tagFollowing = favored;
            }
        }
    }

}
