package go.vipme.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import go.vipme.fragment.TagContainerFragment;

/**
 * Created by OguzhanBasar on 21.03.2015.
 */
public class PersonalizePagerAdapter extends FragmentPagerAdapter {

    public PersonalizePagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int index){
        switch(index){
            case 0:
                return new TagContainerFragment().newInstance("Personalize","brands","all",false);
            case 1:
                return new TagContainerFragment().newInstance("Personalize","tags","all",false);
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

}
