package go.vipme.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Locale;

import go.vipme.R;
import go.vipme.constant.GeneralValues;
import go.vipme.dao.BrandDao;
import go.vipme.dao.CouponCodeDao;
import go.vipme.dao.LogShareDao;
import go.vipme.dao.PreferenceDao;
import go.vipme.dao.SingleDealDao;
import go.vipme.dao.StoreOpenedDao;
import go.vipme.dao.TagFollowDao;
import go.vipme.model.BrandModel;
import go.vipme.model.CouponModel;
import go.vipme.model.DealModel;
import go.vipme.util.CustomTextView;
import go.vipme.util.Dialogs;
import go.vipme.util.GPSTracker;
import go.vipme.util.InternetStatus;
import go.vipme.util.MyApplication;

/**
 * Created by OguzhanBasar on 24.03.2015.
 */
public class DealDetailActivity extends Activity {

    String dealId,previousPage;
    Context context;
    Locale trlocale= new Locale("tr-TR");
    SimpleDateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss",trlocale);
    DealModel mDealModel;
    BrandModel mBrandModel;
    CouponModel mCouponModel;
   public static CustomTextView dealTitle,dealBody,dayCount,startDate,favoriteText,closeStoresText;
   public static ImageView dealPhoto,brandPhoto,favoriteIcon,onlineMagazaButton,closeButton;
    RelativeLayout loadingPanel,closeStores,couponCodeLayout;
     public static    LinearLayout endDate,followButton,couponCode;
    ScrollView scrollView;
    TextView couponCodeTextView;
    Handler handler = new Handler();


    final Handler mHandler = new Handler();
    final Runnable mUpdateResults = new Runnable() {
        public void run() {
            setUI();
        }
    };

    @Override
        protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deal_detail);


        Intent intent = getIntent();
        context = this;

        if (intent.getExtras().containsKey("deal_id")){
            dealId = intent.getStringExtra("deal_id");
            Log.e(MyApplication.TAG,"DealID="+dealId);
        }


        previousPage = intent.getStringExtra("calling");

        ImageButton  backButton         = (ImageButton)findViewById(R.id.back_button);
        LinearLayout shareButton        = (LinearLayout)findViewById(R.id.share_button);
        closeStores                     = (RelativeLayout)findViewById(R.id.close_stores);
        endDate                         = (LinearLayout)findViewById(R.id.end_date);
        dealTitle                       = (CustomTextView)findViewById(R.id.deal_detail_title);
        dealPhoto                       = (ImageView)findViewById(R.id.deal_photo);
        brandPhoto                      = (ImageView)findViewById(R.id.brand_photo);
        dealBody                        = (CustomTextView)findViewById(R.id.deal_body);
        dayCount                        = (CustomTextView)findViewById(R.id.day_count);
        startDate                       = (CustomTextView)findViewById(R.id.start_date);
        loadingPanel                    = (RelativeLayout)findViewById(R.id.loadingPanel);
        scrollView                      = (ScrollView)findViewById(R.id.scrollview);
        followButton                    = (LinearLayout)findViewById(R.id.favorite_button);
        favoriteIcon                    = (ImageView)findViewById(R.id.favorite_icon);
        favoriteText                    = (CustomTextView)findViewById(R.id.favorite_text);
        closeStoresText                 = (CustomTextView)findViewById(R.id.close_stores_text);
        onlineMagazaButton              = (ImageView)findViewById(R.id.onlineMagazaButton);
        couponCode                      = (LinearLayout)findViewById(R.id.coupon_code_linear);
        couponCodeLayout                = (RelativeLayout)findViewById(R.id.coupon_code_layout);
        closeStores.setVisibility(View.GONE);
        scrollView.setVisibility(View.GONE);

        getData(dealId);
        registerLocation();
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View button) {
                onBackPressed();

            }
        });

        shareButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View button){
                Toast.makeText(context, "Paylaşım seçenekleri hazırlanıyor.", Toast.LENGTH_SHORT).show();
                LogShareDao.sendShareDataToServer("deal", mDealModel.id);
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, mDealModel.brandName+" "+mDealModel.title+" vipme’de. http://www.vipme.com.tr/f/"+mDealModel.id+" #vipme");
                sendIntent.setType("text/plain");
                context.startActivity(Intent.createChooser(sendIntent, "İndirimi Paylaş"));
            }
        });
        followButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (InternetStatus.getInstance(DealDetailActivity.this).isOnline()) {
                    if (followButton.isActivated()) {
                        followButton.setActivated(false);
                        favoriteIcon.setImageResource(R.drawable.plus_v2);
                        favoriteText.setText("Takip Et");
                        favoriteText.setTextColor(getResources().getColor(R.color.tag_bg));
                        TagFollowDao.setTagFollow("brand_following", mDealModel.brandId, "0");

                    } else {

                        followButton.setActivated(true);
                        favoriteIcon.setImageResource(R.drawable.check_v2);
                        favoriteText.setText("Takibi Bırak");
                        favoriteText.setTextColor(getResources().getColor(R.color.brand_background));
                        TagFollowDao.setTagFollow("brand_following", mDealModel.brandId, "1");

                    }
                }
                else{
                    Dialogs.InternetControlDialog(DealDetailActivity.this);
            }


            }
        });

        brandPhoto.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (InternetStatus.getInstance(DealDetailActivity.this).isOnline()) {
                    Intent intent = new Intent(context, BrandDetailActivity.class);
                intent.putExtra("brand_id", mDealModel.brandId);
                context.startActivity(intent);
            }
                else{
                    Dialogs.InternetControlDialog(DealDetailActivity.this);

                }
        }

        });

        onlineMagazaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (InternetStatus.getInstance(DealDetailActivity.this).isOnline()) {
                StoreOpenedDao.sendStoreDataToServer(mDealModel.id, "0");
                Intent intent = new Intent(context, DealWebViewActivity.class);
                intent.putExtra("url", mDealModel.url);
                context.startActivity(intent);
            }
                else{
                    Dialogs.InternetControlDialog(DealDetailActivity.this);
                }
        }



        });

    }

    @Override
    public void onBackPressed() {
        if (previousPage.equals("deal_list_adapter")) {
            finish();
        }
        else if (previousPage.equals("intent_service")) {
            Intent intent = new Intent(DealDetailActivity.this, MainActivity.class);
            intent.putExtra("previous_page", "deal_detail_activity");
            startActivity(intent);
            finish();
        }
    }



    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance(this).activityStart(this); // Add this method.
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance(this).activityStop(this); // Add this method.
    }

    private void getData(final String dealId) {
        if (InternetStatus.getInstance(this).isOnline()) {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {

                    try {
                        mDealModel = new DealModel();
                        mDealModel = SingleDealDao.getDealFromServer(dealId);
                        mBrandModel = new BrandModel();
                        mBrandModel = BrandDao.getBrandFromServer(mDealModel.brandId);
                        if (mDealModel != null) {
                            mHandler.post(mUpdateResults);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
            thread.start();
        }
        else{

        }
    }
    private void setUI(){

        String str="- -";

        loadingPanel.setVisibility(View.GONE);
        scrollView.setVisibility(View.VISIBLE);

        dealTitle.setText(mDealModel.title);
        dealBody.setText(mDealModel.body);

        Picasso.with(this).load(mDealModel.dealPhoto).into(dealPhoto);
        Picasso.with(this).load(mDealModel.brandPhoto).into(brandPhoto);

        if(mDealModel.showEndDate.equals("1")){
            dayCount.setVisibility(View.VISIBLE);
            dayCount.setText(mDealModel.dateDiffString);
        }

        try{
            str = outputFormat.format(mDealModel.started);
        }catch(Exception e){e.printStackTrace();}

        String[] parts = str.split(" ");
        String start = parts[0];
        String[] parts2 = start.split("-"); //daha güzel çözülebilir.

        startDate.setText(parts2[0] + "." + parts2[1] + "." + parts2[2] + " te başladı");
        if(mBrandModel.following.equals("1")){
            followButton.setActivated(true);
            favoriteIcon.setImageResource(R.drawable.check_v2);
            favoriteText.setText("Takibi Bırak");
            favoriteText.setTextColor(getResources().getColor(R.color.brand_background));
        }
        else{
            followButton.setActivated(false);
            favoriteIcon.setImageResource(R.drawable.plus_v2);
            favoriteText.setText("Takip Et");
            favoriteText.setTextColor(getResources().getColor(R.color.tag_bg));
        }

//Kupon Kodu Kontrolü
    if(mDealModel.couponEnable!=null){
        if(!mDealModel.couponEnable.isEmpty()&& mDealModel.couponEnable.equals("1")) {

            couponCode.setVisibility(View.VISIBLE);
            couponCode.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    closeButton         = (ImageView) findViewById(R.id.coupon_code_close);
                    couponCodeTextView  = (TextView) findViewById(R.id.coupon_code);

                    Thread couponThread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            mCouponModel = new CouponModel();
                            mCouponModel = CouponCodeDao.getCouponCode(dealId);
                            handler.post(new Runnable() {
                                @Override
                                public void run() {

                                    if (mCouponModel != null) {
                                        couponCodeLayout.setVisibility(View.VISIBLE);
                                        couponCodeTextView.setText(mCouponModel.code);
                                        closeButton.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                couponCodeLayout.setVisibility(View.GONE);
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    });
                    couponThread.start();
                }
            });


        }
    }
    else{
            couponCode.setVisibility(View.GONE);
        }

        if(mDealModel.online.equals("0")){

            if (mDealModel.url!=null&& !mDealModel.url.equals("")){
                onlineMagazaButton.setVisibility(View.VISIBLE);
            }
            else {
                onlineMagazaButton.setVisibility(View.INVISIBLE);
            }

            closeStoresText.setText("Yakın Mağazalar");
            closeStores.setVisibility(View.VISIBLE);
            closeStores.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View button) {
                    if (InternetStatus.getInstance(DealDetailActivity.this).isOnline()) {
                    Intent intent = new Intent(DealDetailActivity.this, CloseStoreActivity.class);
                    intent.putExtra("deal_id", dealId);
                    intent.putExtra("brand_id", mDealModel.brandId);
                    intent.putExtra("brand_name", mDealModel.title);
                    intent.putExtra("calling_activity", GeneralValues.DEAL_DETAIL_ACTIVITY);
                    String userLocation = PreferenceDao.getValue("user_location");
                    if (userLocation.equals("") || userLocation.equals("")) {
                        new AlertDialog.Builder(context)
                                .setTitle("Lokasyon Hatası")
                                .setMessage("Konum servislerini açıp tekrar deneyin")
                                .setPositiveButton("Tamam", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                })
                                //.setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    } else {
                        startActivity(intent);
                    }
                }
                    else{
                        Dialogs.InternetControlDialog(DealDetailActivity.this);
                    }
            }

            });
        }

        else if(mDealModel.online.equals("1")){
            closeStoresText.setText("Online Mağazaya Git");
            closeStores.setVisibility(View.VISIBLE);
            closeStores.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View button){

                    StoreOpenedDao.sendStoreDataToServer(mDealModel.id, "0");
                    Intent intent = new Intent(context, DealWebViewActivity.class);
                    intent.putExtra("url", mDealModel.url);
                    context.startActivity(intent);
                }
            });
        }

    }

    private void registerLocation(){

        GPSTracker gps= new GPSTracker(this);
        String mLocation = gps.getLatitude() + "/" + gps.getLongitude();

        if(mLocation != null && mLocation.length() > 0){
            PreferenceDao.setValue("user_location", mLocation);
            Log.e(MyApplication.TAG,"SplashScreenActivity - Newly Setted Location : " + mLocation);
        }else{
            Log.e(MyApplication.TAG,"SplashScreenActivity - Location Did Not Get");
        }

    }


}
