package go.vipme.dao;

import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

import go.vipme.constant.GeneralValues;
import go.vipme.model.ServerResponse;
import go.vipme.util.MyApplication;
import go.vipme.util.ServerUtilFunctions;

/**
 * Created by AhmetOguzhanBasar on 23.06.2015.
 */
public class TagFollowDao {

    public static void setTagFollow(final String function,final String tagId,final String following){
        Log.e(MyApplication.TAG,"TagFollowDao calling");

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    final List<NameValuePair> mParams = new ArrayList<>();
                    if (function.equals("brand_following")) {
                        Log.e(MyApplication.TAG,"TagFollowDao brandFollowing");
                        mParams.add(new BasicNameValuePair("brand_id", tagId));
                    } else if (function.equals("tag_following")) {
                        Log.e(MyApplication.TAG,"TagFollowDao tagFollowing");
                        mParams.add(new BasicNameValuePair("tag_id", tagId));
                    }

                        Log.e(MyApplication.TAG,"ikisinede giremedim");

                    mParams.add(new BasicNameValuePair("following", following));
                    ServerResponse s = ServerUtilFunctions.callServer(GeneralValues.BASE_URL + function, mParams, true);
                    if(!s.success){
                        Log.e("", "TagFollowDao - setTagFollow() - ServerResponse : " + s.error_message);
                    }
                }catch(Exception e){e.printStackTrace();}
            }
        });
        thread.start();

    }

}
