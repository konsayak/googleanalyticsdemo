package go.vipme.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ksoichiro.android.observablescrollview.ObservableListView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.analytics.tracking.android.Tracker;
import com.melnykov.fab.FloatingActionButton;

import java.util.ArrayList;
import java.util.Iterator;

import go.vipme.R;
import go.vipme.activity.MainActivity;
import go.vipme.activity.SearchActivity;
import go.vipme.adapter.NavDrawerListAdapter;
import go.vipme.adapter.NewsListAdapter;
import go.vipme.adapter.TempNewsListAdapter;
import go.vipme.dao.CategoriesDao;
import go.vipme.dao.NewsesDao;
import go.vipme.model.NavigationDrawerListItemModel;
import go.vipme.model.NewsModel;
import go.vipme.util.CustomTextView;
import go.vipme.util.Dialogs;
import go.vipme.util.MyApplication;

public class NewsFragment extends Fragment implements ObservableScrollViewCallbacks {

    ActionBar ab;
    int currentScrollState, currentFirstVisibleItem, currentTotalItemCount;
    static TextView noContentNews;
    Activity activity;
    static Context context;
    static LayoutInflater headerInflater;
    static ObservableListView mListView;
    static View rootView;
    static boolean isLoading = false, loadedOnce = false, swipeToRefresh = false;
    static int listViewFirstVisibleItem;
    FloatingActionButton fab;
    private Tracker tracker;
    boolean isAlertShow=false,control=true;

    CustomTextView myNewsButton, newsButton;
    static boolean myNewsButtonClikable = false, newsButtonClikable = true, myNewsControl = true, newsControl = false;

    public static int pageCounter = 1;
    public static boolean isFirstLoad, isNewsFirstLoad;
    public static String news_category;
    public static ArrayList<NewsModel> newsArrayList;
    public static ArrayList<NewsModel> myNewsArrayList;
    public static NewsListAdapter newsListAdapter;
    public static TempNewsListAdapter geciciCozum;
    public static NavDrawerListAdapter mNavDrawerArrayListAdapter;
    public static ArrayList<NavigationDrawerListItemModel> categoryArray;

    private static ArrayList<NewsModel> tempNewsArrayList;
    private static ArrayList<NewsModel> tempMyNewsArrayList;
    private static SwipeRefreshLayout swipeContainer;
    private static Handler handler = new Handler();
    private static ArrayList<String> navDrawerItems = new ArrayList<>();

    final static Handler mHandler = new Handler();
    final static Runnable mUpdateResults = new Runnable() {
        public void run() {
            setUI();
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        pageCounter=1;
        MainActivity.controlDrawlerClickNews=false;
        rootView = inflater.inflate(R.layout.fragment_newses, container, false);
        mListView = (ObservableListView) rootView.findViewById(R.id.news_list);
        headerInflater = inflater;
        ab = ((MainActivity) getActivity()).actionBar;
        fab = (FloatingActionButton) rootView.findViewById(R.id.fab_newses_fragment);
        myNewsButton = (CustomTextView) rootView.findViewById(R.id.my_news_button);
        newsButton = (CustomTextView) rootView.findViewById(R.id.news_button);
        noContentNews = (TextView) rootView.findViewById(R.id.no_content_news);


        MyApplication.setNewsFragment(this);
        context = getActivity();
        mListView.setScrollViewCallbacks(this);
        ViewGroup header = (ViewGroup) headerInflater.inflate(R.layout.top_padding_layout, mListView, false);
        mListView.addHeaderView(header, null, false);

        isNewsFirstLoad = true;
        newsArrayList = new ArrayList<>();
        myNewsArrayList = new ArrayList<>();
        tempNewsArrayList = new ArrayList<>();
        tempMyNewsArrayList = new ArrayList<>();

        news_category = getArguments().getString("news_category");
        Log.d("news_category", getArguments().getString("news_category"));
        swipeContainer = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeContainer);
        swipeContainer.setProgressViewOffset(false, 0, (int) (getResources().getDimension(R.dimen.top_padding_caused_by_action_bar)));
        swipeContainer.setColorSchemeResources(
                R.color.theme_blue,
                R.color.tag_bg,
                R.color.male_blue);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mListView.setVisibility(View.GONE);
                noContentNews.setVisibility(View.GONE);
                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        pageCounter = 1;
                        swipeToRefresh = true;
                        getData("newses", pageCounter, news_category);
                        ab.show();
                    }
                }, 800);

            }
        });
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isOnline()){
                    Intent i = new Intent(getActivity(), SearchActivity.class);
                    i.putExtra("isDeal", "news");
                    startActivity(i);
                }
                else{
                    if (!isAlertShow){
                        isAlertShow=true;
                        new AlertDialog.Builder(context)
                                .setMessage("İnternet bağlantısı yok.")
                                .setCancelable(false)
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        isAlertShow=false;

                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }
                }
            }
        });

        mListView.setOnScrollListener(new OnScrollListener() {

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                currentFirstVisibleItem = firstVisibleItem;
                listViewFirstVisibleItem = currentFirstVisibleItem;
                currentTotalItemCount = totalItemCount;
            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                currentScrollState = scrollState;
                this.isScrollCompleted();
            }

            private void isScrollCompleted() {
                if (currentFirstVisibleItem > currentTotalItemCount - 5 && currentScrollState == SCROLL_STATE_IDLE) {
                    Log.d("isLoading", isLoading + "");
                    Log.d("pageCounter", pageCounter + "");
                    if (!isLoading) {
                        isLoading = true;
                        Log.d("isLoading", isLoading + " e girdi");
                        pageCounter++;
                        getData("newses", pageCounter, news_category);
                    }
                }

            }

        });



        myNewsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.controlDrawlerClickNews=false;
                MainActivity.mDrawerList.clearChoices();
                if (myNewsButtonClikable) {
                    myNewsButton.setTextColor(getResources().getColor(R.color.close_stores_bg));
                    newsButton.setTextColor(getResources().getColor(R.color.light_grey));
                    swipeContainer.setRefreshing(true);
                    mListView.setVisibility(View.GONE);
                    myNewsButtonClikable = false;
                    newsButtonClikable = true;
                    myNewsControl = true;
                    pageCounter = 1;
                    getData("newses", pageCounter, "personal");
                    MainActivity.changeTopTitle("Dergim");
                    ab.show();
                }
            }
        });

        newsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.controlDrawlerClickNews=false;
                MainActivity.mDrawerList.clearChoices();
                if (newsButtonClikable) {

                    newsButton.setTextColor(getResources().getColor(R.color.close_stores_bg));
                    myNewsButton.setTextColor(getResources().getColor(R.color.light_grey));
                    swipeContainer.setRefreshing(true);
                    mListView.setVisibility(View.GONE);
                    myNewsButtonClikable = true;
                    newsButtonClikable = false;
                    myNewsControl = false;
                    pageCounter = 1;
                    getData("newses", pageCounter, "all");
                    MainActivity.changeTopTitle("Dergim");
                    ab.show();
                }
            }
        });

        MainActivity.mDrawerList.clearChoices();
        getData("newses", pageCounter, news_category);

        return rootView;
    }

    @Override
    public void onActivityCreated(final Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity = getActivity();
        this.tracker = EasyTracker.getInstance(getActivity());
    }

    @Override
    public void onResume() {
        super.onResume();
        this.tracker.set(Fields.SCREEN_NAME, getClass().getSimpleName());
        this.tracker.send(MapBuilder.createAppView().build());
    }


    @Override
    public void onStart() {
        super.onStart();

        if(control){
            myNewsButtonClikable = false;
            newsButtonClikable = true;
            control=false;
        }
        else if (control){
            myNewsButtonClikable = true;
            newsButtonClikable = false;
            control=true;

        }
        this.tracker.set(Fields.SCREEN_NAME, getClass().getSimpleName());
        this.tracker.send(MapBuilder.createAppView().build());
    }

    public void getData(final String function, final int page, final String category) {
        Log.e("NewTAG", "getDataforNews: " );
        swipeContainer.setRefreshing(true);
        noContentNews.setVisibility(View.GONE);
        if (isOnline()){
            if (!isLoading) {
                mListView.setVisibility(View.GONE);
            }
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (!loadedOnce) {
                            loadedOnce = true;
                            Thread thread = new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    categoryArray = CategoriesDao.getCategories("tag");
                                    if (categoryArray != null) {
                                        handler.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                int counter = 0;
                                                navDrawerItems.add("Tümü");
                                                while (!categoryArray.isEmpty() && counter < categoryArray.size()) {
                                                    try {
                                                        navDrawerItems.add(categoryArray.get(counter).name);
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                    counter++;
                                                }
                                                mNavDrawerArrayListAdapter = new NavDrawerListAdapter(context, R.layout.drawer_list_item, navDrawerItems);
                                            }
                                        });
                                    }
                                }
                            });
                            thread.start();
                        }
                        if (myNewsControl) {
                            tempMyNewsArrayList = NewsesDao.getNewsesFromServer(function, page, "personal", category, "", "");
                            if (page == 1) {
                                isNewsFirstLoad = true;
                            }
                            handler.post(new Runnable() {
                                @Override
                                public void run() {


                                    swipeContainer.setRefreshing(true);
                                    mListView.setVisibility(View.VISIBLE);

                                    if (isNewsFirstLoad) {
                                        myNewsArrayList.clear();
                                        swipeContainer.setRefreshing(false);
                                        myNewsArrayList.addAll(myNewsArrayList.size(), tempMyNewsArrayList);
                                        newsListAdapter = new NewsListAdapter(context, R.layout.news_list_item, myNewsArrayList);
                                        mListView.setAdapter(newsListAdapter);
                                        mListView.setVisibility(View.VISIBLE);
                                        isNewsFirstLoad = false;
                                        if (myNewsArrayList.isEmpty()) {
                                            noContentNews.setText("Size özel haberler için kişiselleştir menüsünden ilgi alanı seçmelisiniz");
                                            noContentNews.setVisibility(View.VISIBLE);
                                            // Toast.makeText(context,"Uygun içerik bulunamadı",Toast.LENGTH_SHORT).show();
                                        }
                                        if (!myNewsArrayList.isEmpty()) {
                                            noContentNews.setVisibility(View.GONE);
                                        }
                                    } else {
                                        if (!tempMyNewsArrayList.isEmpty()) {
                                            if (isNewsFirstLoad && myNewsArrayList.isEmpty()) {
                                                myNewsArrayList.addAll(myNewsArrayList.size(), tempMyNewsArrayList);
                                            } else if (!isFirstLoad && !myNewsArrayList.isEmpty()) {
                                                // myNewsArrayList.clear();
                                                Log.d("isFirstLoad", "222222");
                                                myNewsArrayList.addAll(myNewsArrayList.size(), tempMyNewsArrayList);
                                            }
                                            newsListAdapter = new NewsListAdapter(context, R.layout.news_list_item, myNewsArrayList);
                                            mListView.setAdapter(newsListAdapter);
                                            if (!myNewsArrayList.isEmpty()) {
                                                mListView.setSelection(listViewFirstVisibleItem);
                                            } else {
                                                mListView.setSelection(0);
                                            }
                                        } else if (tempMyNewsArrayList.isEmpty() && myNewsArrayList.isEmpty()) {
                                            Toast.makeText(context, "Uygun veri bulunamadı", Toast.LENGTH_SHORT).show();

                                            newsListAdapter = new NewsListAdapter(context, R.layout.news_list_item, myNewsArrayList);
                                            mListView.setAdapter(newsListAdapter);
                                        }
                                        swipeContainer.setRefreshing(false);
                                        mListView.setVisibility(View.VISIBLE);
                                        tempMyNewsArrayList.clear();
                                        isLoading = false;
                                    }
                                }
                            });

                        } else {
                            tempNewsArrayList = NewsesDao.getNewsesFromServer(function, page, "all", category, "", "");
                            if (page == 1) {
                                isNewsFirstLoad = true;
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {


                                        if(isNewsFirstLoad){
                                            newsArrayList.clear();
                                        swipeContainer.setRefreshing(false);
                                        newsArrayList.addAll(newsArrayList.size(),tempNewsArrayList);
                                        newsListAdapter = new NewsListAdapter(context, R.layout.news_list_item, newsArrayList);
                                        mListView.setAdapter(newsListAdapter);
                                        mListView.setVisibility(View.VISIBLE);
                                            isNewsFirstLoad=false;
                                    }
                                        else {
                                        if (!tempNewsArrayList.isEmpty()) {
                                            newsArrayList.addAll(newsArrayList.size(), tempNewsArrayList);
                                            newsListAdapter = new NewsListAdapter(context, R.layout.news_list_item, newsArrayList);
                                            mListView.setAdapter(newsListAdapter);
                                            if (!newsArrayList.isEmpty()) {
                                                mListView.setSelection(listViewFirstVisibleItem);
                                            } else {
                                                mListView.setSelection(0);
                                            }
                                        }
                                        swipeContainer.setRefreshing(false);
                                        mListView.setVisibility(View.VISIBLE);
                                        tempNewsArrayList.clear();
                                        isLoading = false;
                                    }
                                }
                            });
                        }
                    }catch(Exception e){e.printStackTrace();}
                }

            });
            thread.start();
        }
        else{
            swipeContainer.setRefreshing(false);
            Dialogs.InternetControlDialog(context);
        }

    }

    private static void setUI(){
        if(!tempNewsArrayList.isEmpty()) {
            if(swipeToRefresh){
                newsArrayList.clear();
            }
            newsArrayList.addAll(newsArrayList.size(), tempNewsArrayList);
            newsListAdapter.notifyDataSetChanged();
            if(!newsArrayList.isEmpty()) {
                mListView.setSelection(listViewFirstVisibleItem);
            }else{
                if(isFirstLoad) {
                    geciciCozum = new TempNewsListAdapter(MyApplication.getAppContext(), R.layout.news_list_item, newsArrayList);
                    mListView.setAdapter(geciciCozum);
                    isFirstLoad = false;
                }
                mListView.setSelection(0);
            }
        }
        swipeContainer.setRefreshing(false);
        swipeToRefresh = false;
        mListView.setVisibility(View.VISIBLE);
        tempNewsArrayList.clear();
        isLoading = false;
    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {}
    @Override
    public void onDownMotionEvent() {}
    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {
        if ((scrollState == ScrollState.UP)) {
            if (ab.isShowing()&&(newsListAdapter.getCount()>5)) {
                ab.hide();

            }
            fab.hide();
        }else if(scrollState == ScrollState.DOWN) {
            if (!ab.isShowing()&&(newsListAdapter.getCount()>5)){
                ab.show();

            }
            fab.show();
        }
    }

    public static NewsFragment newInstance(String type) {
        NewsFragment newFragment = new NewsFragment();
        Bundle args = new Bundle();
        args.putString("news_category", type);
        newFragment.setArguments(args);
        return newFragment;
    }

    public static void setFavoredFromWebView(String id,String favored){
        Iterator iterator = newsArrayList.iterator();
        NewsModel tempNews;
        while (iterator.hasNext()) {
            tempNews = (NewsModel)iterator.next();
            if(tempNews.id.equals(id)){
                newsArrayList.get(newsArrayList.indexOf(tempNews)).favored = favored;
            }
        }
        newsListAdapter.notifyDataSetChanged();
    }

    public static void setLikedFromWebView(String id,String liked){
        Iterator iterator = newsArrayList.iterator();
        NewsModel tempNews;
        while (iterator.hasNext()) {
            tempNews = (NewsModel)iterator.next();
            if(tempNews.id.equals(id)){
                newsArrayList.get(newsArrayList.indexOf(tempNews)).liked = liked;
                if(liked.equals("1")) {
                    newsArrayList.get(newsArrayList.indexOf(tempNews)).disliked = "0";
                }
            }
        }
        newsListAdapter.notifyDataSetChanged();
    }

    public static void setDislikedFromWebView(String id,String disliked){
        Iterator iterator = newsArrayList.iterator();
        NewsModel tempNews;
        while (iterator.hasNext()) {
            tempNews = (NewsModel)iterator.next();
            if(tempNews.id.equals(id)){
                newsArrayList.get(newsArrayList.indexOf(tempNews)).disliked = disliked;
                if(disliked.equals("1")) {
                    newsArrayList.get(newsArrayList.indexOf(tempNews)).liked = "0";
                }
            }
        }
        newsListAdapter.notifyDataSetChanged();
    }

    public static boolean isOnline() {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    public void onStop(){
        super.onStop();

    }

}



