package go.vipme.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;

import go.vipme.R;
import go.vipme.dao.PreferenceDao;
import go.vipme.dao.ProfileDao;
import go.vipme.dao.TagDao;
import go.vipme.dao.TagFollowDao;
import go.vipme.model.ProfileModel;
import go.vipme.model.TagModel;
import go.vipme.util.CustomTextView;
import go.vipme.util.Dialogs;
import go.vipme.util.InteractiveScrollView;
import go.vipme.util.InternetStatus;
import go.vipme.util.MyApplication;
import go.vipme.util.SystemFunctions;

public class TagContainerFragment extends Fragment implements TextWatcher,TextView.OnEditorActionListener {

    ArrayList<TagModel> tagList = new ArrayList<>(),mTagList;
    String fragmentType,callingPage,fragmentCategoryId;
    Boolean btnControl=true;

    PersonalizeFragment mParentFragment=null;
    CarryFragment myParentFragment=null;
    CustomTextView error;
    InteractiveScrollView mScrollView;
    LinearLayout mLinearLayout;
    SwipeRefreshLayout swipeRefresh;
    Context context;
    Activity activity;
    boolean isAlertShow=false;
    View loading;
    ImageView clearEditTextBtn;
    EditText searchText;
    ImageButton refreshButton;






    private static boolean isComplete=false;
    int width = 0, margin;
    boolean isLoading=false,isSearching=false,isFirstTime=true;
    public static ProfileModel mProfileModel;
    boolean firstGetProfileData=false;
    private Handler handler=new Handler();

    final Runnable mUpdateTagsResults = new Runnable() {
        public void run() {
            searchText.requestFocus();
        }
    };
    final Runnable mUpdateRemoveLinear= new Runnable() {
        public void run() {
            mLinearLayout.removeAllViews();
        }
    };
    final Runnable remoViewRunnable = new Runnable() {
        public void run() {
            mLinearLayout.removeView(loading);
        }
    };
    final Runnable visiblityRunnable = new Runnable() {
        public void run() {

            error.setVisibility(View.GONE);
            mScrollView.setVisibility(View.VISIBLE);
        }
    };

    public static int brandPage=1,brandStart = 0,tagPage = 1,tagStart = 0;

    final Handler mHandler = new Handler();
    final Runnable mUpdateResults = new Runnable() {

        public void run() {

            Log.d("mTagList",mTagList.size()+"");
            if(fragmentType.equals("brands")){
                Log.e(MyApplication.TAG, "UpdateResultBrandStart"+brandStart);
                setTags(brandStart,btnControl);
            }else if(fragmentType.equals("tags")){
                setTags(tagStart,btnControl);
            }
        }
    };

    public static int userBrandFollowingCount=0,userTagFollowingCount=0;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        Log.d(MyApplication.TAG, "TagContainerFragment onCreateView: ");
        final View view                 = inflater.inflate(R.layout.fragment_tag_container,container, false);
        loading                         = inflater.inflate(R.layout.loading,container, false);

        RelativeLayout popularLayout    = (RelativeLayout)view.findViewById(R.id.popular);
        mLinearLayout                   = (LinearLayout)view.findViewById(R.id.linear);
        searchText                      = (EditText)view.findViewById(R.id.search);
        error                           = (CustomTextView)view.findViewById(R.id.error);
        refreshButton                   = (ImageButton)view.findViewById(R.id.refresh_button_on_not_online);
        mScrollView                     = (InteractiveScrollView) view.findViewById(R.id.scroll_view);
        swipeRefresh                    = (SwipeRefreshLayout)view.findViewById(R.id.swipeContainer);
        clearEditTextBtn                = (ImageView)view.findViewById(R.id.clearEditText);

        fragmentType                    = getArguments().getString("type");
        fragmentCategoryId              = getArguments().getString("categoryId");
        callingPage                     = getArguments().getString("page");
        btnControl                      = getArguments().getBoolean("buttonControl");



        Log.d("fragmentType",fragmentType);
        Log.d("fragmentCategoryId",fragmentCategoryId);
        Log.d("fragmentCategoryId",fragmentCategoryId);

        width                           = SystemFunctions.getScreenWidth(getActivity());
        margin                          = SystemFunctions.getScreenHeight(getActivity()) / 100;

        context                         = getActivity();
        activity                        = getActivity();

        error.setVisibility(View.GONE);

        final Fragment fragment = this;


        swipeRefresh.setProgressViewOffset(false, 0, (int) (getResources().getDimension(R.dimen.top_padding_caused_by_action_bar)));
        swipeRefresh.setColorSchemeResources(
                R.color.theme_blue,
                R.color.tag_bg,
                R.color.male_blue);
        swipeRefresh.setEnabled(false);

        mScrollView.setOnBottomReachedListener(new InteractiveScrollView.OnBottomReachedListener() {
            @Override
            public void onBottomReached() {
                if (InternetStatus.getInstance(getActivity()).isOnline()) {
                    if (!isLoading) {
                        Log.d("OnBottomReachedListener", "girdi");
                        mLinearLayout.addView(loading);
                        mScrollView.fullScroll(ScrollView.FOCUS_DOWN);
                        if (fragmentType.equals("brands")) {

                            brandPage++;
                            PreferenceDao.setValue("last_known_brandsPage", brandPage);

                            Log.e(MyApplication.TAG, "brandPage=" + brandPage);
                            getData(fragment, callingPage, fragmentType, brandPage, "", fragmentCategoryId, "", searchText);
                        } else if (fragmentType.equals("tags")) {
                            tagPage++;
                            getData(fragment, callingPage, fragmentType, tagPage, "", fragmentCategoryId, "", searchText);
                        }

                    }
            }
                else{

                }
            }
        });


        if(fragmentType.equals("brands")){
            searchText.setHint(R.string.search_hint_brand);

        }else{
            searchText.setHint(R.string.search_hint_tag);
            popularLayout.setVisibility(View.GONE);
        }
        if (!firstGetProfileData){
            Thread threadProfile=new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        mProfileModel = new ProfileModel();
                        mProfileModel = ProfileDao.getProfileDataFromServer();

                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (mProfileModel != null) {

                                    if(!mProfileModel.brandCount.equals(""))
                                        userBrandFollowingCount=Integer.parseInt(mProfileModel.brandCount);
                                    if(!mProfileModel.tagCount.equals(""))
                                        userTagFollowingCount=Integer.parseInt(mProfileModel.tagCount);

                                    Log.e(MyApplication.TAG,"TagContainerFragment getProfileData " +
                                            "userBrandFollowingCount="+userBrandFollowingCount);
                                    Log.e(MyApplication.TAG,"TagContainerFragment getProfileData " +
                                            "userTagFollowingCount="+userTagFollowingCount);
                                }

                            }
                        });


                    }
                    catch (Exception e){
                        Log.e(MyApplication.TAG,"TagContainerFragment mProfileModel getProfileData error");
                    }
                }
            });
            threadProfile.start();
            firstGetProfileData=true;
        }


        clearEditTextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PreferenceDao.hideSoftKeyboard(context,searchText);
                searchText.setText("");
                isLoading = false;
                isFirstTime = true;

                if (fragmentType.equals("brands")) {

                    brandStart = 0;
                    Log.e(MyApplication.TAG, "ClearEditText TagContainerFragment brands dayız. Keyword=boş");
                    brandPage = 1;
                    getData(fragment, callingPage, fragmentType, brandPage, ",", fragmentCategoryId,"", searchText);


                } else if (fragmentType.equals("tags")) {

                    tagStart = 0;
                    tagPage = 1;
                    Log.e(MyApplication.TAG, "ClearEditText TagContainerFragment tags dayız. Keyword=boş");
                    getData(fragment, callingPage, fragmentType, tagPage, "", fragmentCategoryId,"", searchText);

                } else {
                    Log.e(MyApplication.TAG, "fragmentType BOŞ GELİYOR");
                }

            }
        });


        getData(this, callingPage, fragmentType, brandPage, "",fragmentCategoryId ,"",searchText);

        searchText.addTextChangedListener(this);
        searchText.setOnEditorActionListener(this);




        return view;

    }



    private  void getData(final Fragment fragment, final String callingPage, final String function, final int page, final String type, final String categoryId, final String keyword, final EditText searchText) {

        Log.e("tagcontainerTAG", "getDataforConainer: " );
        if (InternetStatus.getInstance(getActivity()).isOnline()) {
            refreshButton.setVisibility(View.GONE);
            swipeRefresh.setVisibility(View.VISIBLE);
            if (!isLoading) {
                isLoading = true;
                if (isFirstTime) {
                    swipeRefresh.setRefreshing(true);
                }


                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        mTagList = new ArrayList<>();

                        Log.e("aaaaaaaaaaaaa", page + "");

                        mTagList = TagDao.getTagsFromServer(fragment, callingPage, function, page, type, categoryId, keyword);


                        // sakat burası
                        if (page >= 5) {

                            Thread threadRemove = new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    try {

                                        mHandler.post(remoViewRunnable);

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                            threadRemove.start();
                        }

                        Log.e(MyApplication.TAG, "MyTagList DEGERLER=" + mTagList);

                        if (mTagList != null) {

                            Log.e("bbbbbb", "mTaglist is not NULL TAGLIST");
                            mHandler.post(mUpdateResults);

                            Thread thread = new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    try {

                                        mHandler.post(mUpdateTagsResults);

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                            thread.start();

                        }

                    }


                });
                thread.start();
            }
        } else {
            if (InternetStatus.getInstance(getActivity()).isOnline()) {
                refreshButton.setVisibility(View.VISIBLE);
            swipeRefresh.setVisibility(View.GONE);
            refreshButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    getData(TagContainerFragment.this, callingPage, fragmentType, brandPage, "", fragmentCategoryId, "", searchText);

                }
            });
        }
            else{
                refreshButton.setVisibility(View.GONE);
                Dialogs.InternetControlDialog(context);
            }
    }
    }

    private void  checkEditTextIsSame() {


        if (!searchText.getText().toString().equals(PreferenceDao.getValue("keywordStr"))) {

            Log.e(MyApplication.TAG,"TagContainerFragment checkEditTextIsSame searchtext!=keyword" );
            afterTextChanged(searchText.getEditableText());

        }

    }

    private void setTags(final int start,final Boolean buttonControl) {

        final Handler handler = new Handler();
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {

                int recentId = 0, totalWidth = 0;

                final LinearLayout lyTemp = new LinearLayout(context);

                lyTemp.setLayoutParams(new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT));
                lyTemp.setOrientation(LinearLayout.VERTICAL);
                lyTemp.setGravity(Gravity.CENTER_HORIZONTAL);

                TextView mTv;
                RelativeLayout.LayoutParams mLayoutParams;
                RelativeLayout mRelativeLayout = null;
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);

                lp.setMargins(0, margin, 0, 0);



                //getData dan dönen değer
                Log.e(MyApplication.TAG,"mTagList"+mTagList.toString());
                if(mTagList==null||mTagList.size()==0){
                    if (tagList.isEmpty()) {
                        Log.e(MyApplication.TAG, "mTagList is empty");
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                swipeRefresh.setRefreshing(false);
                                mLinearLayout.removeView(loading);
                                mScrollView.setVisibility(View.GONE);
                                if (fragmentType.equals("brands")) {

                                    error.setText("Aranan marka henüz vipme’ de yok");
                                } else if (fragmentType.equals("tags")) {
                                    error.setText("Aranan ilgi alanı henüz vipme’ de yok");

                                }
                                error.setVisibility(View.VISIBLE);

                            }


                        });
                    }


                }else{
                    Log.e(MyApplication.TAG, "setTags mTagList boş değil");
                    tagList.addAll(mTagList);
                    Log.e(MyApplication.TAG, "tagListe EKLEME YAPILDI");
                    Log.e(MyApplication.TAG,"TagList değer="+tagList.toString());


                    Thread threadVisibility = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {

                                mHandler.post(visiblityRunnable);

                            }catch (Exception e){e.printStackTrace();}
                        }
                    });
                    threadVisibility.start();



                }


                int a;


                if(!buttonControl) {
                    for (a = start; a < tagList.size(); a++) {

                        Log.d("tagListSize", "girdi");
                        TagModel mTagModel = tagList.get(a);
                        if (getActivity() == null)
                            return;
                        Activity activity = getActivity();
                        final Resources mResources = activity.getResources();

                        mTv = new TextView(getActivity());
                        mTv.setId(Integer.parseInt(mTagModel.tagId));
                        mTv.setText(mTagModel.brandName);
                        mTv.setTextColor(mResources.getColor(R.color.tag_text));
                        mTv.setTextSize(mResources.getDimension(R.dimen.tag_text_size));
                        if (fragmentType.equals("brands")) {
                            mTv.setBackgroundResource(R.drawable.brand_selector);
                        } else {
                            mTv.setBackgroundResource(R.drawable.tag_selector);
                        }
                        if (mTagModel.following.equals("1")) {
                            mTv.setActivated(true);
                            mTv.setTextColor(mResources.getColor(R.color.white));
                        } else {
                            mTv.setActivated(false);
                            mTv.setTextColor(mResources.getColor(R.color.tag_text));
                        }
                        mTv.setPadding(
                                mResources.getDimensionPixelOffset(R.dimen.tag_left_padding),
                                mResources.getDimensionPixelOffset(R.dimen.tag_top_padding),
                                mResources.getDimensionPixelOffset(R.dimen.tag_right_padding),
                                mResources.getDimensionPixelOffset(R.dimen.tag_bottom_padding)
                        );
                        mTv.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
                        final TextView finalMTv = mTv;
                        mTv.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                if (v.isActivated()) {
                                    v.setActivated(false);
                                    finalMTv.setTextColor(mResources.getColor(R.color.tag_text));
                                    if (fragmentType.equals("brands")) {
                                        try {
                                            Log.e(MyApplication.TAG, "TagContainerFragment brands unfollowing");
                                            userBrandFollowingCount--;

                                            if (mParentFragment != null) {
                                                mParentFragment.userCounter.setText(String.valueOf(userBrandFollowingCount));
                                            }

                                            TagFollowDao.setTagFollow("brand_following", Integer.toString(v.getId()), "0");
                                            Log.e(MyApplication.TAG, "TagContainerFragment userBrandFollowingCount=" +
                                                    userBrandFollowingCount);

                                        } catch (Exception e) {
                                            Log.e(MyApplication.TAG, "TagContainerFragment brandsunfollowing error");
                                            e.printStackTrace();

                                        }
                                    } else {
                                        try {
                                            Log.e(MyApplication.TAG, "TagContainerFragment tags unfollowing");
                                            userTagFollowingCount--;
                                            if (mParentFragment != null) {
                                                mParentFragment.userCounter.setText(String.valueOf(userTagFollowingCount));
                                            }

                                            TagFollowDao.setTagFollow("tag_following", Integer.toString(v.getId()), "0");
                                            Log.e(MyApplication.TAG, "TagContainerFragment userTagFollowingCount=" +
                                                    userTagFollowingCount);
                                        } catch (Exception e) {
                                            Log.e(MyApplication.TAG, "TagContainerFragment tagsunfollowing error");
                                            e.printStackTrace();
                                        }
                                    }
                                } else {
                                    v.setActivated(true);
                                    finalMTv.setTextColor(mResources.getColor(R.color.white));
                                    if (fragmentType.equals("brands")) {
                                        try {
                                            Log.e(MyApplication.TAG, "TagContainerFragment brands following");
                                            userBrandFollowingCount++;
                                            if (mParentFragment != null) {
                                                mParentFragment.userCounter.setText(String.valueOf(userBrandFollowingCount));
                                            }

                                            TagFollowDao.setTagFollow("brand_following", Integer.toString(v.getId()), "1");
                                            Log.e(MyApplication.TAG, "TagContainerFragment userBrandFollowingCount=" +
                                                    userBrandFollowingCount);

                                        } catch (Exception e) {
                                            Log.e(MyApplication.TAG, "TagContainerFragment brandsfollowing error");
                                            e.printStackTrace();
                                        }
                                    } else {
                                        try {
                                            Log.e(MyApplication.TAG, "TagContainerFragment tags following");
                                            userTagFollowingCount++;
                                            if (mParentFragment != null) {
                                                mParentFragment.userCounter.setText(String.valueOf(userTagFollowingCount));
                                            }

                                            TagFollowDao.setTagFollow("tag_following", Integer.toString(v.getId()), "1");
                                            Log.e(MyApplication.TAG, "TagContainerFragment userTagFollowingCount=" +
                                                    userTagFollowingCount);
                                        } catch (Exception e) {
                                            Log.e(MyApplication.TAG, "TagContainerFragment tagsfollowing error");
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }

                        });

                        mLayoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        mLayoutParams.setMargins(margin / 2, 0, margin / 2, 0);


                        if (totalWidth != 0) {
                            mLayoutParams.addRule(RelativeLayout.RIGHT_OF, recentId);
                        }


                        recentId = mTv.getId();

                        if (totalWidth == 0) {
                            try{


                            mRelativeLayout = new RelativeLayout(getActivity());
                            mRelativeLayout.setLayoutParams(lp);
                            mRelativeLayout.setGravity(Gravity.CENTER_HORIZONTAL);
                            lyTemp.addView(mRelativeLayout);
                        }
                            catch (Exception e){
                                e.printStackTrace();
                            }
                        }

                        if (totalWidth + mTv.getMeasuredWidth() + (mLayoutParams.rightMargin * 2) >= width) {
                            totalWidth = 0;
                            a--;

                        } else {
                            mRelativeLayout.addView(mTv, mLayoutParams);
                            totalWidth = totalWidth + mTv.getMeasuredWidth() + (mLayoutParams.rightMargin * 2);

                        }

                    }
                } else {
                    if(start > 0){
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mLinearLayout.removeAllViews();
                            }
                        });
                    }
                    btnControl=false;
                    for (a = 0; a < tagList.size(); a++) {

                        Log.d("tagListSize", "girdi");
                        TagModel mTagModel = tagList.get(a);
                        if (getActivity() == null)
                            return;
                        Activity activity = getActivity();
                        final Resources mResources = activity.getResources();

                        mTv = new TextView(getActivity());
                        mTv.setId(Integer.parseInt(mTagModel.tagId));
                        mTv.setText(mTagModel.brandName);
                        mTv.setTextColor(mResources.getColor(R.color.tag_text));
                        mTv.setTextSize(mResources.getDimension(R.dimen.tag_text_size));
                        if (fragmentType.equals("brands")) {
                            mTv.setBackgroundResource(R.drawable.brand_selector);
                        } else {
                            mTv.setBackgroundResource(R.drawable.tag_selector);
                        }
                        if (mTagModel.following.equals("1")) {
                            mTv.setActivated(true);
                            mTv.setTextColor(mResources.getColor(R.color.white));
                        } else {
                            mTv.setActivated(false);
                            mTv.setTextColor(mResources.getColor(R.color.tag_text));
                        }
                        mTv.setPadding(
                                mResources.getDimensionPixelOffset(R.dimen.tag_left_padding),
                                mResources.getDimensionPixelOffset(R.dimen.tag_top_padding),
                                mResources.getDimensionPixelOffset(R.dimen.tag_right_padding),
                                mResources.getDimensionPixelOffset(R.dimen.tag_bottom_padding)
                        );
                        mTv.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
                        final TextView finalMTv1 = mTv;
                        final TextView finalMTv2 = mTv;
                        mTv.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                if (v.isActivated()) {
                                    v.setActivated(false);
                                    finalMTv1.setTextColor(mResources.getColor(R.color.tag_text));
                                    if (fragmentType.equals("brands")) {
                                        try {
                                            Log.e(MyApplication.TAG, "TagContainerFragment brands unfollowing");
                                            userBrandFollowingCount--;

                                            if (mParentFragment != null) {
                                                mParentFragment.userCounter.setText(String.valueOf(userBrandFollowingCount));
                                            }

                                            TagFollowDao.setTagFollow("brand_following", Integer.toString(v.getId()), "0");
                                            Log.e(MyApplication.TAG, "TagContainerFragment userBrandFollowingCount=" +
                                                    userBrandFollowingCount);

                                        } catch (Exception e) {
                                            Log.e(MyApplication.TAG, "TagContainerFragment brandsunfollowing error");
                                            e.printStackTrace();

                                        }
                                    } else {
                                        try {
                                            Log.e(MyApplication.TAG, "TagContainerFragment tags unfollowing");
                                            userTagFollowingCount--;
                                            if (mParentFragment != null) {
                                                mParentFragment.userCounter.setText(String.valueOf(userTagFollowingCount));
                                            }

                                            TagFollowDao.setTagFollow("tag_following", Integer.toString(v.getId()), "0");
                                            Log.e(MyApplication.TAG, "TagContainerFragment userTagFollowingCount=" +
                                                    userTagFollowingCount);
                                        } catch (Exception e) {
                                            Log.e(MyApplication.TAG, "TagContainerFragment tagsunfollowing error");
                                            e.printStackTrace();
                                        }
                                    }
                                } else {
                                    v.setActivated(true);
                                    finalMTv2.setTextColor(mResources.getColor(R.color.white));
                                    if (fragmentType.equals("brands")) {
                                        try {
                                            Log.e(MyApplication.TAG, "TagContainerFragment brands following");
                                            userBrandFollowingCount++;
                                            if (mParentFragment != null) {
                                                mParentFragment.userCounter.setText(String.valueOf(userBrandFollowingCount));
                                            }

                                            TagFollowDao.setTagFollow("brand_following", Integer.toString(v.getId()), "1");
                                            Log.e(MyApplication.TAG, "TagContainerFragment userBrandFollowingCount=" +
                                                    userBrandFollowingCount);

                                        } catch (Exception e) {
                                            Log.e(MyApplication.TAG, "TagContainerFragment brandsfollowing error");
                                            e.printStackTrace();
                                        }
                                    } else {
                                        try {
                                            Log.e(MyApplication.TAG, "TagContainerFragment tags following");
                                            userTagFollowingCount++;
                                            if (mParentFragment != null) {
                                                mParentFragment.userCounter.setText(String.valueOf(userTagFollowingCount));
                                            }

                                            TagFollowDao.setTagFollow("tag_following", Integer.toString(v.getId()), "1");
                                            Log.e(MyApplication.TAG, "TagContainerFragment userTagFollowingCount=" +
                                                    userTagFollowingCount);
                                        } catch (Exception e) {
                                            Log.e(MyApplication.TAG, "TagContainerFragment tagsfollowing error");
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }

                        });

                        mLayoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        mLayoutParams.setMargins(margin / 2, 0, margin / 2, 0);


                        if (totalWidth != 0) {
                            mLayoutParams.addRule(RelativeLayout.RIGHT_OF, recentId);
                        }


                        recentId = mTv.getId();

                        if (totalWidth == 0) {
                            mRelativeLayout = new RelativeLayout(getActivity());
                            mRelativeLayout.setLayoutParams(lp);
                            mRelativeLayout.setGravity(Gravity.CENTER_HORIZONTAL);
                            lyTemp.addView(mRelativeLayout);

                        }

                        if (totalWidth + mTv.getMeasuredWidth() + (mLayoutParams.rightMargin * 2) >= width) {
                            totalWidth = 0;
                            a--;

                        } else {
                            mRelativeLayout.addView(mTv, mLayoutParams);
                            totalWidth = totalWidth + mTv.getMeasuredWidth() + (mLayoutParams.rightMargin * 2);

                        }

                    }
                }
                //for sonu


                if(fragmentType.equals("brands")){
                    brandStart=a;
                }else if(fragmentType.equals("tags")){
                    tagStart=a;
                }

                handler.post(new Runnable() {


                    @Override
                    public void run() {
                        checkEditTextIsSame();
                        if(isFirstTime){
                            isFirstTime = false;
                            swipeRefresh.setRefreshing(false);
                        }
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    Log.e(MyApplication.TAG,"aaaaaaaaaaaaa");
                                    myParentFragment = ((CarryFragment) getParentFragment());
                                    mParentFragment = ((PersonalizeFragment)(myParentFragment.getParentFragment()));
                                }catch (ClassCastException e){e.printStackTrace();}

                                if(fragmentType.equals("brands")&&mParentFragment!=null){
                                    Log.e(MyApplication.TAG,"aaaaaaaaaaaaa-ifffff");
                                    mParentFragment.userCounter.setText(String.valueOf(userBrandFollowingCount));
                                }else if(fragmentType.equals("tags")&&mParentFragment!=null){
                                    Log.e(MyApplication.TAG,"aaaaaaaaaaaaa-elseiffff");
                                    mParentFragment.userCounter.setText(String.valueOf(userTagFollowingCount));
                                }
                                mLinearLayout.removeView(loading);
                                mLinearLayout.addView(lyTemp);

                            }
                        });
                    }
                });
                isLoading = false;
                isSearching=false;
            }
        });
        thread.start();
        Log.e(MyApplication.TAG,"BrandStart="+brandStart);

        isComplete = true;
    }


    public static TagContainerFragment newInstance(String page,String type, String categoryId, Boolean buttonControl) {

        TagContainerFragment newFragment = new TagContainerFragment();
        Bundle args = new Bundle();
        args.putString("type", type);
        args.putString("page", page);
        args.putString("categoryId",categoryId);
        args.putBoolean("buttonControl",buttonControl);
        newFragment.setArguments(args);

        return newFragment;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (charSequence.length()>0){
            clearEditTextBtn.setVisibility(View.VISIBLE);
        }
        else {
            clearEditTextBtn.setVisibility(View.GONE);
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {

        if (!isSearching){

            isSearching=true;
            brandPage = 1;
            tagPage = 1;
            final String keywordStr = editable.toString();
            PreferenceDao.setValue("keywordStr", keywordStr);


            final Handler handler = new Handler();
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {

                    if (tagList!=null){
                        tagList.clear();
                    }
                    if (mTagList!=null){
                        mTagList.clear();
                    }




                    Thread threadRemove = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {

                                mHandler.post(mUpdateRemoveLinear);
                            }catch (Exception e){e.printStackTrace();}
                        }
                    });
                    threadRemove.start();

                    handler.post(new Runnable() {

                        @Override
                        public void run() {

                            if (keywordStr.equals("")) {
                                isFirstTime = true;

                                swipeRefresh.setRefreshing(true);

                                if (fragmentType.equals("brands")) {

                                    brandStart = 0;

                                    Log.e(MyApplication.TAG, "TagContainerFragment brands dayız. Keyword=boş");

                                    getData(TagContainerFragment.this, callingPage, fragmentType, brandPage, "",fragmentCategoryId, "",searchText);


                                } else if (fragmentType.equals("tags")) {
                                    tagStart = 0;
                                    getData(TagContainerFragment.this, callingPage, fragmentType, tagPage, "",fragmentCategoryId, "", searchText);

                                }

                            } else {
                                isFirstTime = true;

                                if (fragmentType.equals("brands")) {
                                    brandStart = 0;

                                    Log.e(MyApplication.TAG, "TagContainerFragment brands dayız. Keyword=" + keywordStr);

                                    getData(TagContainerFragment.this, callingPage, fragmentType, brandPage, "", fragmentCategoryId, keywordStr, searchText);

                                } else if (fragmentType.equals("tags")) {
                                    tagStart = 0;

                                    getData(TagContainerFragment.this, callingPage, fragmentType, tagPage, "", fragmentCategoryId,keywordStr, searchText);
                                }





                            }


                        }
                    });
                }
            });

            thread.start();

        }
    }

    @Override
    public boolean onEditorAction(TextView textView, int actionID, KeyEvent keyEvent) {
        if (actionID== EditorInfo.IME_ACTION_SEARCH){
            afterTextChanged(textView.getEditableText());

            PreferenceDao.hideSoftKeyboard(context, searchText);
            return true;
        }
        return false;
    }





    @Override
    public void onPause() {

        super.onPause();
        brandPage=1;
        tagPage=1;
    }

    public static boolean isComplete() {
        return isComplete;
    }

}