package go.vipme.fragment;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import go.vipme.R;
import go.vipme.util.CustomTextView;
import go.vipme.util.SystemFunctions;

public class IntroductionInnerFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v                  = inflater.inflate(R.layout.fragment_intro,container,false);
        ImageView phoneImage    = (ImageView)v.findViewById(R.id.intro_phone_image);
        CustomTextView textView = (CustomTextView)v.findViewById(R.id.intro_textview);
        ViewPager pager         = (ViewPager)getActivity().findViewById(R.id.pager);

        int text                = getArguments().getInt("text");
        int image               = getArguments().getInt("image");
        int screenHeight = SystemFunctions.getScreenHeight(getActivity());

        textView.setText(getResources().getString(text));
        phoneImage.setImageDrawable(getResources().getDrawable(image));

        Resources resources     = getActivity().getResources();
        DisplayMetrics metrics  = resources.getDisplayMetrics();

        phoneImage.getLayoutParams().height = (int) (screenHeight * 52.7) / 100;
        textView.setTextSize((int)(screenHeight*3.9)/100/(metrics.densityDpi/160f));
        textView.getLayoutParams().height = pager.getLayoutParams().height-phoneImage.getLayoutParams().height;
        textView.setPadding((screenHeight*2)/100,0,(screenHeight*2)/100,0);

        return v;
    }

}