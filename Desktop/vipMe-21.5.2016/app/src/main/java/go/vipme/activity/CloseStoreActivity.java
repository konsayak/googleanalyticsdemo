package go.vipme.activity;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import go.vipme.R;
import go.vipme.adapter.StoreListAdapter;
import go.vipme.constant.GeneralValues;
import go.vipme.dao.PreferenceDao;
import go.vipme.dao.StoresDao;
import go.vipme.model.StoreModel;
import go.vipme.util.CustomTextView;
import go.vipme.util.Dialogs;
import go.vipme.util.InternetStatus;
import go.vipme.util.MyApplication;

public class CloseStoreActivity extends ListActivity {

    String dealId,brandId,calling_activity;
    ArrayList<StoreModel> mStoreArray;
    StoreListAdapter mStoreListAdapter;
    Context context;

    CustomTextView closeStoreTitle,closeStoresSubtitle;
    RelativeLayout loadingPanel;

    private GoogleMap googleHarita;

    final Handler mHandler = new Handler();
    final Runnable mUpdateResults = new Runnable() {
        public void run() {
            setUI();
        }
    };

    public static String brandName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_close_stores);

        Intent intent = getIntent();
        context = this;
        brandId = intent.getStringExtra("brand_id");
        dealId = intent.getStringExtra("deal_id");
        calling_activity=intent.getStringExtra("calling_activity");
        Log.e(MyApplication.TAG,"Çağıran activity="+calling_activity);

        ImageButton backButton              = (ImageButton)findViewById(R.id.back_button);
                    closeStoreTitle         = (CustomTextView)findViewById(R.id.close_store_title);
                    closeStoresSubtitle     = (CustomTextView)findViewById(R.id.close_store_sub_title);
                    loadingPanel            = (RelativeLayout)findViewById(R.id.loadingPanel);

        backButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v){
                onBackPressed();
                finish();
            }

        });

        switch (calling_activity){
            case GeneralValues.DEAL_DETAIL_ACTIVITY:

                getData(dealId,brandId);
                break;
            case GeneralValues.BRAND_DETAIL_ACTIVITY:
                getData("0",brandId);
                break;
        }






    }

    @Override
    public void onStart() {
        super.onStart();
        EasyTracker.getInstance(this).activityStart(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EasyTracker.getInstance(this).activityStop(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode==1){
            Log.e(MyApplication.TAG,"onActivityResulttayız");
            switch (calling_activity){
                case GeneralValues.DEAL_DETAIL_ACTIVITY:

                    getData(dealId,brandId);
                    break;
                case GeneralValues.BRAND_DETAIL_ACTIVITY:
                    getData("0",brandId);
                    break;
            }
        }
    }

    private void getData(final String DealId,final String BrandId) {
        if (InternetStatus.getInstance(this).isOnline()) {
            loadingPanel.setVisibility(View.VISIBLE);
        if (PreferenceDao.getValue("user_location").equals("0.0/0.0")) {

            showLocationSettingsAlert();

        }

        Log.e(MyApplication.TAG, "LOCATION=" + PreferenceDao.getValue("user_location"));
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    mStoreArray = StoresDao.getStoresFromServer(DealId, BrandId, context);

                    if (mStoreArray != null) {
                        mStoreListAdapter = new StoreListAdapter(context, R.layout.store_list_item, mStoreArray, dealId);
                        mHandler.post(mUpdateResults);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }
        else{
            Dialogs.InternetControlDialog(CloseStoreActivity.this);
        }
    }


    private void showLocationSettingsAlert() {
        final AlertDialog.Builder alertDialog=new AlertDialog.Builder(this);
        alertDialog.setTitle("Uyarı")
                .setMessage("Konum servislerini açıp tekrar deneyin.")

                .setNegativeButton("Hayır", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        onBackPressed();
                    }
                })
                .setPositiveButton("Ayarlar",new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent=new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivityForResult(intent, 1);


                    }
                })
                .show();
    }



    private void setUI(){
        int counter = 0;
        MarkerOptions markerOption;

        googleHarita = ((MapFragment) getFragmentManager().findFragmentById(R.id.map_fragment)).getMap();
        try {
            googleHarita.setMyLocationEnabled(true);
        }catch(Exception e){e.printStackTrace();}

        while(mStoreArray!=null && counter!=mStoreArray.size()){

            markerOption = new MarkerOptions().position(new LatLng(
                    Double.parseDouble(mStoreArray.get(counter).latitude),
                    Double.parseDouble(mStoreArray.get(counter).longitude))).title(mStoreArray.get(counter).name);
            googleHarita.addMarker(markerOption);
            counter++;

        }

        String userLocation = PreferenceDao.getValue("user_location");
        String[] parts = userLocation.split("/");
        try {
            googleHarita.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
                    Double.parseDouble(parts[0]),
                    Double.parseDouble(parts[1])),
                    9f));
        }catch(Exception e){e.printStackTrace();}

        setListAdapter(mStoreListAdapter);
        closeStoreTitle.setText(brandName);
        closeStoresSubtitle.setText("Yakın Mağazalar");
        loadingPanel.setVisibility(View.GONE);
    }



}