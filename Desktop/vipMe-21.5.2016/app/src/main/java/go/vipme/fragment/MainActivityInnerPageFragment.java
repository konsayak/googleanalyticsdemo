package go.vipme.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import go.vipme.R;

public class MainActivityInnerPageFragment extends Fragment {

    View rootView;

    static Bundle args;

    public MainActivityInnerPageFragment() {}

    public static final String ARG_SECTION_NUMBER = "section_number";
    public static MainActivityInnerPageFragment newInstance(int sectionNumber) {
        MainActivityInnerPageFragment fragment = new MainActivityInnerPageFragment();
        args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_special_for_user, container, false);
        return rootView;
    }

}