package go.vipme.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import go.vipme.R;
import go.vipme.model.DealModel;
import go.vipme.util.CustomTextView;

/**
 * Created by OguzhanBasar on 19.04.2015.
 */
public class NavDrawerListAdapter extends ArrayAdapter<String> {

    ArrayList<String> mDraverListItems;

    public NavDrawerListAdapter(Context context,int textViewResourceId,ArrayList<String> draverListItems){

        super(context,textViewResourceId,draverListItems);
        this.mDraverListItems = draverListItems;

    }

    @Override
    public View getView(int position,View convertView,final ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.drawer_list_item, null);
        }

        TextView element = (TextView)v.findViewById(R.id.line);
        element.setText(mDraverListItems.get(position));
        return v;
    }
}
