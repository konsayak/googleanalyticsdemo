package go.vipme.constant;

/**
 * Created by OguzhanBasar on 11.03.2015.
 */
public class GeneralValues {
    public static final String BASE_URL	= "http://markalar.herokuapp.com/api2/";
    public static final String DEVICE_ID	= "device_id";
    public static final String DEVICE_TYPE	= "device_type";
    public static final String DEVICE_MODEL = "device_model";
    public static final String TOKEN = "token";
    public static final String APP_VERSION = "app_version";
    public static final String CARRIER = "carrier";
    public static final String PUSH_ID ="push_id";
    public static final String LAT = "latitude";
    public static final String LNG = "longitude";

    public static final String RESULT_MESSAGE = "resultMessage";

    public static final String VIPME_FACEBOOK ="https://www.facebook.com/vipmetr";
    public static final String VIPME_TWITTER ="https://twitter.com/Vipmetr";
    public static final String VIPME_INSTAGRAM ="https://instagram.com/vipmetr";
    public static final String VIPME_WEBSITE ="http://www.vipme.com.tr/";

    public static final String SENDER_ID ="980108046382";


    //activity constants
    public static final String BRAND_DETAIL_ACTIVITY="bda";
    public static final String DEAL_DETAIL_ACTIVITY="dda";


    //facebook login constants
    public static final String FB_LOGIN_STATUS="fb_login_status";
    public static final String FB_LOGIN="1";
    public static final String FB_LOGOUT="0";

}
