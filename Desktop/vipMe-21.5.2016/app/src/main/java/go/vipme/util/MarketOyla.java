package go.vipme.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import go.vipme.dao.PreferenceDao;

public class MarketOyla {
    public static void marketOyla(final Activity activity){
        if(PreferenceDao.getValue("market_oyla_count").equals("")){
            PreferenceDao.setValue("market_oyla_count", "1");
        }else if(!PreferenceDao.getValue("market_oyla_count").equals("") && !PreferenceDao.getValue("market_oyla_count").equals("10")){
            int marketOylaCount = Integer.parseInt(PreferenceDao.getValue("market_oyla_count"));

            if(marketOylaCount < 1){
                PreferenceDao.setValue("market_oyla_count", "1");
            }else if(marketOylaCount > 0 && marketOylaCount < 9){
                PreferenceDao.setValue("market_oyla_count", String.valueOf(marketOylaCount+1));
            }else if(marketOylaCount == 9){
                AlertDialog.Builder alert = new AlertDialog.Builder(activity);
                alert.setMessage("VIPME'ye markette yorum yaparak ve arkadaşlarınız ile paylaşarak bize destek olabilirsiniz. Teşekkürler.");
                alert.setPositiveButton("Markette Oyla", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Uri uri = Uri.parse("market://details?id=" + activity.getPackageName());
                        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                        try {
                            activity.startActivity(goToMarket);
                        } catch (ActivityNotFoundException e) {
                            activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + activity.getPackageName())));
                        }
                        PreferenceDao.setValue("market_oyla_count", "10");
                    }
                });
                alert.setNegativeButton("İptal", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        PreferenceDao.setValue("market_oyla_count", "1");
                        dialog.cancel();
                    }
                });
                alert.show();
            }
        }
        Log.e(MyApplication.TAG,"MyVIPMessagesActivity - market_oyla_count : "+ PreferenceDao.getValue("market_oyla_count"));
    }
}
