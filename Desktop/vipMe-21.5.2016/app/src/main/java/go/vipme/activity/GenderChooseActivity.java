package go.vipme.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.adjust.sdk.Adjust;
import com.facebook.appevents.AppEventsLogger;

import go.vipme.R;
import go.vipme.dao.GenderDao;

public class GenderChooseActivity extends Activity {


    ImageView maleButton,femaleButton;
    boolean clickable = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_gender_choose);

        maleButton      = (ImageView)findViewById(R.id.male_button);
        femaleButton    = (ImageView)findViewById(R.id.female_button);

        maleButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                if(!v.isActivated()){
                    v.setActivated(!v.isActivated());
                    femaleButton.setActivated(false);
                    GenderDao.sendGenderToServer(2);
                    Intent intent = new Intent(GenderChooseActivity.this, TagSelectActivity.class);
                    intent.putExtra("type", "brands");
                    startActivity(intent);
                    onBackPressed();
                    finish();
                    clickable = true;
                }
            }

        });

        femaleButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (!v.isActivated()) {
                    v.setActivated(!v.isActivated());
                    maleButton.setActivated(false);
                    GenderDao.sendGenderToServer(1);
                    Intent intent = new Intent(GenderChooseActivity.this, TagSelectActivity.class);
                    intent.putExtra("type", "brands");
                    startActivity(intent);
                    onBackPressed();
                    finish();

                    clickable = true;
                }
            }

        });


    }

    @Override
    public void onResume(){
        super.onResume();
        Adjust.onResume();
        AppEventsLogger.activateApp(this, "520852994707454");
    }

}
