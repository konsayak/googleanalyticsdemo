package go.vipme.util;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;

import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import go.vipme.R;
import go.vipme.activity.DealDetailActivity;
import go.vipme.activity.MainActivity;

import go.vipme.constant.GeneralValues;
import go.vipme.dao.PreferenceDao;

import go.vipme.model.NewsModel;

public class GcmIntentService extends IntentService {
    public static final int NOTIFICATION_ID = 1;
    private NotificationManager mNotificationManager;
    private NewsModel mNewsModel;
    private Handler handler=new Handler();


    public GcmIntentService() {
        super(GeneralValues.SENDER_ID);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        String messageType = gcm.getMessageType(intent);

        if (!extras.isEmpty()) {
            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                Log.e(MyApplication.TAG,"Notification Send error: " + extras.toString());
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
                Log.e(MyApplication.TAG,"Notification Deleted messages on server : " + extras.toString());

            } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {

                try{
                    String msg = extras.getString("msg").toString();
                    String type=extras.getString("type").toString();
                    String rid = extras.containsKey("rid") ? extras.getString("rid").toString() : "0";
                    String uid = extras.containsKey("uid") ? extras.getString("uid").toString() : "0";

                    if (uid != "0" && PreferenceDao.getValue("user_id").equals(uid)) {
                        sendNotification(msg, rid, uid,type);
                    }
                    Log.e(MyApplication.TAG, "Notification - msg :" + msg);
                    Log.e(MyApplication.TAG, "Notification - rid :" + rid);
                    Log.e(MyApplication.TAG, "Notification - uid :" + uid);
                    Log.e(MyApplication.TAG,"Notification -type:"+type);
                    Log.e(MyApplication.TAG, "Notification - extras :" + extras.toString());
                }
                catch (Exception e){
                    Log.e(MyApplication.TAG,"GCMIntent Service extras Error");

                }
            }
        }

        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    public void sendNotification(final String message, final String rid, final String uid,String type) {
        mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

        if(rid.equals("0")){
            Intent intent = new Intent(this, MainActivity.class);
            intent.putExtra("uid", uid);
            intent.putExtra("calling", "intent_service");
            createNotificationsToIntent(intent, message);
        } else if (!rid.equals("0")&&type.equals("0")){
            PreferenceDao.setValue("isDeal","yes");
            Intent intent = new Intent(this, DealDetailActivity.class);
            intent.putExtra("deal_id", rid);
            intent.putExtra("uid", uid);
            intent.putExtra("calling", "intent_service");

            createNotificationsToIntent(intent, message);
        } else if (!rid.equals("0")&&type.equals("1")){
            PreferenceDao.setValue("isNews","yes");
            final Intent intent=new Intent(this,MainActivity.class);
            intent.putExtra("calling", "intent_service");
            intent.putExtra("news_id", rid);
            intent.putExtra("isNews","yes");

            createNotificationsToIntent(intent, message);

        }
        else {
            Intent intent=new Intent(this,MainActivity.class);
            intent.putExtra("uid",uid);
            intent.putExtra("calling", "intent_service");
            createNotificationsToIntent(intent, message);
        }


    }



    public void createNotificationsToIntent(Intent intent, String message){

        intent.addFlags(PendingIntent.FLAG_UPDATE_CURRENT);


        Log.e(MyApplication.TAG,"createNotificationsToIntent newsID="+intent.getStringExtra("id"));


        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,intent, Intent.FLAG_ACTIVITY_NEW_TASK);


        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)

                .setSmallIcon(R.drawable.logo)
                .setTicker(message)
                .setContentTitle(this.getString(R.string.app_name))
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(message))
                .setSound(Uri.parse("android.resource://" + "go.vipme" + "/" + R.raw.notification))
                .setAutoCancel(true)
                .setContentText(message);

        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }
}