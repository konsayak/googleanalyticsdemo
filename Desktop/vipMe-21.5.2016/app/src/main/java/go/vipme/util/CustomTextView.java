package go.vipme.util;

/**
 * Created by OguzhanBasar on 05.04.2015.
 */
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class CustomTextView extends TextView {

    Typeface normalTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/HelveticaNeue.otf");
    Typeface boldTypeface = Typeface.createFromAsset(getContext().getAssets(),  "fonts/HelveticaNeue-Bold.otf");

    public CustomTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomTextView(Context context) {
        super(context);
    }

    public void setTypeface(Typeface tf, int style) {
        if (style == Typeface.BOLD) {
            super.setTypeface(boldTypeface/*, -1*/);
        } else {
            super.setTypeface(normalTypeface/*, -1*/);
        }
    }

}
