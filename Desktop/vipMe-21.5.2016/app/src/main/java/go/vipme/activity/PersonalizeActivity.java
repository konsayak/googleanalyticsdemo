package go.vipme.activity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.adjust.sdk.Adjust;
import com.facebook.appevents.AppEventsLogger;
import com.google.analytics.tracking.android.Fields;
import com.google.analytics.tracking.android.MapBuilder;
import com.google.analytics.tracking.android.Tracker;

import go.vipme.R;
import go.vipme.fragment.CarryFragment;
import go.vipme.fragment.TagContainerFragment;
import go.vipme.util.CustomTextView;
import go.vipme.util.Dialogs;
import go.vipme.util.InternetStatus;
import go.vipme.util.MyApplication;

/**
 * Created by OguzhanBasar on 21.03.2015.
 */
public class PersonalizeActivity extends FragmentActivity {



    LinearLayout footerButton;
    public Tracker tracker;
    CustomTextView footerText,brandsButton,tagsButton;

    public String type;
    boolean brand=true,tag=false;

    public static CustomTextView userCounter;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personalize);


        userCounter       = (CustomTextView)findViewById(R.id.user_follow_count);
        footerButton      = (LinearLayout)findViewById(R.id.footer_button);
        brandsButton      = (CustomTextView)findViewById(R.id.brands);
        tagsButton        = (CustomTextView)findViewById(R.id.tags);
        footerText        = (CustomTextView)findViewById(R.id.footer_text);


        if(brand){
            Log.d("P if brand","girdi");
            brandsButton.setTextColor(getResources().getColor(R.color.black));
            tagsButton.setTextColor(getResources().getColor(R.color.light_grey));
            brand=false;
            tag=true;
            userCounter.setText(String.valueOf(TagContainerFragment.userBrandFollowingCount));
            footerText.setText("Adet Markan Var");

            setCurrentFragment(new CarryFragment().newInstance("Personalize","brands"));
        }else if(tag){
            Log.d("P else brand", "girdi ");
            brandsButton.setTextColor(getResources().getColor(R.color.light_grey));
            tagsButton.setTextColor(getResources().getColor(R.color.black));
            brand=true;
            tag=false;
            userCounter.setText(String.valueOf(TagContainerFragment.userTagFollowingCount));
            footerText.setText("Adet İlgi Alanın Var");

            setCurrentFragment(new CarryFragment().newInstance("Personalize","tags"));
        }

        brandsButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v){

                if(brand) {
                    Log.d("brandsButton if brand","oncliclistener");
                    brandsButton.setTextColor(getResources().getColor(R.color.black));
                    tagsButton.setTextColor(getResources().getColor(R.color.light_grey));
                    brand=false;
                    tag=true;
                    userCounter.setText(String.valueOf(TagContainerFragment.userBrandFollowingCount));
                    footerText.setText("Adet Markan Var");

                    setCurrentFragment(new CarryFragment().newInstance("Personalize","brands"));
                }

            }

        });

        tagsButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v){

                if(tag) {
                    Log.d("tagsButton if tag","oncliclistener");
                    brandsButton.setTextColor(getResources().getColor(R.color.light_grey));
                    tagsButton.setTextColor(getResources().getColor(R.color.black));
                    brand=true;
                    tag=false;
                    userCounter.setText(String.valueOf(TagContainerFragment.userTagFollowingCount));
                    footerText.setText("Adet İlgi Alanın Var");

                    setCurrentFragment(new CarryFragment().newInstance("Personalize","tags"));
                }

            }

        });

        footerButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v){
                if (InternetStatus.getInstance(PersonalizeActivity.this).isOnline()) {
                    Intent intent;
                    if (brand) {
                        intent = new Intent(PersonalizeActivity.this, UserFavoredActivity.class);
                        intent.putExtra("type", "tags");
                    } else {
                        intent = new Intent(PersonalizeActivity.this, UserFavoredActivity.class);
                        intent.putExtra("type", "brands");
                    }
                    startActivity(intent);
                }else {
                    Dialogs.InternetControlDialog(PersonalizeActivity.this);
                }
            }

        });


    }
    private void setCurrentFragment(Fragment _fragment){
        FragmentManager manager =getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.pager, _fragment);
        transaction.commit();

    }

    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null &&
                cm.getActiveNetworkInfo().isConnectedOrConnecting();
    }

    @Override
    public void onStart() {
        super.onStart();
        userCounter.setVisibility(View.VISIBLE);
        footerText.setVisibility(View.VISIBLE);
        brandsButton.performClick();
        setCurrentFragment(new CarryFragment().newInstance("Personalize","brands"));
        this.tracker.set(Fields.SCREEN_NAME, getClass().getSimpleName());
        this.tracker.send(MapBuilder.createAppView().build());
    }


    @Override
    public void onPause() {
        super.onPause();
        brandsButton.performClick();
        userCounter.setVisibility(View.INVISIBLE);
        footerText.setVisibility(View.INVISIBLE);
        setCurrentFragment(new CarryFragment().newInstance("Personalize","brands"));


        Log.e(MyApplication.TAG, "Personalize Activity onPause() brandPage="+TagContainerFragment.brandPage);
        Adjust.onPause();
        try {
            ProfileActivity.setUpdate(String.valueOf(TagContainerFragment.userBrandFollowingCount), String.valueOf(TagContainerFragment.userTagFollowingCount));
        }catch(Exception e){e.printStackTrace();}
        try {
            TagContainerFragment.brandStart=0;
            TagContainerFragment.brandPage=1;
        }catch(Exception e){e.printStackTrace();}
        try{
            TagContainerFragment.tagStart=0;
            TagContainerFragment.tagPage=1;
        }catch (Exception e){e.printStackTrace();}
    }

    @Override
    public void onResume(){
        super.onResume();
        Adjust.onResume();



        AppEventsLogger.activateApp(this, "520852994707454");
    }

    public void setUpdate(String count){
        userCounter.setText(count);
    }



}
