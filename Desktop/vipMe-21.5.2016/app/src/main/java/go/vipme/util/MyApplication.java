package go.vipme.util;

import android.app.Application;
import android.content.Context;

import com.adjust.sdk.Adjust;
import com.adjust.sdk.AdjustConfig;
import com.adjust.sdk.LogLevel;
import com.facebook.FacebookSdk;

import go.vipme.fragment.DealsFragment;
import go.vipme.fragment.FavoritesFragment;
import go.vipme.fragment.NewsFragment;

public class MyApplication extends Application{

    private static Context context,mainActivity,personalizeActivity,tagSelectActivity,profileActivity;
    private static NewsFragment newsFragment;
    private static DealsFragment dealsFragment;
    private static FavoritesFragment favoritesFragment;
    public static String TAG = "VIPME";


    public static Context getAppContext(){
        return MyApplication.context;
    }

    public static Context getMainActivity(){
        return MyApplication.mainActivity;
    }

    public static Context getPersonalizeActivity(){
        return MyApplication.personalizeActivity;
    }

    public static Context getTagSelectActivity(){
        return MyApplication.tagSelectActivity;
    }

    public static Context getProfileActivity(){
        return MyApplication.profileActivity;
    }


    public static NewsFragment getNewsFragment(){
        return MyApplication.newsFragment;
    }

    public static DealsFragment getDealsFragment(){
        return MyApplication.dealsFragment;
    }

    public static FavoritesFragment getFavoritesFragment(){
        return MyApplication.favoritesFragment;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        //adjust sdk
        String appToken="gatnceh2g8a2";
        String environment= AdjustConfig.ENVIRONMENT_PRODUCTION;
        AdjustConfig config=new AdjustConfig(this,appToken,environment);
        config.setLogLevel(LogLevel.VERBOSE);
        Adjust.onCreate(config);

        context = getApplicationContext();
        FacebookSdk.sdkInitialize(context);
    }

    public static void setMainActivity(Context mainActivityContext) {
        mainActivity = mainActivityContext;
    }

    public static void setPersonalizeActivity(Context personalizeActivityContext) {
        personalizeActivity = personalizeActivityContext;
    }

    public static void setTagSelectActivity(Context tagSelectActivityContext) {
        tagSelectActivity = tagSelectActivityContext;
    }

    public static void setProfileActivity(Context profileActivityContext) {
        profileActivity = profileActivityContext;
    }

    public static void setNewsFragment(NewsFragment newsFragmentCont) {
        newsFragment = newsFragmentCont;
    }

    public static void setFavoritesFragment(FavoritesFragment favoritesFragmentCont) {
        favoritesFragment = favoritesFragmentCont;
    }

    public static void setDealsFragment(DealsFragment dealsFragmentCont) {
        dealsFragment = dealsFragmentCont;
    }



}