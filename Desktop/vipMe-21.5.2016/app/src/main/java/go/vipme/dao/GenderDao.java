package go.vipme.dao;

import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

import go.vipme.constant.GeneralValues;
import go.vipme.model.ServerResponse;
import go.vipme.util.ServerUtilFunctions;

/**
 * Created by AhmetOguzhanBasar on 25.06.2015.
 */
public class GenderDao {

    public static void sendGenderToServer(int choosedGender){
        final List<NameValuePair> mParams = new ArrayList<>();
        mParams.add(new BasicNameValuePair("gender", choosedGender + ""));

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    ServerResponse s = ServerUtilFunctions.callServer(GeneralValues.BASE_URL + "set_gender", mParams, true);
                    if(!s.success){
                        Log.e("", "GenderDao - sendGenderToServer() - ServerResponse : " + s.error_message);
                    }
                }catch(Exception e){e.printStackTrace();}
            }
        });
        thread.start();
    }

}
