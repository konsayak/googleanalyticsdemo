package go.vipme.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.adjust.sdk.Adjust;
import com.facebook.appevents.AppEventsLogger;

import java.util.ArrayList;

import go.vipme.R;
import go.vipme.adapter.MainActivityPagerAdapter;
import go.vipme.dao.PreferenceDao;
import go.vipme.fragment.DealsFragment;
import go.vipme.fragment.NewsFragment;
import go.vipme.util.CustomTextView;
import go.vipme.util.LocationService;
import go.vipme.util.MyApplication;
import go.vipme.util.NetworkChangeReceiver;

/**
 * Created by OguzhanBasar on 16.03.2015.
 */
public class MainActivity extends ActionBarActivity implements ActionBar.TabListener {
    boolean isAlertShow=false;
    MainActivityPagerAdapter mSectionsPagerAdapter;
    ViewPager mViewPager;
    public static String lastCategoryItemDeal ="İndirimlerim";
    public static String lastCategoryItemNew="Dergim";
    NetworkChangeReceiver receiver;
   public static boolean controlDrawlerClickDeals =false;
    public static boolean controlDrawlerClickNews=false;
    ImageView imageButton, drawerButton;

    int i;

    Boolean firstOpen = true;

    public  ActionBar actionBar;
    private final static String DEBUG_TAG = "FirstLifeLog";

    static LayoutInflater headerInflater;
    static CustomTextView actionBarTitle;

    public static DrawerLayout mDrawerLayout;
    public static ListView mDrawerList;
    private Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);




        MyApplication.setMainActivity(this);

        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        receiver = new NetworkChangeReceiver();
        registerReceiver(receiver, filter);


        context = MyApplication.getMainActivity();
        Intent serviceIntent = new Intent(this, LocationService.class);
        startService(serviceIntent);
        actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS | ActionBar.NAVIGATION_MODE_STANDARD);
        headerInflater = getLayoutInflater();



            LayoutInflater yeniInflater = LayoutInflater.from(this);
            View yeniView               = yeniInflater.inflate(R.layout.action_bar_layout, null);

            actionBarTitle              = (CustomTextView) yeniView.findViewById(R.id.action_bar_title);
            mViewPager                  = (ViewPager) findViewById(R.id.pager);
            mDrawerLayout               = (DrawerLayout) findViewById(R.id.drawer_layout);
            mDrawerList                 = (ListView) findViewById(R.id.list_slidermenu);
            imageButton                 = (ImageView) yeniView.findViewById(R.id.imageButton);
            drawerButton                = (ImageView) yeniView.findViewById(R.id.drawerButton);


            mSectionsPagerAdapter = new MainActivityPagerAdapter(getSupportFragmentManager());
            mViewPager.setOffscreenPageLimit(3);
            mViewPager.setAdapter(mSectionsPagerAdapter);


            if(firstOpen){
                actionBarTitle.setText("İndirimlerim");
                imageButton.setVisibility(View.GONE);
                drawerButton.setVisibility(View.VISIBLE);
                mDrawerList.setAdapter(((DealsFragment) mSectionsPagerAdapter.getItem(0)).mNavDrawerArrayListAdapter);
                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                mSectionsPagerAdapter.notifyDataSetChanged();
                firstOpen=false;

            }

            imageButton.setVisibility(View.GONE);
            drawerButton.setVisibility(View.VISIBLE);
            mSectionsPagerAdapter.notifyDataSetChanged();



            ViewGroup header = (ViewGroup) headerInflater.inflate(R.layout.top_padding_layout, mDrawerList, false);

            mDrawerList.addHeaderView(header, null, false);

            mDrawerList.setOnItemClickListener(new ListView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView parent, View view, int position, long id) {
                    selectItem(position);
                    Log.d("position",position+"");
                }

            });



            mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {


                @Override
                public void onPageSelected(int position) {

                    actionBar.setSelectedNavigationItem(position);
                    if (!actionBar.isShowing()) {

                        actionBar.show();

                    }
                    if (mDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {
                        mDrawerLayout.closeDrawer(Gravity.RIGHT);
                    }


                    if (position == 0) {
                        if (!controlDrawlerClickDeals)
                                actionBarTitle.setText("İndirimlerim");
                            else if(controlDrawlerClickDeals){
                                actionBarTitle.setText(lastCategoryItemDeal);


                            }

                        imageButton.setVisibility(View.GONE);
                        drawerButton.setVisibility(View.VISIBLE);
                        mDrawerList.setAdapter(((DealsFragment) mSectionsPagerAdapter.getItem(position)).mNavDrawerArrayListAdapter);
                        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                        mSectionsPagerAdapter.notifyDataSetChanged();


                    }
                    else if (position == 1) {

                            if (!controlDrawlerClickNews)
                                actionBarTitle.setText("Dergim");
                            else if (controlDrawlerClickNews) {
                                actionBarTitle.setText(lastCategoryItemNew);

                            }


                        imageButton.setVisibility(View.GONE);
                        drawerButton.setVisibility(View.VISIBLE);
                        mDrawerList.setAdapter(((NewsFragment) mSectionsPagerAdapter.getItem(position)).mNavDrawerArrayListAdapter);
                        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                        mSectionsPagerAdapter.notifyDataSetChanged();

                    }
                    else if (position == 2) {
                        actionBarTitle.setText("Butikler/Tasarımcılar");
                        drawerButton.setVisibility(View.GONE);
                        imageButton.setVisibility(View.GONE);
                        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                        mSectionsPagerAdapter.notifyDataSetChanged();

                    }
                    else if (position == 3) {
                        actionBarTitle.setText("Kişiselleştir");
                        drawerButton.setVisibility(View.GONE);
                        imageButton.setVisibility(View.VISIBLE);
                        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                        changeTopTitle();
                    }
                }



            });

            ArrayList<Drawable> tabsIconList = new ArrayList<>();

            tabsIconList.add(getResources().getDrawable(R.drawable.deals));
            tabsIconList.add(getResources().getDrawable(R.drawable.news));
            tabsIconList.add(getResources().getDrawable(R.drawable.favorite));
            tabsIconList.add(getResources().getDrawable(R.drawable.profile));

            for (i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
                actionBar.addTab(
                        actionBar.newTab()
                                .setIcon(tabsIconList.get(i))
                                .setTabListener(this));
            }
            actionBar.setDisplayShowHomeEnabled(false);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setCustomView(yeniView);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBarTitle.setText("İndirimlerim");
            actionBarTitle.setTextSize(this.getResources().getDimension(R.dimen.action_bar_title_size));

            imageButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if(isOnline()){
                    Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
                    startActivity(intent);
                }
                    else{
                        if (!isAlertShow){
                            new AlertDialog.Builder(context)
                                    .setMessage("İnternet bağlantısı yok.")
                                    .setCancelable(false)
                                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                            isAlertShow=true;

                                        }
                                    })
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .show();
                        }
                    }
            }

            });

            drawerButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (mDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {
                        mDrawerList.setSelection(mDrawerList.getSelectedItemPosition());

                        mDrawerLayout.closeDrawer(Gravity.RIGHT);
                    } else {
                        mDrawerList.setSelection(mDrawerList.getSelectedItemPosition());
                        mDrawerLayout.openDrawer(Gravity.RIGHT);
                    }
                }

            });




    }

    @Override
    public void onNewIntent(Intent intent) {
        Log.e(MyApplication.TAG, "MainActivity -- onNewIntent");
        Bundle extras = intent.getExtras();
        if (extras != null) {
            if (extras.containsKey("isNews")) {
                try {
                    Log.e(MyApplication.TAG, "MainActivity -- onNewIntent isNews");
                    String isNews = extras.getString("isNews");
                    if (isNews.equals("yes")) {
                        Log.e(MyApplication.TAG, "MainActivity -- onNewIntent isNews yes");
                        String newsID = extras.getString("news_id");

                        if (!newsID.equals("") && newsID != null) {
                            Intent innerIntent = new Intent(context, NewsWebViewActivity.class);
                            innerIntent.putExtra("news_id", newsID);
                            this.getIntent().removeExtra("news_id");
                            context.startActivity(innerIntent);

                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        Log.e(DEBUG_TAG, "onresume executes ...");
        onNewIntent(getIntent());
        Adjust.onResume();
        AppEventsLogger.activateApp(this, "520852994707454");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_activity_actions, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (item != null && id == android.R.id.home) {
            if (mDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {
                mDrawerLayout.closeDrawer(Gravity.RIGHT);
            }
            else {
                mDrawerLayout.openDrawer(Gravity.RIGHT);
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    public static void changeTopTitle() {
        if (PreferenceDao.getValue("user_name").length() > 2) {
            actionBarTitle.setText(PreferenceDao.getValue("user_name"));
        }
        else {
            actionBarTitle.setText("Kişiselleştir");
        }

    }

    public static void changeTopTitle(String _title) {
        actionBarTitle.setText(_title);
    }

    public boolean isOnline() {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }


    private void selectItem(int position) {

        if (position == 1 && mViewPager.getCurrentItem() == 0) {
            ((DealsFragment) mSectionsPagerAdapter.getItem(0)).dealArrayList.clear();
            ((DealsFragment) mSectionsPagerAdapter.getItem(0)).mDealListAdapter.notifyDataSetChanged();
            ((DealsFragment) mSectionsPagerAdapter.getItem(0)).deal_category = "0";
            ((DealsFragment) mSectionsPagerAdapter.getItem(0)).getData("deals", 1, "0");
            ((DealsFragment) mSectionsPagerAdapter.getItem(0)).pageCounter = 1;
            changeTopTitle("Tüm İndirimler");
            lastCategoryItemDeal ="Tüm İndirimler";
            controlDrawlerClickDeals=true;
        }
        else if (position != 1 && mViewPager.getCurrentItem() == 0) {
            ((DealsFragment) mSectionsPagerAdapter.getItem(0)).dealArrayList.clear();
            String category_id = ((DealsFragment) mSectionsPagerAdapter.getItem(0)).categoryArray.get(position - 2).id;
            ((DealsFragment) mSectionsPagerAdapter.getItem(0)).deal_category = category_id;
            ((DealsFragment) mSectionsPagerAdapter.getItem(0)).getData("deals", 1, category_id);
            ((DealsFragment) mSectionsPagerAdapter.getItem(0)).pageCounter = 1;
            changeTopTitle(((DealsFragment) mSectionsPagerAdapter.getItem(0)).categoryArray.get(position - 2).name);
            lastCategoryItemDeal =((DealsFragment) mSectionsPagerAdapter.getItem(0)).categoryArray.get(position - 2).name;
            controlDrawlerClickDeals=true;
        }


        if (position == 1 && mViewPager.getCurrentItem() == 1) {
            ((NewsFragment) mSectionsPagerAdapter.getItem(1)).newsArrayList.clear();
            ((NewsFragment) mSectionsPagerAdapter.getItem(1)).newsListAdapter.notifyDataSetChanged();
            ((NewsFragment) mSectionsPagerAdapter.getItem(1)).news_category = "0";
            ((NewsFragment) mSectionsPagerAdapter.getItem(1)).getData("newses", 1, "0");
            ((NewsFragment) mSectionsPagerAdapter.getItem(1)).pageCounter = 1;
            changeTopTitle("Tüm Haberler");
            lastCategoryItemNew="Tüm Haberler";
            controlDrawlerClickNews=true;
        }
        else if (position != 1 && mViewPager.getCurrentItem() == 1) {
            ((NewsFragment) mSectionsPagerAdapter.getItem(1)).newsArrayList.clear();
            ((NewsFragment) mSectionsPagerAdapter.getItem(1)).newsListAdapter.notifyDataSetChanged();
            String category_id = ((NewsFragment) mSectionsPagerAdapter.getItem(1)).categoryArray.get(position - 2).id;
            ((NewsFragment) mSectionsPagerAdapter.getItem(1)).news_category = category_id;
            ((NewsFragment) mSectionsPagerAdapter.getItem(1)).getData("newses", 1, category_id);
            ((NewsFragment) mSectionsPagerAdapter.getItem(1)).pageCounter = 1;
            changeTopTitle(((NewsFragment) mSectionsPagerAdapter.getItem(1)).categoryArray.get(position - 2).name);
            lastCategoryItemNew=((NewsFragment) mSectionsPagerAdapter.getItem(1)).categoryArray.get(position - 2).name;
            controlDrawlerClickNews=true;
        }

        if (mDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {
            mDrawerList.setSelection(MainActivity.mDrawerList.getSelectedItemPosition());
            mDrawerLayout.closeDrawer(Gravity.RIGHT);
        }

    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e(DEBUG_TAG, "onDestroy executes ...");
        unregisterReceiver(receiver);
        }

    @Override
    public void onBackPressed() {


       super.onBackPressed();
        Log.e(DEBUG_TAG, "onback executes ...");
    }

}
