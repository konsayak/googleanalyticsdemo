package go.vipme.util;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

public class SystemFunctions {

    public static String getVersionName(){
        String versionName="";
        try {
            versionName = MyApplication.getAppContext().getPackageManager().getPackageInfo(MyApplication.getAppContext().getPackageName(), 0).versionName;
            Log.e(MyApplication.TAG,"SystemFunctions - VersionName - Version : " + versionName);
        }catch (PackageManager.NameNotFoundException e1) {
            Log.e(MyApplication.TAG, "SystemFunctions - VersionName - Error : " + e1.toString());
        }
        return versionName;
    }

    public static int getScreenWidth(Activity activity) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int screenWidth = displaymetrics.widthPixels;
        Log.e(MyApplication.TAG,"SystemFunctions - Display Width : " + screenWidth);
        return screenWidth;
    }

    public static int getScreenHeight(Activity activity) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int screenHeight = displaymetrics.heightPixels;
        Log.e(MyApplication.TAG,"SystemFunctions - Display Height : " + screenHeight);
        return screenHeight;
    }

    public static String getCarrierValue(){
        String carrier = "";
        TelephonyManager manager = (TelephonyManager)MyApplication.getAppContext().getSystemService(Context.TELEPHONY_SERVICE);
        try{
            carrier = manager.getNetworkOperatorName();
        }catch(NullPointerException e){e.printStackTrace();}
        Log.e(MyApplication.TAG,"SystemFunctions - Carrier :" + carrier);
        return carrier;
    }

    public static String getDeviceId(){
        String deviceId="";
        try{
            deviceId = Secure.getString(MyApplication.getAppContext().getContentResolver(), Secure.ANDROID_ID);
        }catch(NullPointerException e){e.printStackTrace();}
        Log.e(MyApplication.TAG,"SystemFunctions - Device Id :" + deviceId);
        return deviceId;
    }

    public static String getDeviceName(){
        String deviceName = android.os.Build.MODEL;
        String deviceMan = android.os.Build.MANUFACTURER;
        deviceMan = deviceMan.toUpperCase();
        Log.e(MyApplication.TAG,"SystemFunctions - Device Manifacturer / Device Model :" + deviceMan + " / " + deviceName);
        return deviceMan+" / "+deviceName;
    }

}