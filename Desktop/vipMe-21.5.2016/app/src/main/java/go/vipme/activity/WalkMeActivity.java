
package go.vipme.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.adjust.sdk.Adjust;
import com.facebook.appevents.AppEventsLogger;

import go.vipme.R;
import go.vipme.dao.PreferenceDao;

/**
 * Created by OguzhanBasar on 13.04.2015.
 */
public class WalkMeActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_walk_me);

        ImageView closeButton = (ImageView)findViewById(R.id.walk_me_close_button);

        PreferenceDao.setValue("first_time", "not");

        closeButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v){

                Intent intent = new Intent(WalkMeActivity.this, MainActivity.class);
                intent.putExtra("previous_page","walk_me");
                startActivity(intent);
                onBackPressed();
                finish();

            }

        });

    }

    @Override
    public void onResume(){
        super.onResume();
        Adjust.onResume();
        AppEventsLogger.activateApp(this, "520852994707454");
    }

}
