package go.vipme.dao;

import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import go.vipme.constant.GeneralValues;
import go.vipme.model.CouponModel;
import go.vipme.model.ServerResponse;
import go.vipme.util.JsonFunctions;
import go.vipme.util.ServerUtilFunctions;

/**
 * Created by Egemen on 28.01.2016.
 */
public class CouponCodeDao {
    public static CouponModel getCouponCode(String dealId){
        CouponModel mCouponModel= null;
        String function = "get_coupon_code";

        final List<NameValuePair> mParams = new ArrayList<>();
        mParams.add(new BasicNameValuePair("deal_id", dealId));

        try {
            ServerResponse s = ServerUtilFunctions.callServer(GeneralValues.BASE_URL + function, mParams, true);
            if(s.success) {
                mCouponModel = new CouponModel();
                JSONObject mJSONObject = JsonFunctions.getJsonObjectFromObject(s.json, "data");
                mCouponModel = couponJsonParse(mJSONObject);
            }else{
                Log.e("", "BrandDao - getBrandFromServer() - ServerResponse : " + s.error_message);
            }
        }catch(Exception e){e.printStackTrace();}

        return mCouponModel;

    }

    private static CouponModel couponJsonParse(JSONObject jsonObject){

        CouponModel couponModel = null;
        try {
            couponModel = new CouponModel();
            couponModel.code = jsonObject.getString("code");

        }catch (JSONException e){e.printStackTrace();}

        return couponModel;

    }
}
