package go.vipme.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import go.vipme.activity.PersonalizeActivity;
import go.vipme.fragment.CarryFragment;
import go.vipme.fragment.TagContainerFragment;

/**
 * Created by OguzhanBasar on 22.03.2015.
 */
public class TagSelectPagerAdapter extends FragmentPagerAdapter {

    public TagSelectPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int index){
        switch(index){
            case 0:
                return new CarryFragment().newInstance("Personalize","brands");
            case 1:
                return new CarryFragment().newInstance("Personalize","tags");
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

}
