package go.vipme.dao;

import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

import go.vipme.constant.GeneralValues;
import go.vipme.model.ServerResponse;
import go.vipme.util.ServerUtilFunctions;

public class ReadDao {

    public static void sendReadDataToServer(final String newsId){
        final List<NameValuePair> mParams = new ArrayList<>();
        mParams.add(new BasicNameValuePair("news_id", newsId));

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    ServerResponse s = ServerUtilFunctions.callServer(GeneralValues.BASE_URL + "news", mParams, true);
                    if(!s.success){
                        Log.e("", "ReadDao - sendReadDataToServer() - ServerResponse : " + s.error_message);
                    }
                }catch(Exception e){e.printStackTrace();}
            }
        });
        thread.start();
    }

}
