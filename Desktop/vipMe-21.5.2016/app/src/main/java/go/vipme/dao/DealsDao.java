package go.vipme.dao;

import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import go.vipme.constant.GeneralValues;
import go.vipme.model.DealModel;
import go.vipme.model.ServerResponse;
import go.vipme.util.JsonFunctions;
import go.vipme.util.ServerUtilFunctions;

public class DealsDao {



    public static ArrayList<DealModel> getDealsFromServer(String function,int page,String type,String brandId,String categoryId,String sinceDate,String keyword){
        ArrayList<DealModel> mDealArrayList= null;
        String mPage = "";
        if (page != -1) {
            mPage = Integer.toString(page);
        }
        final List<NameValuePair> mParams = new ArrayList<>();
        mParams.add(new BasicNameValuePair("page", mPage));
        mParams.add(new BasicNameValuePair("type", type));
        mParams.add(new BasicNameValuePair("brand_id", brandId));
        mParams.add(new BasicNameValuePair("category_id", categoryId));
        mParams.add(new BasicNameValuePair("since_date", sinceDate));
        mParams.add(new BasicNameValuePair("keyword", keyword));

        try {
            ServerResponse s = ServerUtilFunctions.callServer(GeneralValues.BASE_URL + function, mParams, true);
            if(s.success) {
                mDealArrayList = new ArrayList<>();
                JSONObject mJSONObject = JsonFunctions.getJsonObjectFromObject(s.json, "data");

                mDealArrayList = dealJsonParse(JsonFunctions.getArrayFromObject(mJSONObject, "deals"));
                PreferenceDao.setValue("user_id", JsonFunctions.getStringValue(mJSONObject, "user_id"));
            }else{
                Log.e("","DealsDao - getDealsFromServer() - ServerResponse : "+s.error_message);
            }
        }catch(Exception e){e.printStackTrace();}

        return mDealArrayList;

    }

    private static ArrayList<DealModel> dealJsonParse(JSONArray jsonArray){

        DealModel dealModel;
        ArrayList<DealModel> mDealList = new ArrayList<>();
        int counter=0;

        while(jsonArray!=null && counter!=jsonArray.length()){
            dealModel = new DealModel();
            try {
                dealModel.id = jsonArray.getJSONObject(counter).getString("id");
                dealModel.body = jsonArray.getJSONObject(counter).getString("body");
                dealModel.title = jsonArray.getJSONObject(counter).getString("title");
                dealModel.brandName = jsonArray.getJSONObject(counter).getString("brand_name");
                dealModel.brandId = jsonArray.getJSONObject(counter).getString("brand_id");
                dealModel.brandPhoto = jsonArray.getJSONObject(counter).getString("brand_photo");
                dealModel.type = jsonArray.getJSONObject(counter).getString("type");
                dealModel.url = jsonArray.getJSONObject(counter).getString("url");
                dealModel.online = jsonArray.getJSONObject(counter).getString("online");
                dealModel.dealPhoto = jsonArray.getJSONObject(counter).getString("deal_photo");
                dealModel.favored = jsonArray.getJSONObject(counter).getString("favored");
                mDealList.add(dealModel);
            }catch (JSONException e){e.printStackTrace();}
            counter++;
        }

        return mDealList;

    }

}
